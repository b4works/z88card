#!/bin/bash

# *************************************************************************************
# Compile script for the Z88 Application Card Generator.
# Mac OSX edition using Xcode and QtSDK or installed/compiled Qt sources
#
# (C) Gunther Strube (gstrube@gmail.com) 2012-2015
#
# Requirements:
#
#   1)   make and gcc compiler tools are available in PATH and LD_LIBRARY_PATH configured
#        by Xcode installation
#
#   2)   Qt tools and libraries are installed using QtSDK Installer (or compile Qt sources)
#        which are available through QTDIR and PATH environment variables, which must be
#        defined in .bash_profile (system script executed during account login)
#
#   2.1) QTDIR points to <Qt-SDK-Dir>/<version>/clang_64/bin
#        export QTDIR=<Qt-source-compiled-Dir>/Qt/<version>/bin
#        export PATH=$QTDIR:$PATH
#
# -------------------------------------------------------------------------------------
#
# Z88Card is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Z88Card;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# *************************************************************************************


# get the relative path to this script
Z88CARD_PATH=$PWD

command -v qmake >/dev/null 2>&1 || { echo "qmake utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v gcc >/dev/null 2>&1 || { echo "GNU C compiler is not available on system!" >&2; exit 1; }

# delete any previous compile output
rm -fR build
rm -fR bin
rm -f Makefile
rm -f src/Makefile

qmake -config release z88card.pro
if test $? -gt 0; then
	echo "qmake failed generating makefile!"
	exit 1
fi

if [ ! -f $Z88CARD_PATH/Makefile ]; then
	echo "$Z88CARD_SRC/Makefile was not created by qmake!"
	exit 1
fi

# compile Z88Card sources...
make -f Makefile
if test $? -gt 0; then
	echo "make failed compiling Z88Card sources!"
	exit 1
fi

echo "Z88Card compiled successfully on your system."
