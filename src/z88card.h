/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
  (C) Copyright Garry Lancaster 2012

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/


#ifndef Z88CARD_H
#define Z88CARD_H

#include <stdbool.h>
#include "symbols.h"
#include "exprprsr.h"
#include "directives.h"
#include "prsline.h"
#include "files.h"
#include "errors.h"
#include "bank.h"
#include "card.h"
#include "filecard.h"

typedef
struct module       {
    struct module      *nextmodule;       /* pointer to next module */
    char               *mname;            /* pointer to string of module name */
    sourcefile_t       *cfile;            /* pointer to current file record */
    avltree_t          *notdeclsymbols;   /* pointer to root of symbols tree not yet declared/defined */
    avltree_t          *localsymbols;     /* pointer to root of local symbols tree */
    avltree_t          *globalsymbols;    /* pointer to root of copied global symbols tree (after pass 2) */
    expressions_t      *mexpr;            /* pointer to expressions in this module */
} module_t;

typedef
struct modules      {
    module_t           *first;            /* pointer to first module */
    module_t           *last;             /* pointer to current/last module */
} modules_t;


module_t *CurrentModule(void);

void DisplayApplVersion(void);
void ReleaseModules (void);
int ParseCardSize(char *str);

bool ProcessCommandline(int argc, char *argv[]);
bool ParseLoadMapFile (void);
bool CreateModule(void);

#define VERSION_NUMBER 20

#endif // Z88CARD_H
