/******************************************************************************
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
 * (C) Copyright Garry Lancaster 2012
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 ******************************************************************************/


#ifndef CARD_H
#define CARD_H

#include <stdlib.h>
#include "bank.h"

typedef struct {
    int         totalBanks;
    bank_t      **banks;            /* array of (pointers to) bank(s) in card */
} card_t;

card_t *Card(int totalBanks, int romUpdateConfigFileType, char *outputFilename);
bool LoadCode(card_t **card, char *filename, mem_t codeBuffer, int codeBufferSize, unsigned int extaddress);
bool CreateRomUpdCfgFile_AppCard(card_t *card, char *romUpdateConfigFilename);
bool CreateRomUpdCfgFile_Card(card_t *card, char *romUpdateConfigFilename);
bool CreateRomUpdCfgFile_OzSlot(card_t *card, int slotNo, char *romUpdateConfigFilename);
bool CreateOzUpdCfgFile_OzSlot(card_t *card, int slotNo, char *ozUpdateConfigFilename);
bool Patch(card_t *card, int argc, char *argv[]);
bank_t *GetBottomBank(card_t *card);
bank_t *GetTopBank(card_t *card);
void ApplyCardId(card_t *card);
void DumpCard(card_t *card, char *outputFilename);
void DumpCardAsBanks(card_t *card);
void FreeCard(card_t *card);
void GetNextExtAddress(int *extAddress);
byte CardGetByte(card_t *card, int extaddress);
void CardSetByte(card_t *card, int extaddress, int byte);

#endif // CARD_H
