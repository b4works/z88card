
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/


#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "z88card.h"

/* local functions */
static void PushItem (long oprconst, pfixstack_t **stackpointer);
static long PopItem (pfixstack_t **stackpointer);
static void EvalExprStackOperands (enum symbols opr, pfixstack_t **stackptr);
static int Condition (expression_t *pfixexpr);
static int Expression (expression_t *pfixexpr);
static int Term (expression_t *pfixexpr);
static int Pterm (expression_t *pfixexpr);
static int Factor (expression_t *pfixexpr);
static expression_t *AllocExpr (void);
static void NewPfixSymbol (expression_t *pfixexpr, long oprconst, enum symbols oprtype, char *symident, unsigned long type);
static pfixstack_t *AllocStackItem (void);
static postfixexpr_t *AllocPfixSymbol (void);
static long Pw (long x, long y);
static void AddStringToInfixExpr(expression_t *pfixexpr, char *ident);
static void AddCharToInfixExpr(expression_t *pfixexpr, char c);


/* global variables */
extern avltree_t *globalsymbols;
extern enum symbols sym, ssym[];
extern char *ident;
extern char separators[];


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


expression_t *ParseNumExpr (void)
{
    expression_t *pfixhdr;
    enum symbols constant_expression = nil;

    if ((pfixhdr = AllocExpr ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return NULL;
    } else {

        pfixhdr->srcfile = CurrentFile();                 /* pointer to record containing current source file */
        pfixhdr->exprlineptr = CurrentFile()->lineptr;    /* preserve pointer to start of expression in current source file */
        pfixhdr->curline = CurrentFile()->lineno;         /* line number of expression in current source file */

        pfixhdr->nextexpr = NULL;
        pfixhdr->firstnode = NULL;
        pfixhdr->currentnode = NULL;
        pfixhdr->rangetype = 0;

        if ((pfixhdr->infixexpr = (char *) calloc(MAX_EXPR_SIZE+1, sizeof(char))) == NULL) {
            ReportError (NULL, Err_Memory);
            free (pfixhdr);
            return NULL;
        } else {
            /* null-terminated buffer for infix expression prepared */
            pfixhdr->infixptr = pfixhdr->infixexpr;    /* initialise pointer to start of buffer */
        }
    }

    if (sym == cnstexpr) {
        GetSym ();                /* leading '#' : ignore relocatable address expression */
        constant_expression = cnstexpr;  /* convert to constant expression */
        AddCharToInfixExpr(pfixhdr, separators[cnstexpr]);
    }

    if (Condition (pfixhdr)) {
        /* parse expression... */
        if (constant_expression == cnstexpr) {
            NewPfixSymbol (pfixhdr, 0, cnstexpr, NULL, 0);    /* convert to constant expression */
        }

        return pfixhdr;
    } else {
        FreeNumExpr (pfixhdr);
        return NULL;              /* syntax error in expression or no room */
    }                             /* for postfix expression */
}


long EvalNumExpr (expression_t *pfixlist)
{
    pfixstack_t *stackptr = NULL;
    postfixexpr_t *pfixexpr;
    symbol_t *symptr;

    pfixlist->rangetype &= EVALUATED;     /* prefix expression as evaluated (reset previous flags) */
    pfixexpr = pfixlist->firstnode;       /* initiate to first node */

    do {
        switch (pfixexpr->operatortype) {
        case number:
            if (pfixexpr->id == NULL) {   /* Is operand an identifier? */
                PushItem (pfixexpr->operandconst, &stackptr);
            } else {
                if (pfixexpr->type != SYM_NOTDEFINED) {
                    /* symbol was not defined and not declared */
                    /* if all bits are set to zero */
                    if (pfixexpr->type & SYMLOCAL) {
                        symptr = FindSymbol (pfixexpr->id, CurrentModule()->localsymbols);
                        pfixlist->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type
                                                                         * bits */
                        PushItem (symptr->symvalue, &stackptr);
                    } else {
                        symptr = FindSymbol (pfixexpr->id, globalsymbols);
                        if (symptr != NULL) {
                            pfixlist->rangetype |= (symptr->type & SYMTYPE);      /* Copy appropriate type
                                                                                 * bits */
                            if (symptr->type & SYMDEFINED) {
                                PushItem (symptr->symvalue, &stackptr);
                            } else {
                                pfixlist->rangetype |= NOTEVALUABLE;
                                PushItem (0, &stackptr);
                            }
                        } else {
                            pfixlist->rangetype |= NOTEVALUABLE;
                            PushItem (0, &stackptr);
                        }
                    }
                } else {
                    /* try to Find symbol now as either */

                    symptr = GetSymPtr (pfixexpr->id);    /* declared local or global */
                    if (symptr != NULL) {
                        pfixlist->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type bits */
                        if (symptr->type & SYMDEFINED) {
                            PushItem (symptr->symvalue, &stackptr);
                        } else {
                            pfixlist->rangetype |= NOTEVALUABLE;
                            PushItem (0, &stackptr);
                        }
                    } else {
                        pfixlist->rangetype |= NOTEVALUABLE;
                        PushItem (0, &stackptr);
                    }
                }
            }
            break;

        case negated:
            stackptr->stackconstant = -stackptr->stackconstant;
            break;

        case log_not:
            stackptr->stackconstant = !(stackptr->stackconstant);
            break;

        case bin_not:
            stackptr->stackconstant = ~(stackptr->stackconstant);
            break;

        case div256:
            stackptr->stackconstant = stackptr->stackconstant / 256;
            break;

        case mod256:
            stackptr->stackconstant = stackptr->stackconstant % 256;
            break;

        case cnstexpr:
            pfixlist->rangetype &= CLEAR_EXPRADDR;            /* convert to constant expression */
            break;

        default:
            EvalExprStackOperands (pfixexpr->operatortype, &stackptr);   /* expression requiring two operands */
            break;
        }

        pfixexpr = pfixexpr->nextoperand;         /* get next operand in postfix expression */
    } while (pfixexpr != NULL);

    if (stackptr != NULL) {
        return PopItem (&stackptr);
    } else {
        return 0;    /* Unbalanced stack - probably during low memory... */
    }
}


void FreeNumExpr (expression_t *pfixexpr)
{
    postfixexpr_t *node, *tmpnode;

    if (pfixexpr == NULL) {
        return;
    }

    node = pfixexpr->firstnode;
    while (node != NULL) {
        tmpnode = node->nextoperand;
        if (node->id != NULL) {
            free (node->id);    /* Remove symbol id, if defined */
        }

        free (node);
        node = tmpnode;
    }

    if (pfixexpr->infixexpr != NULL) {
        free (pfixexpr->infixexpr);    /* release infix expr. string */
    }

    free (pfixexpr);              /* release header of postfix expression */
}


void ReleaseExprns (expressions_t *express)
{
    expression_t *tmpexpr, *curexpr;

    curexpr = express->firstexpr;
    while (curexpr != NULL) {
        tmpexpr = curexpr->nextexpr;
        FreeNumExpr (curexpr);
        curexpr = tmpexpr;
    }

    free (express);
}


expressions_t *AllocExprHdr (void)
{
    return (expressions_t *) malloc (sizeof (expressions_t));
}


/* ------------------------------------------------------------------------------------------
   void AddExpr2Module(expression_t *pfixexpr, unsigned long constrange)

   Add (append) an expression to the linked list of module expressions. Adjust
   the pointer to the first node and current (end of list) when necessary.

   The linked list contains expressions that are being re-evaluated during pass 2.
   ------------------------------------------------------------------------------------------ */
void AddExpr2Module(expression_t *pfixexpr, unsigned long constrange /* allowed size of value to be parsed */)
{
    pfixexpr->rangetype = constrange;

    if (CurrentModule()->mexpr->firstexpr == NULL) {
        CurrentModule()->mexpr->firstexpr = pfixexpr;
        CurrentModule()->mexpr->currexpr = pfixexpr;              /* Expression header points at first expression */
    } else {
        CurrentModule()->mexpr->currexpr->nextexpr = pfixexpr;    /* Current expression node points to new expression node */
        CurrentModule()->mexpr->currexpr = pfixexpr;              /* Pointer to current expression node updated */
    }
}



/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


static void EvalExprStackOperands (enum symbols opr, pfixstack_t **stackptr)
{
    long leftoperand, rightoperand;

    rightoperand = PopItem (stackptr);    /* first get right operator */
    leftoperand = PopItem (stackptr);     /* then get left operator... */

    switch (opr) {
    case bin_and:
        PushItem (leftoperand & rightoperand, stackptr);
        break;

    case bin_or:
        PushItem (leftoperand | rightoperand, stackptr);
        break;

    case bin_nor:
        PushItem (~(leftoperand | rightoperand), stackptr);
        break;

    case bin_xor:
        PushItem (leftoperand ^ rightoperand, stackptr);
        break;

    case plus:
        PushItem (leftoperand + rightoperand, stackptr);
        break;

    case minus:
        PushItem (leftoperand - rightoperand, stackptr);
        break;

    case multiply:
        PushItem (leftoperand * rightoperand, stackptr);
        break;

    case divi:
        PushItem (leftoperand / rightoperand, stackptr);
        break;

    case mod:
        PushItem (leftoperand % rightoperand, stackptr);
        break;

    case lshift:
        PushItem (leftoperand << (rightoperand % 32), stackptr);
        break;

    case rshift:
        PushItem ((unsigned long) leftoperand >> (rightoperand % 32), stackptr);
        break;

    case power:
        PushItem (Pw (leftoperand, rightoperand), stackptr);
        break;

    case assign:
        PushItem ((leftoperand == rightoperand), stackptr);
        break;

    case less:
        PushItem ((leftoperand < rightoperand), stackptr);
        break;

    case lessequal:
        PushItem ((leftoperand <= rightoperand), stackptr);
        break;

    case greater:
        PushItem ((leftoperand > rightoperand), stackptr);
        break;

    case greatequal:
        PushItem ((leftoperand >= rightoperand), stackptr);
        break;

    case notequal:
        PushItem ((leftoperand != rightoperand), stackptr);
        break;

    default:
        PushItem (0, stackptr);
    }
}


static long Pw (long x, long y)
{
    long i;

    for (i = 1; y > 0; --y) {
        i *= x;
    }

    return i;
}


static void NewPfixSymbol (expression_t *pfixexpr,
               long oprconst,
               enum symbols oprtype,
               char *symident,
               unsigned long symtype)
{
    postfixexpr_t *newnode;

    if ((newnode = AllocPfixSymbol ()) != NULL) {
        newnode->operandconst = oprconst;
        newnode->operatortype = oprtype;
        newnode->nextoperand = NULL;
        newnode->type = symtype;

        if (symident != NULL) {
            newnode->id = AllocIdentifier (strlen (symident) + 1);        /* Allocate symbol */

            if (newnode->id == NULL) {
                free (newnode);
                ReportError (NULL, Err_Memory);
                return;
            }
            strcpy (newnode->id, symident);
        } else {
            newnode->id = NULL;
        }
    } else {
        ReportError (NULL, Err_Memory);

        return;
    }

    if (pfixexpr->firstnode == NULL) {
        pfixexpr->firstnode = newnode;
        pfixexpr->currentnode = newnode;
    } else {
        pfixexpr->currentnode->nextoperand = newnode;
        pfixexpr->currentnode = newnode;
    }
}



static void PushItem (long oprconst, pfixstack_t **stackpointer)
{
    pfixstack_t *newitem;

    if ((newitem = AllocStackItem ()) != NULL) {
        newitem->stackconstant = oprconst;
        newitem->prevstackitem = *stackpointer;   /* link new node to current node */
        *stackpointer = newitem;  /* update stackpointer to new item */
    } else {
        ReportError (NULL, Err_Memory);
    }
}



static long PopItem (pfixstack_t **stackpointer)
{

    pfixstack_t *stackitem;
    long constant;

    constant = (*stackpointer)->stackconstant;
    stackitem = *stackpointer;
    *stackpointer = (*stackpointer)->prevstackitem;       /* Move stackpointer to previous item */
    free (stackitem);                                     /* return old item memory to OS */
    return constant;
}


static void AddStringToInfixExpr(expression_t *pfixexpr, char *ident)
{
    int identLength = strlen(ident);

    if (strlen(pfixexpr->infixexpr)+identLength <= MAX_EXPR_SIZE) {
        strcpy (pfixexpr->infixptr, ident);       /* add identifier to infix expr */
        pfixexpr->infixptr += strlen (ident);     /* point at null terminator */
        *pfixexpr->infixptr = 0;                  /* null-terminate */
    } else {
        ReportError (CurrentFile(), Err_ExprTooBig);
    }
}


static void AddCharToInfixExpr(expression_t *pfixexpr, char c)
{
    if (strlen(pfixexpr->infixexpr)+1 <= MAX_EXPR_SIZE) {
        *pfixexpr->infixptr++ = c;                /* add operator, single char to infix expression */
        *pfixexpr->infixptr = 0;                  /* null terminate */
    } else {
        ReportError (CurrentFile(), Err_ExprTooBig);
    }
}


static int Factor (expression_t *pfixexpr)
{
    long constant;
    symbol_t *symptr;
    char eval_err;
    char *exprfuncArg, *exprfuncEndBracket;
    int c;
    symfunc exprFunction;

    switch (sym) {
    case asmfnname:
        exprfuncArg = strchr (ident, '[');
        if (exprfuncArg != NULL) {
            *exprfuncArg = '\0';    /* temporarily cut function name and argument, to find base function name */
        }

        exprFunction = LookupExprFunction(ident);
        if (exprfuncArg != NULL) {
            *exprfuncArg++ = '[';    /* restore complete identifier, and point to first char of function argument text */
        }

        if (exprFunction != NULL) {
            exprfuncEndBracket = strrchr (ident, ']');
            if (exprfuncEndBracket != NULL) {
                *exprfuncEndBracket = '\0';
            }

            symptr = exprFunction(exprfuncArg);

            if (exprfuncEndBracket != NULL) {
                *exprfuncEndBracket = ']';
            }

            if (symptr != NULL) {
                if (symptr->type & SYMDEFINED) {
                    pfixexpr->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type bits */
                    if (symptr->type & SYMADDR) {
                        /* If symbol is an address that always ensures current look-up value of symbol */
                        NewPfixSymbol (pfixexpr, symptr->symvalue, number, ident, symptr->type);
                    } else {
                        NewPfixSymbol (pfixexpr, symptr->symvalue, number, NULL, symptr->type);
                    }
                } else {
                    pfixexpr->rangetype |= ((symptr->type & SYMTYPE) | NOTEVALUABLE);
                    /* Copy appropriate declaration bits */

                    NewPfixSymbol (pfixexpr, 0, number, ident, symptr->type);
                    /* symbol only declared, store symbol name */
                }
            } else {
                pfixexpr->rangetype |= NOTEVALUABLE;  /* expression not evaluable */
                NewPfixSymbol (pfixexpr, 0, number, ident, SYM_NOTDEFINED);   /* symbol not found */
            }
        } else {
            pfixexpr->rangetype |= NOTEVALUABLE;  /* expression not evaluable */
            NewPfixSymbol (pfixexpr, 0, number, ident, SYM_NOTDEFINED);   /* symbol not found */
        }
        AddStringToInfixExpr(pfixexpr, ident);

        GetSym ();
        break;

    case name:
        symptr = GetSymPtr (ident);
        if (symptr != NULL) {
            if (symptr->type & SYMDEFINED) {
                pfixexpr->rangetype |= (symptr->type & SYMTYPE);  /* Copy appropriate type bits */
                if (symptr->type & SYMADDR) {
                    /* If symbol is an address that always ensures current look-up value of symbol */
                    NewPfixSymbol (pfixexpr, symptr->symvalue, number, ident, symptr->type);
                } else {
                    NewPfixSymbol (pfixexpr, symptr->symvalue, number, NULL, symptr->type);
                }
            } else {
                pfixexpr->rangetype |= ((symptr->type & SYMTYPE) | NOTEVALUABLE);
                /* Copy appropriate declaration bits */

                NewPfixSymbol (pfixexpr, 0, number, ident, symptr->type);
                /* symbol only declared, store symbol name */
            }
        } else {
            pfixexpr->rangetype |= NOTEVALUABLE;  /* expression not evaluable */
            NewPfixSymbol (pfixexpr, 0, number, ident, SYM_NOTDEFINED);   /* symbol not found */
        }
        AddStringToInfixExpr(pfixexpr, ident);

        GetSym ();
        break;

    case hexconst:
    case binconst:
    case decmconst:
        AddStringToInfixExpr(pfixexpr, ident);
        constant = GetConstant (&eval_err);

        if (eval_err == 1) {
            ReportError (CurrentFile(), Err_ExprSyntax);
            return 0;             /* syntax error in expression */
        } else {
            NewPfixSymbol (pfixexpr, constant, number, NULL, 0);
        }

        GetSym ();
        break;

    case lparen:
    case lexpr:
        AddCharToInfixExpr(pfixexpr, separators[sym]); /* store '(' or '[' in infix expr */
        GetSym ();

        if (Condition (pfixexpr)) {
            if (sym == rparen || sym == rexpr) {
                AddCharToInfixExpr(pfixexpr, separators[sym]); /* store ')' or ']' in infix expr */
                GetSym ();
                break;
            } else {
                ReportError (CurrentFile(), Err_ExprBracket);
                return 0;
            }
        } else {
            return 0;
        }

    case log_not:
        AddCharToInfixExpr(pfixexpr, separators[log_not]);
        GetSym ();

        if (!Factor (pfixexpr)) {
            return 0;
        } else {
            NewPfixSymbol (pfixexpr, 0, log_not, NULL, 0);    /* Unary logical NOT... */
        }
        break;

    case greater:
        AddCharToInfixExpr(pfixexpr, '>');
        GetSym ();

        if (!Factor (pfixexpr)) {
            return 0;
        } else {
            NewPfixSymbol (pfixexpr, 0, div256, NULL, 0);    /* Unary Divide By 256 */
        }
        break;

    case less:
        AddCharToInfixExpr(pfixexpr, '<');
        GetSym ();

        if (!Factor (pfixexpr)) {
            return 0;
        } else {
            NewPfixSymbol (pfixexpr, 0, mod256, NULL, 0);    /* Unary Modulus 256 */
        }
        break;

    case bin_not:
        AddCharToInfixExpr(pfixexpr, separators[bin_not]);
        GetSym ();

        if (!Factor (pfixexpr)) {
            return 0;
        } else {
            NewPfixSymbol (pfixexpr, 0, bin_not, NULL, 0);    /* Unary Binary NOT... */
        }
        break;

    case squote:
        AddCharToInfixExpr(pfixexpr, separators[squote]);
        c = GetChar ();
        if (c == EOF) {
            ReportError (CurrentFile(), Err_Syntax);
            return 0;
        } else {
            AddCharToInfixExpr(pfixexpr, (char) c); /* store char in infix expr */
            if (GetSym () == squote) {
                AddCharToInfixExpr(pfixexpr, separators[squote]);
                NewPfixSymbol (pfixexpr, (long) c, number, NULL, 0);
            } else {
                ReportError (CurrentFile(), Err_ExprSyntax);
                return 0;
            }
        }

        GetSym ();
        break;

    default:
        ReportError (CurrentFile(), Err_ExprSyntax);
        return 0;
    }

    return 1;  /* syntax OK */
}



static int Pterm (expression_t *pfixexpr)
{
    if (!Factor (pfixexpr)) {
        return (0);
    }

    while (sym == power) {
        AddStringToInfixExpr(pfixexpr, "**");	/* add '**' power symbol to infix expr */

        GetSym ();
        if (Factor (pfixexpr)) {
            NewPfixSymbol (pfixexpr, 0, power, NULL, 0);
        } else {
            return 0;
        }
    }

    return (1);
}



static int Term (expression_t *pfixexpr)
{
    enum symbols mulsym;

    if (!Pterm (pfixexpr)) {
        return (0);
    }

    while ((sym == multiply) || (sym == divi) || (sym == mod)) {
        AddCharToInfixExpr(pfixexpr, separators[sym]); /* store '/', '%', '*' in infix expression */
        mulsym = sym;
        GetSym ();
        if (Pterm (pfixexpr)) {
            NewPfixSymbol (pfixexpr, 0, mulsym, NULL, 0);
        } else {
            return 0;
        }
    }

    return (1);
}



static int Expression (expression_t *pfixexpr)
{
    enum symbols addsym;

    if ((sym == plus) || (sym == minus)) {
        if (sym == minus) {
            AddCharToInfixExpr(pfixexpr, separators[minus]);
        }

        addsym = sym;
        GetSym ();

        if (Term (pfixexpr)) {
            if (addsym == minus) {
                NewPfixSymbol (pfixexpr, 0, negated, NULL, 0);      /* operand is signed, plus is redundant... */
            }
        } else {
            return (0);
        }
    } else if (!Term (pfixexpr)) {
        return (0);
    }

    while ((sym == plus) || (sym == minus) ||
            (sym == bin_and) || (sym == bin_or) || (sym == bin_nor) || (sym == bin_xor) ||
            (sym == lshift) || (sym == rshift)) {

        if (sym == lshift) {
            AddStringToInfixExpr(pfixexpr, "<<");
        } else if (sym == rshift) {
            AddStringToInfixExpr(pfixexpr, ">>");
        } else {
            AddCharToInfixExpr(pfixexpr, separators[sym]);
        }

        addsym = sym;
        GetSym ();
        if (sym == minus) {
            AddCharToInfixExpr(pfixexpr, separators[sym]);
            GetSym ();
            if (Term (pfixexpr)) {
                NewPfixSymbol (pfixexpr, 0, negated, NULL, 0);      /* operand is signed, negate it... */
                NewPfixSymbol (pfixexpr, 0, addsym, NULL, 0);       /* then push original operator to stack */
            }
        } else if (Term (pfixexpr)) {
            NewPfixSymbol (pfixexpr, 0, addsym, NULL, 0);
        } else {
            return (0);
        }
    }

    return (1);
}


static int Condition (expression_t *pfixexpr)
{
    enum symbols relsym;

    if (!Expression (pfixexpr)) {
        return 0;
    }

    switch (sym) {
    case less:      /* '<' */
    case greater:   /* '>' */
    case assign:    /* '=' */
        AddCharToInfixExpr(pfixexpr, separators[sym]);
        relsym = sym;
        GetSym ();
        break;

    case lessequal:
        AddStringToInfixExpr(pfixexpr, "<=");
        relsym = sym;
        GetSym ();
        break;

    case greatequal:
        AddStringToInfixExpr(pfixexpr, ">=");
        relsym = sym;
        GetSym ();
        break;

    case notequal:
        AddStringToInfixExpr(pfixexpr, "<>");
        relsym = sym;
        GetSym ();
        break;

    default:
        return 1;                 /* implicit (left side only) expression */
    }

    if (!Expression (pfixexpr)) {
        return 0;
    } else {
        NewPfixSymbol (pfixexpr, 0, relsym, NULL, 0);       /* condition... */
    }

    return 1;
}


static expression_t *AllocExpr (void)
{
    return (expression_t *) malloc (sizeof (expression_t));
}


static postfixexpr_t *AllocPfixSymbol (void)
{
    return (postfixexpr_t *) malloc (sizeof (postfixexpr_t));
}


static pfixstack_t *AllocStackItem (void)
{
    return (pfixstack_t *) malloc (sizeof (pfixstack_t));
}
