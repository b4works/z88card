/******************************************************************************
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
 * (C) Copyright Garry Lancaster 2012
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "z88card.h"


int main(int argc, char *argv[])
{
    int status;
    
    if ((argc == 2 && strcmp(argv[1],"-v") == 0)) {
        DisplayApplVersion();
        return VERSION_NUMBER;
    } else {
        AllocateTextBuffers();
        if (CreateModule() == false) {
            return 1;
        }

        if (ProcessCommandline(argc, argv) == true)
            status = 0; // mission completed..
        else
            status = 1; // signal error to command line

        /* parsing completed, release all dynamic data structures */
        ReleaseModules();
        FreeTextBuffers();
            
        return status;
    }
}
