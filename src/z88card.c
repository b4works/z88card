/******************************************************************************
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
 * (C) Copyright Garry Lancaster 2012
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 ******************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "z88card.h"

/* global variables */
bool pass1 = true;
bool splitBanks = false; /* by default dont't split binary image into 16K files */

char *outputFilename = NULL;
char romUpdateConfigFilename[] = "romupdate.cfg";
char *ozUpdateConfigFilename = NULL;

pathlist_t *gIncludePath = NULL;    /* linked list of include paths */
avltree_t *cachedincludefiles = NULL;

int appCardBanks = 1;               /* default output is 16K bank */
int appCardSize = 16;
int romUpdateConfigFileType = 4;    /* default card type */
bool generateCardId = false;        /* default is not to generate card ID */
card_t *card = NULL;                /* the card container for binaries */
filearea_t *filearea = NULL;        /* no file area yet specified */

/* refer to external variables */
extern bool errorstatus;

/* local variables */
static const char copyrightmsg[] = "Z88Card V2.0.2";
static bool BIGENDIAN, USEBIGENDIAN;
static module_t *currentmoduleptr = NULL;
static modules_t *modulehdr;

/* local functions */
static void DefineEndianLayout(void);
static void CreateRomUpdCfgFile(card_t *card);
static void Prompt(void);
static bool LoadMapFile(char *loadmapFilename);
static modules_t *AllocModuleHdr (void);
static module_t *AllocModule (void);
static module_t *NewModule (void);


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


void DisplayApplVersion(void) {
    fprintf(stdout,"%s\n", copyrightmsg);
}


/** ------------------------------------------------------------------------------------------
 * int ParseCardSize(char *str)
 *
 * @brief ParseCardSize
 * Parse integer value from string (fetched from command line or loadmap
 * file) and interpret it as the card size. The value must be a valid card
 * size and not larger than 1Mb (max size of Z88 slot).
 *
 * Card size is evaluated as size in K.
 *
 * @param str
 * @return card sice in K, or -1 if the value was illegal or badly formed
 * ----------------------------------------------------------------------------------------- */
int ParseCardSize(char *str)
{
    long cardSize = StrToLong(str, 10);

    if ( (cardSize != 16) && (cardSize != 32) && (cardSize != 64) &&
        (cardSize != 128) && (cardSize != 256) && (cardSize != 512) && (cardSize != 1024)) {
        cardSize = -1;
    }

    return (int) cardSize;
}


/* ------------------------------------------------------------------------------------------ */
bool ProcessCommandline(int argc, char *argv[])
{
    int argidx = 1;
    unsigned int offset, LengthOfCode;
    char *fileName;
    mem_t codeBuffer;
    bool loadedStatus, loadMapParsed = false;

    DefineEndianLayout();
    USEBIGENDIAN = false;   /* Z80 always use little endian format on integers */
    errorstatus = false;    /* indicate no global error status enabled */

    /* Get command line arguments, if any... */
    if (argc == 1) {
        puts(copyrightmsg);
        puts("Try -h for more information.");
        return true;
    } else {
        /* get all options first */
        while (argidx < argc) {
            if ( argv[argidx][0] == '-') {
                /* ================   -h (display help) option   ================ */
                if (strcmp(argv[argidx],"-h") == 0) {
                    Prompt();
                    return true;

                /* ================   -v (display version) option   ================ */
                } else if (strcmp(argv[argidx],"-v") == 0) {
                    DisplayApplVersion();
                    exit(VERSION_NUMBER);

                /* ================   -D (define symbol) option   ================ */
                } else if (strncmp(argv[argidx],"-D",2) == 0) {
                    ParseCmdLineDefSym( (argv[argidx++]+2) );

                /* ================   -I (include path) option   ================ */
                } else if (strncmp(argv[argidx],"-I",2) == 0) {
                    /* create Include Path list from argument */
                    AddPathNode (argv[argidx++]+2, &gIncludePath);

                /* ================   -sz or -szc (card size) options   ================ */
                } else if ((strcmp(argv[argidx],"-sz") == 0) || (strcmp(argv[argidx],"-szc") == 0) ) {

                    if ( strchr (argv[argidx], 'c') != NULL ) {
                        /* split output image into 16K bank files as well */
                        splitBanks = true;
                    }

                    /* get application image size argument */
                    argidx++;
                    if (argidx < argc) {
                        appCardSize = ParseCardSize(argv[argidx++]);
                        if (appCardSize != -1) {
                            appCardBanks = appCardSize / 16;
                        } else {
                            fprintf(stderr,"Illegal card size. Use only: 16K, 32K, 64K, 128K, 256K, 512K or 1024K.\n");
                            return false;
                        }
                    } else {
                        fprintf(stderr,"Card size not specified\n");
                        return false;
                    }

                /* ================   -f (parse loadmap) option   ================ */
                } else if (strcmp(argv[argidx],"-f") == 0) {
                    argidx++;
                    if (argidx < argc) {
                        /* load & parse contents of loadmap file... */
                        LoadMapFile(argv[argidx]);
                        loadMapParsed = true;
                    }
                } else {
                    fprintf(stderr,"Unknown option\n");
                    return false;
                }
            } else {
                /* first non-option is output filename */
                break;
            }
        }
    }

    if (loadMapParsed == false) {
        /* get application image output filename */
        if (argidx < argc) {
            outputFilename = strclone(argv[argidx++]);
        } else {
            fprintf(stderr,"Output filename not specified\n");
            return false;
        }

        /* only if loadmap file wasn't done; parse remaining arguments for the file binaries to be loaded into the card container */
        while (argidx < argc) {
            fileName = argv[argidx++];

            if (argidx < argc) {
                offset = StrToLong(argv[argidx++], 16);

                LengthOfCode = LengthOfFile(fileName);
                if (LengthOfCode > 0) {
                    codeBuffer = LoadFile(fileName);
                    if (codeBuffer != NULL) {
                        loadedStatus = LoadCode(&card, fileName, codeBuffer, LengthOfCode, offset);
                        free(codeBuffer);

                        if ( loadedStatus == false ) {
                            FreeCard(card);
                            FreeFileArea(filearea);
                            return false;
                        }
                    }
                }
            } else {
                fprintf(stderr,"Offset argument is missing\n");
                FreeCard(card);
                FreeFileArea(filearea);
                return false;
            }
        }
    }

    if (errorstatus == false) {
        if (generateCardId == true) {
            /* the loadmap indicated to generate an automized card ID via CRC-32 */
            ApplyCardId(card);
        }

        /* all binary fragments loaded, now dump the final card binary as complete output file... */
        DumpCard(card, outputFilename);

        if (splitBanks == true) {
            /* Also dump the final card binary as 16K bank files (only the non-empty) */
            DumpCardAsBanks(card);

            /* create a 'romupdate.cfg' file, if it has been directed in loadmap file. */
            CreateRomUpdCfgFile(card);
        }
    }

    FreeCard(card);
    FreeFileArea(filearea);
    if (outputFilename != NULL) free(outputFilename);
    if (ozUpdateConfigFilename != NULL) free(ozUpdateConfigFilename);

    return true;
}


bool CreateModule(void)
{
    bool status = true;

    if ((currentmoduleptr = NewModule ()) == NULL) {
        /* Create module data structure for general processing */
        ReportError (NULL, Err_Memory);
        status = false;
    }

    return status;
}


module_t *CurrentModule(void)
{
    return currentmoduleptr;
}


bool ParseLoadMapFile (void)
{
    while ( (!MfEof (CurrentFile())) && (errorstatus == false))
        ParseLine (true);

    return !errorstatus;
}


/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


static bool LoadMapFile(char *loadmapFilename)
{
    FILE *lmfile;
    bool parseStatus;

    currentmoduleptr->cfile = Newfile (NULL, loadmapFilename);
    if (CurrentFile() == NULL) {
        ReportError (NULL, Err_Memory);
        return false;
    } else {
        lmfile = fopen (CurrentFile()->fname, "rb");
        if (lmfile == NULL) {
            fprintf(stderr,"Couldn't read '%s' loadmap file\n", loadmapFilename);
            return false;
        } else {
            CacheFile (CurrentFile(), lmfile);     /* Cache loadmap source file, prepared for parsing... */
            fclose(lmfile);

            parseStatus = ParseLoadMapFile();

            return parseStatus;
        }
    }
}


/** ------------------------------------------------------------------------------------------
 * void CreateRomUpdCfgFile()
 *
 * @brief Create RomUpdate.cfg file, based on loadmap directive.
 *
 * <pre>
 * "romupdate oz.{0|1}"  Create romupdate.cfg using OZ ROM template
 * "romupdate app"       Create romupdate.cfg using APP template
 * </pre>
 * ----------------------------------------------------------------------------------------- */
static void CreateRomUpdCfgFile(card_t *card)
{
    switch (romUpdateConfigFileType) {
        case 1:
            /* CFG.V1 RomUpdate application card */
            CreateRomUpdCfgFile_AppCard(card, romUpdateConfigFilename);
            break;
        case 2:
            /* OZ CFG.V3 syntax for embedded OZ slot 0 update functionality in Index (OZ v4.7+) */
            CreateOzUpdCfgFile_OzSlot(card, 0, ozUpdateConfigFilename);
            /* CFG.V4 Generic RomUpdate, OZ ROM for slot 0 */
            CreateRomUpdCfgFile_OzSlot(card, 0, romUpdateConfigFilename);
            break;
        case 3:
            /* OZ CFG.V3 syntax for embedded OZ slot 1 update functionality in Index (OZ v4.7+) */
            CreateOzUpdCfgFile_OzSlot(card, 1, ozUpdateConfigFilename);
            /* CFG.V4 Generic RomUpdate, OZ ROM for slot 1 */
            CreateRomUpdCfgFile_OzSlot(card, 1, romUpdateConfigFilename);
            break;
        case 4:
            /* Generic Card (same V4 syntax as for OZ, but without the OZ-specific comments */
            CreateRomUpdCfgFile_Card(card, romUpdateConfigFilename);
            break;
    }
}


/* ------------------------------------------------------------------------------------------
   Investigate whether the memory architecture running this tool
   use Little Endian (low byte - high byte order) or Big Endian (high byte - low byte order)
   ------------------------------------------------------------------------------------------ */
static void DefineEndianLayout(void)
{
    unsigned short  v = 0x8000;
    unsigned char   *vp;

    vp = (unsigned char *) &v;  /* point at first byte of signed long word */
    if (*vp == 0x80) {
        BIGENDIAN = true;
    } else {
        BIGENDIAN = false;    /* little endian - low byte, high byte order */
    }
}


/* ------------------------------------------------------------------------------------------ */
static void Prompt(void)
{
    puts(copyrightmsg);
    puts("(C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016");
    puts("(C) Copyright Garry Lancaster 2012");
    puts("For more info, see https://cambridgez88.jira.com/wiki/display/MKAP\n");
    puts("Syntax:");
    puts("-v display program version, -h display this help");
    puts("[{-Dsymbol}] [{-Ipath}] -f loadmap.file");
    puts("or");
    puts("[-sz[c] Size] memdump.file input1.file offset {inputX.file offset}\n");
    puts("Usage: Load binary files into one or several 16K memory bank, and save it");
    puts("all to a new file. Offsets are specified in hex (truncated to 16K offsets).");
    puts("Larger application cards is created by optionally specifying size in K, eg.");
    puts("32 ... up to 1024K. Offsets are then extended with relative bank number,");
    puts("for example 3fc000 for bank 3f (top), offset 0000 (start of top bank).\n");
    puts("If you need to split a large assembled card into 16 bank on the output,");
    puts("use the -c switch, eg. -szc 64 will make both a 64K file and 4 files,");
    puts("added with .63 for the top bank of the card and downwards.\n");
    puts("-Ipath is to define multiple search paths for include files in loadmap");
    puts("-Dsymbol defines symbols, usable for IF conditions in loadmap\n");
    puts("Example, using default 16K application bank dump:");
    puts("appl.epr code.bin c000 romhdr.bin 3fc0");
    puts("(load 1st file at 0000, 2nd file at 3fc0, and save 16K bank to appl.epr)\n");
    puts("Example, using a 32K application bank dump (and separate 16K bank files):");
    puts("-szc 32 bigappl.epr mth.bin 3e0000 code.bin 3fc000 romhdr.bin 3f3fc0\n");
    puts("-----------------------------------------------------------------------");
    puts("Using the -f <loadmap.file> option, all load instructions are specified");
    puts("in the 'loadmap' text file, having one load directive per line.");
    puts("Comments are allowed (to document the loadmap) using ; (semicolon).");
    puts("Directives, and arguments in <>, are specified in the following order:");
    puts("1) outputfile <filename>     ; filename of combined binary.");
    puts("2) size <size>               ; total file size K, from 16K-1024K");
    puts("3) save16k                   ; save output as 16K bank files (optional)");
    puts("4) romupdate oz.{0|1}        ; Create romupdate.cfg using OZ ROM template");
    puts("   ozupdate oz.{0|1}         ; Create ozsX-<gitrev>.upd file for OZ v4.7+");
    puts("5) romupdate app             ; Create romupdate.cfg using APP template");
    puts("6) romupdate card            ; Create romupdate.cfg using CARD template");
    puts("7) generateCardId            ; Create a unique CRC-32 Card ID, based on binary");
    puts("8) patch <$addr> {,<$byte>}  ; patch memory at address with max 16 byte(s)");
    puts("x) <input.file> <addr>       ; the binary file fragment to load at address\n");
}


static modules_t *
AllocModuleHdr (void)
{
    return (modules_t *) malloc (sizeof (modules_t));
}


static module_t *
AllocModule (void)
{
    return (module_t *) malloc (sizeof (module_t));
}


static module_t *
NewModule (void)
{
    module_t *newm;

    if (modulehdr == NULL) {
        if ((modulehdr = AllocModuleHdr ()) == NULL) {
            return NULL;
        } else {
            modulehdr->first = NULL;
            modulehdr->last = NULL;       /* Module header initialised */
        }
    }

    if ((newm = AllocModule ()) == NULL) {
        return NULL;
    } else {
        newm->nextmodule = NULL;
        newm->mname = NULL;
        newm->cfile = NULL;
        newm->localsymbols = NULL;
        newm->notdeclsymbols = NULL;
        newm->globalsymbols = NULL;

        if ((newm->mexpr = AllocExprHdr ()) != NULL) {
            /* Allocate room for expression header */
            newm->mexpr->firstexpr = NULL;
            newm->mexpr->currexpr = NULL;         /* Module expression header initialised */
        } else {
            free (newm);          /* remove partial module definition */
            return NULL;          /* No room for header */
        }
    }

    if (modulehdr->first == NULL) {
        modulehdr->first = newm;
        modulehdr->last = newm;   /* First module in list */
    } else {
        modulehdr->last->nextmodule = newm;       /* current/last module points now at new current */
        modulehdr->last = newm;                   /* pointer to current module updated */
    }

    return newm;
}


void ReleaseModules (void)
{
    module_t *tmpptr, *curptr;

    if (modulehdr == NULL) {
        return;
    }

    curptr = modulehdr->first;
    do {
        if (curptr->cfile != NULL) {
            ReleaseFile (curptr->cfile);
        }

        DeleteAll (&curptr->localsymbols, (void (*)(void *)) FreeSym);
        DeleteAll (&curptr->notdeclsymbols, (void (*)(void *)) FreeSym);
        DeleteAll (&curptr->globalsymbols, (void (*)(void *)) FreeSym);

        if (curptr->mexpr != NULL) {
            ReleaseExprns (curptr->mexpr);
        }

        if (curptr->mname != NULL) {
            free (curptr->mname);
        }

        tmpptr = curptr;
        curptr = curptr->nextmodule;
        free (tmpptr);            /* Release module */
    } while (curptr != NULL);     /* until all modules are released */

    free (modulehdr);

    modulehdr = NULL;
    currentmoduleptr = NULL;
}
