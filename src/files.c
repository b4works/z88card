/******************************************************************************
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 1991-2016
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 ******************************************************************************/


#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "z88card.h"

/* externally defined variables */
extern pathlist_t *gIncludePath;
extern avltree_t *cachedincludefiles;

/* local functions */
static void ReleaseOwnedFile (usedsrcfile_t *ownedfile);
static sourcefile_t *AllocFile (void);
static sourcefile_t *Setfile (sourcefile_t *curfile, sourcefile_t *newfile, char *fname);
static includefile_t *FindCachedIncludeFile (char *includefilename);
static includefile_t *Add2IncludeFileCache(char *filename, FILE *inclfile);
static usedsrcfile_t *AllocUsedFile (void);
static pathlist_t *AllocPathNode(void);
static filelist_t *AllocFileListNode (void);
static includefile_t *AllocIncludeFile (void);
static int cmpFndInclfile (char *inclfilename, includefile_t * p);
static int cmpInsInclfile (includefile_t * k, includefile_t * p);
static int Flncmp(char *f1, char *f2);


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


sourcefile_t *CurrentFile(void)
{
    return CurrentModule()->cfile;
}


sourcefile_t *Prevfile (void)
{
    usedsrcfile_t *newusedfile;
    sourcefile_t *ownedfile;

    if ((newusedfile = AllocUsedFile ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return CurrentFile();                     /* return parameter pointer - nothing happended! */
    }

    ownedfile = CurrentFile();
    CurrentModule()->cfile = CurrentFile()->prevsourcefile;    /* get back to owner file - now the current */
    CurrentFile()->newsourcefile = NULL;            /* current file is now the last in the list */
    ownedfile->prevsourcefile = NULL;             /* pointer to owner now obsolete... */

    /* set ptr to next record to current ptr to another used file */
    newusedfile->nextusedfile = CurrentFile()->usedsourcefile;

    CurrentFile()->usedsourcefile = newusedfile;    /* new used file now inserted into list */
    newusedfile->ownedsourcefile = ownedfile;     /* the inserted record now points to previously owned file */
    return CurrentFile();
}


sourcefile_t *Newfile (sourcefile_t *curfile, char *fname)
{
    sourcefile_t *nfile;

    if (fname == NULL) {
        /* Don't do anything ... */
        return NULL;
    }

    if (curfile == NULL) {
        /* file record has not yet been created */
        if ((curfile = AllocFile ()) == NULL) {
            ReportError (NULL, Err_Memory);
            return NULL;
        } else {
            return Setfile (NULL, curfile, fname);
        }
    } else if ((nfile = AllocFile ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return curfile;
    } else {
        return Setfile (curfile, nfile, fname);
    }
}


sourcefile_t *FindFile (sourcefile_t *srcfile, char *flnm)
{
    sourcefile_t *foundfile;

    if (srcfile != NULL) {
        if ((foundfile = FindFile(srcfile->prevsourcefile, flnm)) != NULL) {
            return foundfile;    /* trying to include an already included file recursively! */
        }

        if (Flncmp(srcfile->fname,flnm) == 0) {
            return srcfile;    /* this include file already used! */
        } else {
            return NULL;    /* this include file didn't match filename searched */
        }
    } else {
        return NULL;
    }
}


/* ------------------------------------------------------------------------------------------
    includefile_t *CacheIncludeFile (char *fname)

        Include File through shared file cache. If file is not yet available in Include File
        Cache, add it - then return a includefile_t reference to it. If the file has already
        been cached, just return the includefile_t reference to it.

    Returns:
        NULL if file was not found on disc or contents could not be loaded (I/O error)
        Pointer to include file reference, if file was successfully included
   ------------------------------------------------------------------------------------------ */
includefile_t *CacheIncludeFile (char *fname)
{
    FILE *includefile;
    includefile_t *cachedincludefile = NULL;

    if (FindFile(CurrentFile(), fname) != NULL) {
        /* Ups - this file has already been INCLUDE'ed (catch recursive include) */
        ReportError (CurrentFile(), Err_IncludeFile);
        return NULL;
    }

    if ( (cachedincludefile = FindCachedIncludeFile(fname)) != NULL) {
        /* found a cached include file with same name... return that! */
        return cachedincludefile;
    } else {
        /* this include file has not yet been cached... */
        includefile = OpenFile(fname, gIncludePath, false);
        if (includefile != NULL) {
            cachedincludefile = Add2IncludeFileCache(fname, includefile);
            fclose(includefile);
        } else {
            /* this include file wasn't found on the filing system */
            ReportError (CurrentFile(), Err_FileIO);
        }
    }

    return cachedincludefile;
}


/* ------------------------------------------------------------------------------------------
    unsigned char *CacheFile (sourcefile_t *srcfile, FILE *fd)

        Read the contents of specified source file <srcfile> into memory.

        srcfile->filedata is set to point to beginning of loaded ressource.
        srcfile->memfileptr = srcfile->filedata
        srcfile->filesize = size of file (used for EOF management)

    Returns:
        NULL if file was not found or contents could not be loaded (I/O error)
        pointer to start of file data, if file contents was successfully loaded into buffer
   ------------------------------------------------------------------------------------------ */
unsigned char *CacheFile (sourcefile_t *srcfile, FILE *fd)
{
    size_t filesize;

    if (srcfile == NULL) {
        /* Nothing to do here... */
        return NULL;
    }

    if (fd == NULL || srcfile->filedata != NULL) {
        /* report error if file couldnt be opened or data has already been loaded */
        ReportIOError (srcfile->fname);
        return NULL;
    }

    fseek(fd, 0L, SEEK_END); /* file pointer to end of file */
    filesize = ftell(fd);
    fseek(fd, 0L, SEEK_SET); /* file pointer back to start of file */

    return CacheFileData (srcfile, fd, filesize);
}



/* ------------------------------------------------------------------------------------------
    unsigned char *CacheFileData (sourcefile_t *srcfile, FILE *fd, size_t datasize)

        Load the data of current file pointer of opened file stream <fd> of <datasize> into
        specified source file <srcfile> as allocated ressource into srcfile->filedata.

        srcfile->filedata is set to point to beginning of loaded ressource.
        srcfile->memfileptr = srcfile->filedata
        srcfile->filesize = <datasize> (used for EOF management)

    Returns:
        NULL if file was not found or contents could not be loaded (I/O error)
        pointer to start of file data, if file contents was successfully loaded into buffer
   ------------------------------------------------------------------------------------------ */
unsigned char *CacheFileData (sourcefile_t *srcfile, FILE *fd, size_t datasize)
{
    unsigned char *fdbuffer = NULL;

    if (srcfile == NULL) {
        /* Nothing to do here... */
        return NULL;
    }

    if (fd == NULL || srcfile->filedata != NULL) {
        /* report error if file couldnt be opened or data has already been loaded */
        ReportIOError (srcfile->fname);
        return NULL;
    }

    if (datasize == 0) {
        srcfile->filesize = 0;
        srcfile->filedata = NULL;
        srcfile->memfileptr = NULL;
        srcfile->eof = false;
    } else {
        fdbuffer = (unsigned char *) calloc (datasize + 1, sizeof (char));
        if (fdbuffer == NULL) {
            ReportError (srcfile, Err_Memory);
        } else {
            if (fread (fdbuffer, sizeof (char), datasize, fd) != datasize) {    /* read file data into buffer */
                ReportError (srcfile, Err_FileIO);
                free (fdbuffer);
                fdbuffer = NULL;
            } else {
                srcfile->filesize = (long) datasize;
                srcfile->filedata = fdbuffer;
                srcfile->memfileptr = fdbuffer;
                srcfile->eof = false;
            }
        }
    }

    return fdbuffer;
}



/* ------------------------------------------------------------------------------------------
    sourcefile_t *CreateFileFromString (char *name, int lineno, char *string)

        Create a parsable memory file, based on supplied string.
        This is a utility function designed to let the source code parser
        and expression parser run through a sequence as it is contained
        in the string.

        <name> may optionally specify name of memory file, or NULL
        <line no> may optionally specify a beginning line number, or set = 0
        <string> is the null-terminated string to be allocated as memory file data

    Returns:
        NULL if insufficient heap memory was reached, or string was NULL.
        pointer to stand-alone Memory file.
   ------------------------------------------------------------------------------------------ */
sourcefile_t *CreateFileFromString (char *name, int lineno, char *string)
{
    sourcefile_t *nfile = NULL;
    unsigned char *buffer = NULL;

    if (string == NULL) {
        return NULL;
    }

    if ((nfile = AllocFile ()) == NULL) {
        ReportError (NULL, Err_Memory);
        return NULL;
    } else {
        Setfile (NULL, nfile, name);
        nfile->lineno = lineno;

        buffer = (unsigned char *) calloc (strlen(string) + 1, sizeof (char));
        if (buffer == NULL) {
            ReportError (NULL, Err_Memory);
            ReleaseFile(nfile);
            return NULL;
        } else {
            strcpy( (char *) buffer, string);
            nfile->filesize = (long) strlen(string);
            nfile->filedata = buffer;
            nfile->memfileptr = buffer;
        }
    }

    return nfile;
}


/* ------------------------------------------------------------------------------------------
    void ReleaseFile (sourcefile_t *srcfile)

    Release all previously allocated ressources of file.
   ------------------------------------------------------------------------------------------ */
void ReleaseFile (sourcefile_t *srcfile)
{
    if (srcfile != NULL) {
        if (srcfile->usedsourcefile != NULL) {
            ReleaseOwnedFile (srcfile->usedsourcefile);
        }

        if (srcfile->stream != NULL) {
            fclose(srcfile->stream);    /* In case a file wasn't closed in a abort situation */
        }

        ReleaseFileData(srcfile);

        if (srcfile->fname != NULL) {
            free (srcfile->fname);      /* Release allocated area for filename */
        }

        free (srcfile);                 /* Release file information record for this file */
    }
}



/* ------------------------------------------------------------------------------------------
    void ReleaseFileData (sourcefile_t *srcfile)

        Release previously allocated file data ressource and reset variables to indicate
        that no file data is cached.

        srcfile->filedata = NULL
        srcfile->memfileptr = NULL
        srcfile->filesize = 0
   ------------------------------------------------------------------------------------------ */
void ReleaseFileData (sourcefile_t *srcfile)
{
    if (srcfile != NULL) {
        srcfile->filesize = 0;
        srcfile->eof = false;

        if ( (srcfile->filedata != NULL) && (srcfile->includedfile == false) ) {
            /* this memory file is not part of Include File Cache, so release ressource directly */
            free (srcfile->filedata);

            srcfile->filedata = NULL;
            srcfile->memfileptr = NULL;
        }
    }
}


/* ------------------------------------------------------------------------------------------
    void ReleaseCachedIncludeFiles(void)

        Free cached Include file ressource (previously allocated) on each node
        of the Include File Cache AVL tree.
   ------------------------------------------------------------------------------------------ */
void ReleaseCachedIncludeFiles(void)
{
    DeleteAll (&cachedincludefiles, (void (*)(void *)) FreeCachedIncludeFile);
}



/* ------------------------------------------------------------------------------------------
   FILE *OpenFile(char *filename, pathlist_t *pathlist, bool expandfilename)

    The filename will be combined with each directory node in <pathlist> and a file
    open IO function will be executed.

   Returns:
    A file handle, if the file was successfully opened in one of the specified
    directories of <pathlist>, otherwise NULL, if the file wasn't found in
    the <pathlist>.
    The absolute filename of the found file will be written to <filename> string buffer,
    if <expandfilename> argument == ON.
   ------------------------------------------------------------------------------------------ */
FILE *OpenFile(char *filename, pathlist_t *pathlist, bool expandfilename)
{
    char tempflnm[MAX_FILENAME_SIZE+1];
    char tempdirsep[] = {DIRSEP, 0};
    FILE *filehandle;

    filehandle = fopen (AdjustPlatformFilename(filename), "rb");
    if (filehandle != NULL) {
        return filehandle;
    }

    while (pathlist != NULL) {
        if ( (strlen(pathlist->directory) + strlen(filename) + 1) > MAX_FILENAME_SIZE) {
            /* avoid buffer overrun of path + filename */
            return NULL;
        }

        strcpy(tempflnm, pathlist->directory);
        strcat(tempflnm, tempdirsep);
        strcat(tempflnm, filename);

        filehandle = fopen (AdjustPlatformFilename(tempflnm), "rb");
        if (filehandle != NULL) {
            if (expandfilename == true) {
                strcpy(filename, tempflnm);
            }
            return filehandle;                  /* file was found! */
        } else {
            pathlist = pathlist->nextdir;    /* file not in this directory, try next ... */
        }
    }

    return NULL;
}


/* ------------------------------------------------------------------------------------------
   char *AddFileExtension(const char *filename, char* extension)

   Allocate a new filename and add/replace with extension.
   Return NULL, if filename couldn't be allocated.
   ------------------------------------------------------------------------------------------ */
char *AddFileExtension(const char *oldfilename, const char *extension)
{
    char *newfilename;
    int b;
    int pathsepCount = 0;

    if (oldfilename == NULL || extension == NULL) {
        /* Nothing to do */
        return NULL;
    }

    if ((newfilename = AllocIdentifier (strlen (oldfilename) + strlen(extension) + 1)) != NULL) {
        strcpy (newfilename, oldfilename);

        /* scan filename backwards and find extension, but before a pathname separator */
        for (b=strlen(newfilename)-1; b>=0; b--) {
            if (newfilename[b] == '\\' || newfilename[b] == '/') {
                pathsepCount++;    /* Ups, we've scanned past the short filename */
            }

            if (newfilename[b] == '.' && pathsepCount == 0) {
                break; /* we found an extension before a path separator! */
            }
        }

        if (b > 0) {
            strcpy ( (newfilename+b), extension);    /* replace old extension with new */
        } else {
            strcat( newfilename, extension);    /* missing extension, concatanate new */
        }
    }

    return newfilename;
}


/* ------------------------------------------------------------------------------------------
    char *
    Truncate2BaseFilename(char *filename)

    Strip extension (null-terminate at extension "." and return pointer to start of base
    filename without path.

    For example
        /path1/path2/filename.ext
    will return a pointer to
        filename\0
   ------------------------------------------------------------------------------------------ */
char *Truncate2BaseFilename(char *filename)
{
    int b;

    /* scan fetched filename backwards and truncate extension, if found, but before a pathname separator */
    for (b=strlen(filename)-1; b>=0; b--) {
        if (filename[b] == '\\' || filename[b] == '/') {
            return (filename+b+1);
        }

        if (filename[b] == '.') {
            filename[b] = '\0'; /* truncate file extension */
        }
    }

    return filename; /* no path separator found, return start of current filename */
}



/* ------------------------------------------------------------------------------------------
   char *AdjustPlatformFilename(char *filename)

   Adjust filename to use the platform specific directory specifier, which is defined as
   DIRSEP in config.h. Adjusting the filename at runtime enables the freedom to not worry
   about paths in filenames when porting Z80 projects to Windows or Unix platforms.

   Example: if a filename contains a '/' (Unix directory separator) it will be converted
   to a '\' if mpm currently is compiling on Windows (or Dos).

   Returns:
   same pointer as argument (beginning of filename)
   ------------------------------------------------------------------------------------------ */
char *AdjustPlatformFilename(char *filename)
{
    char *flnmptr = filename;

    if (filename == NULL) {
        return NULL;
    }

    while(*flnmptr != '\0') {
        if (*flnmptr == '/' || *flnmptr == '\\') {
            *flnmptr = DIRSEP;
        }

        flnmptr++;
    }

    return filename;
}


/* ------------------------------------------------------------------------------------------
   void AddPathNode (char *path, pathlist_t **plist)

    Scans string <path> for sub paths (separated by ';' or ':' depending on OS) and adds
    them to the path list.

   Returns:
    Updates path list, which is referenced by <plist> pointer.
   ------------------------------------------------------------------------------------------ */
void AddPathNode (char *path, pathlist_t **plist)
{
    pathlist_t *newnode;
    char *pathcpy, *pathtoken, *newtoken, *newpath;

    if (path == NULL) {
        return;    /* nothing to do - no path has been specified */
    }
    if (strlen(path) == 0) {
        return;    /* nothing to do - path is empty! */
    }

    pathcpy = (char *) malloc (strlen(path)+1);
    if (pathcpy == NULL) {
        return;    /* nothing to do - no memory to add paths to list */
    }

    strcpy(pathcpy, path);
    pathtoken = pathcpy;

    while(pathtoken != NULL) {
        newtoken = memchr(pathtoken, ENVPATHSEP, strlen(pathtoken));
        if (newtoken != NULL) {
            *newtoken++ = 0;
        }

        if ((newnode = AllocPathNode()) != NULL) {
            newpath = AllocIdentifier (strlen(pathtoken)+1);
            if (newpath != NULL) {
                strcpy(newpath, pathtoken);
                /* remove directory separator */
                if (newpath[strlen(newpath) - 1] == DIRSEP) {
                    newpath[strlen(newpath) - 1] = '\0';
                }

                newnode->directory = newpath;
                newnode->nextdir = *plist;          /* link new node before current node */
                *plist = newnode;                   /* update start of path list to new node */
            } else {
                break;    /* couldn't allocate memory for sub path */
            }
        } else {
            ReportError (NULL, Err_Memory);
            break;
        }

        pathtoken = newtoken;      /* get next sub path, if available */
    }

    free(pathcpy);    /* release temp working copy of path argument variable */
}


/* ------------------------------------------------------------------------------------------
   void AddFileNameNode (char *filename, filelist_t **flist)

   Add filename (insert as first) to list of filenames <flist>.
   With initial list, the caller must initialize *flist = NULL.

   Returns:
    Updates filename list, which is referenced by <flist> pointer (start of list).
   ------------------------------------------------------------------------------------------ */
void AddFileNameNode (char *filename, filelist_t **flist)
{
    filelist_t *newnode;
    char *filenameCpy;

    if (filename == NULL) {
        return;    /* nothing to do - no filename has been specified */
    }
    if (strlen(filename) == 0) {
        return;    /* nothing to do - filename is empty! */
    }

    filenameCpy = (char *) malloc (strlen(filename)+1);
    if (filenameCpy == NULL) {
        return;    /* nothing to do - no memory to add paths to list */
    }
    strcpy(filenameCpy, filename);

    if ((newnode = AllocFileListNode()) != NULL) {
        newnode->filename = filenameCpy;
        newnode->nextfile = *flist;         /* link new node before current node */
        *flist = newnode;                   /* update start of list to new node */
    } else {
        ReportError (NULL, Err_Memory);
    }
}


/* ----------------------------------------------------------------
    int MfGetc(sourcefile_t *file)

        Reads the character from current memory file pointer
        (auto-increased) and returns it as an unsigned char cast
        to an int, or EOF on end of file.
   ---------------------------------------------------------------- */
int MfGetc(sourcefile_t *file)
{
    int memchar;
    long boundsize = file->memfileptr - file->filedata;

    if (file == NULL) {
        /* file not specified! */
        return EOF;
    } else {
        if (file->filedata == NULL || file->eof == true) {
            return EOF;
        } else {
            if ( (boundsize >= file->filesize) || (boundsize < 0) ) {
                /* memory pointer protection: ensure bounds-check before accessing memory... */
                file->eof = true;
                return EOF;
            } else {
                memchar = *file->memfileptr++;

                if ( (++boundsize) >= file->filesize) {
                    /* signal EOF - last character was just returned */
                    file->eof = true;
                }
            }
        }
    }

    return memchar;
}


/* ----------------------------------------------------------------
    void MfUngetc(sourcefile_t *file)

        "Push back" character in memory file; simply step-back
        the memory file pointer one character.

        If character is successfully pushed-back, the end-of-file
        indicator for the memory file stream is cleared.
        The file-position indicator of <file> is also decremented.
   ---------------------------------------------------------------- */
void MfUngetc(sourcefile_t *file)
{
    if (file != NULL) {
        if (file->filedata != NULL) {
            if (file->memfileptr > file->filedata) {
                /* decrease character file pointer in memory file */
                file->memfileptr--;
                file->eof = false;
            }
        }
    }
}


/* ----------------------------------------------------------------
    int MfTell(sourcefile_t *file)

    returns
        The current file pointer of memory file
   ---------------------------------------------------------------- */
unsigned char *MfTell(sourcefile_t *file)
{
    if (file == NULL) {
        /* file not specified! */
        return NULL;
    } else {
        if (file->filedata == NULL) {
            return NULL;
        } else {
            return file->memfileptr;
        }
    }
}


/* ----------------------------------------------------------------
    int MfSeek(sourcefile_t *file, unsigned char *ptr)

    Set file pointer of memory file
    returns
        0 if pointer was set successfully, otherwise -1
   ---------------------------------------------------------------- */
int MfSeek(sourcefile_t *file, unsigned char *ptr)
{
    if (file == NULL) {
        /* file not specified! */
        return -1;
    } else {
        if (file->filedata == NULL) {
            return -1;
        } else {
            if ( (ptr >= file->filedata) && (ptr <= (file->filedata + file->filesize)) ) {
                /* memory file pointer has reached beyond last character of file */
                file->memfileptr = ptr;
                file->eof = false;

                return 0;
            } else {
                /* pointer is out of range of file buffer! */
                return -1;
            }
        }
    }
}


/* ----------------------------------------------------------------
    int MfEof(sourcefile_t *file)

    returns
        End of File status (EOF, otherwise 0) of memory file
   ---------------------------------------------------------------- */
int MfEof(sourcefile_t *file)
{
    if (file == NULL) {
        /* file not specified! */
        return EOF;
    } else {
        if (file->filedata == NULL || file->eof == true) {
            return EOF;
        } else {
            return 0;
        }
    }
}


void FreeCachedIncludeFile(includefile_t * inclfile)
{
    if (inclfile->fname != NULL) {
        free(inclfile->fname);
    }
    if (inclfile->filedata != NULL) {
        free(inclfile->filedata);
    }
}



/* ------------------------------------------------------------------------------------------ */
size_t LengthOfFile(char *filename)
{
    FILE *binfile;
    size_t filesize = 0;

    if ((binfile = fopen (AdjustPlatformFilename(filename), "rb")) == NULL) {
        ReportIOError (filename);
    } else {
        fseek(binfile, 0L, SEEK_END); /* file pointer to end of file */
        filesize = ftell(binfile);
        fclose (binfile);
    }

    return filesize;
}


/* ------------------------------------------------------------------------------------------
    mem_t LoadFile (char *tokenfilename)

    Load (binary) file into allocated heap memory, null-terminated.

    Returns:
    pointer to allocated memory, or NULL (if no space in system or file I/O)
   ------------------------------------------------------------------------------------------ */
mem_t LoadFile(char *filename)
{
    FILE *binfile;
    size_t filesize = LengthOfFile(filename);
    mem_t bufptr = NULL;

    if (filesize > 0) {
        binfile = fopen (AdjustPlatformFilename(filename), "rb");
        bufptr = (mem_t) malloc (filesize + 1);
        if (bufptr == NULL) {
            ReportError (NULL, Err_Memory);
        } else {
            if (fread (bufptr, sizeof (char), filesize, binfile) != filesize) {    /* read binary code */
                ReportIOError (filename);
                free(bufptr);
                bufptr = NULL;
            } else {
                /* null-terminate EOF */
                bufptr[filesize] = 0;
            }
        }

        fclose (binfile);
    }

    return bufptr;
}


/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


/* ----------------------------------------------------------------------------------------
    static includefile_t *FindCachedIncludeFile (char *filename)

        Find file in the Include File Cache (a searchable AVL tree).

    returns
        includefile_t reference to found include file in cache
        NULL, if include file was not found in Cache
   ---------------------------------------------------------------------------------------- */
static includefile_t *FindCachedIncludeFile (char *filename)
{
    if (cachedincludefiles == NULL) {
        return NULL;
    } else {
        return Find (cachedincludefiles, filename, (int (*)(void *,void *)) cmpFndInclfile);
    }
}


static int cmpFndInclfile (char *inclfilename, includefile_t * p)
{
    return strcmp (inclfilename, p->fname);
}


static int cmpInsInclfile (includefile_t * k, includefile_t * p)
{
    return strcmp (k->fname, p->fname);
}


/* ----------------------------------------------------------------------------------------
    static includefile_t *Add2IncludeFileCache(char *filename, FILE *inclfile)

        Add file ressource to the Include File Cache (a searchable AVL tree).

    returns
        includefile_t reference to cached include file
        NULL, if include file reported file I/O problems or insufficient memory for caching
   ---------------------------------------------------------------------------------------- */
static includefile_t *Add2IncludeFileCache(char *filename, FILE *inclfile)
{
    includefile_t *newinclfile;
    unsigned char *fdbuffer = NULL;

    newinclfile = AllocIncludeFile();
    if (newinclfile == NULL) {
        ReportError (NULL, Err_Memory);
        return NULL;
    }

    newinclfile->fname = AllocIdentifier (strlen (filename) + 1);  /* Allocate area for a include filename */
    if (newinclfile->fname == NULL) {
        free(newinclfile);
        ReportError (CurrentFile(), Err_Memory);
        return NULL;
    }

    strcpy(newinclfile->fname, filename);       /* include filename */

    fseek(inclfile, 0L, SEEK_END); /* file pointer to end of file */
    newinclfile->filesize = ftell(inclfile);
    fseek(inclfile, 0L, SEEK_SET); /* file pointer back to start of file */

    fdbuffer = (unsigned char *) calloc (newinclfile->filesize + 1, sizeof (char));
    if (fdbuffer == NULL) {
        free(newinclfile->fname);
        free(newinclfile);
        ReportError (CurrentFile(), Err_Memory);
        return NULL;
    } else {
        /* read file data into buffer */
        if (fread (fdbuffer, sizeof (char), (size_t) newinclfile->filesize, inclfile) != (size_t) newinclfile->filesize) {
            free (fdbuffer);
            free(newinclfile->fname);
            free(newinclfile);
            ReportError (CurrentFile(), Err_FileIO);
            return NULL;
        } else {
            newinclfile->filedata = fdbuffer;
        }
    }

    /* Insert new include file object into Include File Cache AVL tree */
    if (Insert (&cachedincludefiles, newinclfile, (int (*)(void *,void *)) cmpInsInclfile)) {
        /* return reference to new, cached include file */
        return newinclfile;
    } else {
        free (fdbuffer);
        free(newinclfile->fname);
        free(newinclfile);
        ReportError (CurrentFile(), Err_Memory);
        return NULL;
    }
}


static sourcefile_t *Setfile (sourcefile_t *curfile,    /* pointer to record of current source file */
         sourcefile_t *nfile,      /* pointer to record of new source file */
         char *filename)           /* pointer to filename string */
{
    if (filename != NULL) {
        if ((nfile->fname = AllocIdentifier (strlen (filename) + 1)) == NULL) {
            ReportError (NULL, Err_Memory);
            return nfile;
        }

        nfile->fname = strcpy (nfile->fname, filename);
    } else {
        nfile->fname = NULL;
    }

    nfile->prevsourcefile = curfile;
    nfile->newsourcefile = NULL;
    nfile->usedsourcefile = NULL;
    nfile->lineptr = NULL;
    nfile->lineno = 0;              /* Reset to 0 as line counter during parsing */
    nfile->stream = NULL;
    nfile->filesize = 0;
    nfile->eol = false;
    nfile->eof = false;
    nfile->includedfile = false;
    nfile->filedata = NULL;

    return nfile;
}


static void ReleaseOwnedFile (usedsrcfile_t *ownedfile)
{
    /* Release first other files called by this file */
    if (ownedfile->nextusedfile != NULL) {
        ReleaseOwnedFile (ownedfile->nextusedfile);
    }

    /* Release first file owned by this file */
    if (ownedfile->ownedsourcefile != NULL) {
        ReleaseFile (ownedfile->ownedsourcefile);
    }

    free (ownedfile);             /* Then release this owned file */
}



static int Flncmp(char *f1, char *f2)
{
    int i;

    if (strlen(f1) != strlen(f2)) {
        return -1;
    } else {
        i = strlen(f1);
        while(--i >= 0)
            if( tolower(f1[i]) != tolower(f2[i]) ) {
                return -1;
            }

        /* filenames equal */
        return 0;
    }
}


static pathlist_t *AllocPathNode (void)
{
    return (pathlist_t *) malloc (sizeof (pathlist_t));
}

static filelist_t *AllocFileListNode (void)
{
    return (filelist_t *) malloc (sizeof (filelist_t));
}


static usedsrcfile_t *AllocUsedFile (void)
{
    return (usedsrcfile_t *) malloc (sizeof (usedsrcfile_t));
}


static sourcefile_t *AllocFile (void)
{
    return (sourcefile_t *) malloc (sizeof (sourcefile_t));
}


static includefile_t *AllocIncludeFile (void)
{
    includefile_t *incf = (includefile_t *) malloc (sizeof (includefile_t));
    if (incf != NULL) {
        incf->fname = NULL;
        incf->filesize = 0;
        incf->filedata = NULL;
    }

    return incf;
}
