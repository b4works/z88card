/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#ifndef PRSLINE_H
#define PRSLINE_H

/* globally available functions */
unsigned char *GetLine (unsigned char *);
char *trim(char *str);
char *strclone (const char *s);
char *substr(char *s, char *find);
char *strappend(char *curstr, const char *newstr);
int AllocateTextBuffers();
int strnicmp (const char *s1, const char *s2, size_t n);
int ltoasc(long num, char* str, int len, int base);
int GetChar (void);
int fGetChar (FILE *fptr);
void ParseDirective (const bool interpret);
void ParseCmdLineDefSym(char *symbol);
void FreeTextBuffers();
void ParseLine (bool interpret);
void SkipLine (void);
void fSkipLine (FILE *fptr);
long GetConstant (char *evalerr);
long StrToLong(char *str, int radix);
enum symbols GetSym (void);

#define MAX_LINE_BUFFER_SIZE 4096

#endif // PRSLINE_H
