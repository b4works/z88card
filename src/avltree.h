
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#ifndef AVLTREE_H
#define AVLTREE_H

typedef struct avlnode {
    short           height;        /* height of avltree (max search levels from node) */
    void           *data;          /* pointer to data of node */
    struct avlnode *left, *right;  /* pointers to left and right avl subtrees */
} avltree_t;

void    *Find(avltree_t *p, void *key, int  (*comp)(void *,void *));
void    *ReOrder(avltree_t *p, int  (*symcmp)(void *,void *));
void    DeleteAll(avltree_t **p, void (*deldata)(void *));
void    DeleteNode(avltree_t **root, void *key, int  (*comp)(void *,void *), void (*delkey)(void *));
void    InOrder(avltree_t *p, void  (*action)(void *));
void    PreOrder(avltree_t *p, void  (*action)(void *));
int     Insert(avltree_t **root, void *key, int  (*comp)(void *,void *));
int     Move(avltree_t **p, avltree_t **newroot, int  (*symcmp)(void *,void *));
int     Copy(avltree_t *p, avltree_t **newroot, int  (*symcmp)(void *,void *), void  *(*create)(void *));

#endif /* AVLTREE_H */
