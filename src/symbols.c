
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "z88card.h"

/* external global variables */
extern bool pass1;

/* local functions */
static symbol_t *DefLocalSymbol (char *identifier, symvalue_t value, unsigned long symboltype);
static symbol_t *AllocSymbol (void);


/* global variables */
avltree_t *globalsymbols = NULL, *staticsymbols = NULL;


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


symbol_t *CreateSymNode (symbol_t * symptr)
{
    if (symptr != NULL)
        return CreateSymbol (symptr->symname, symptr->symvalue, symptr->type, symptr->owner);
    else
        return NULL;
}


symbol_t *CreateSymbol (char *identifier, symvalue_t value, unsigned long symboltype, module_t *symowner)
{
    symbol_t *newsym;

    if ((newsym = AllocSymbol ()) == NULL) {
        /* Create area for a new symbol structure */
        ReportError (NULL, Err_Memory);
        return NULL;
    }
    newsym->symname = AllocIdentifier (strlen (identifier) + 1);  /* Allocate area for a new symbol identifier */
    if (newsym->symname != NULL) {
        strcpy (newsym->symname, identifier);    /* store identifier symbol */
    } else {
        free (newsym);        /* Ups no more memory left.. */
        ReportError (NULL, Err_Memory);
        return NULL;
    }

    newsym->owner = symowner;
    newsym->type = symboltype;
    newsym->symvalue = value;

    return newsym;        /* pointer to new symbol node */
}



int cmpidstr (symbol_t * kptr, symbol_t * p)
{
    return strcmp (kptr->symname, p->symname);
}


int cmpidval (symbol_t * kptr, symbol_t * p)
{
    return (kptr->symvalue - p->symvalue);
}



/*
 * DefineSymbol will create a record in memory, inserting it into an AVL tree (or creating the first record)
 */
symbol_t *DefineSymbol (char *identifier,
              symvalue_t value,         /* value of symbol, label */
              unsigned long symboltype) /* symbol is either address label or constant */
{
    symbol_t *foundsymbol;

    if ((foundsymbol = FindSymbol (identifier, globalsymbols)) == NULL) { /* symbol not declared as global/extern */
        return DefLocalSymbol (identifier, value, symboltype);
    } else if (foundsymbol->type & SYMXDEF) {
        if ((foundsymbol->type & SYMDEFINED) == 0) {
            /* symbol declared global, but not yet defined */
            foundsymbol->symvalue = value;
            /* defined, and typed as address label or constant */
            foundsymbol->type |= (symboltype | SYMDEFINED);

            foundsymbol->owner = CurrentModule();   /* owner of symbol is always creator */
            return foundsymbol;
        } else {
            ReportError (CurrentFile(), Err_SymDefined);  /* global symbol already defined */
            return NULL;
        }
    } else {
        /* Symbol was already declared Extern, but now defined as a local symbol. */
        ReportError (CurrentFile(), Err_SymDeclExtern);
        return DefLocalSymbol (identifier, value, symboltype);
    }

    /* the extern symbol is now no longer accessible */
}


static symbol_t *DefLocalSymbol (char *identifier,
                symvalue_t value,           /* value of symbol, label */
                unsigned long symboltype)   /* symbol is either address label or constant */
{
    symbol_t *foundsymbol;

    if ((foundsymbol = FindSymbol (identifier, CurrentModule()->localsymbols)) == NULL) {
        /* symbol not declared as local */
        foundsymbol = CreateSymbol (identifier, value, symboltype | SYMLOCAL | SYMDEFINED, CurrentModule());
        if (foundsymbol == NULL) {
            return NULL;
        } else {
            if (!Insert (&(CurrentModule()->localsymbols), foundsymbol, (int (*)(void *,void *)) cmpidstr)) {
                ReportError (CurrentFile(), Err_Memory);
                FreeSym(foundsymbol);
                return NULL;
            }
        }

        return foundsymbol;
    } else if ((foundsymbol->type & SYMDEFINED) == 0) {
        /* symbol declared local, but not yet defined */
        foundsymbol->symvalue = value;
        /* local symbol type set to address label or constant */
        foundsymbol->type |= symboltype | SYMLOCAL | SYMDEFINED;

        foundsymbol->owner = CurrentModule();   /* owner of symbol is always creator */
        return foundsymbol;
    } else {
        ReportError (CurrentFile(), Err_SymDefined);  /* local symbol already defined */
        return NULL;
    }
}




/*
 * search for symbol in either local tree or global tree, return found pointer if defined/declared, otherwise return
 * NULL
 */
symbol_t *GetSymPtr (char *identifier)
{
    symbol_t *symbolptr;        /* pointer to current search node in AVL tree */
    symvalue_t symval;

    if ((symbolptr = FindSymbol (identifier, CurrentModule()->localsymbols)) == NULL) {
        if ((symbolptr = FindSymbol (identifier, globalsymbols)) == NULL) {
            if (pass1 == true) {
                if ((symbolptr = FindSymbol (identifier, CurrentModule()->notdeclsymbols)) == NULL) {
                    symval = 0;
                    symbolptr = CreateSymbol (identifier, symval, SYM_NOTDEFINED, CurrentModule());
                    if (symbolptr != NULL) {
                        if (!Insert (&(CurrentModule()->notdeclsymbols), symbolptr, (int (*)(void *,void *)) cmpidstr)) {
                            ReportError (CurrentFile(), Err_Memory);
                            FreeSym(symbolptr);
                        }
                    }
                }
            }

            return NULL;
        } else {
            return symbolptr; /* symbol at least declared - return pointer to it... */
        }
    } else {
        return symbolptr;     /* symbol at least declared - return pointer to it... */
    }
}



int compidentifier (char *identifier, symbol_t * p)
{
    return strcmp (identifier, p->symname);
}


/*
 * return pointer to found symbol in a symbol tree, otherwise NULL if not found
 */
symbol_t *FindSymbol (char *identifier,   /* pointer to current identifier */
            avltree_t * treeptr)  /* pointer to root of AVL tree */
{
    symbol_t *found;

    if (treeptr == NULL) {
        return NULL;
    } else {
        found = Find (treeptr, identifier, (int (*)(void *,void *)) compidentifier);
        if (found == NULL) {
            return NULL;
        } else {
            found->type |= SYMTOUCHED;
            return found;     /* symbol found (declared/defined) */
        }
    }
}


void DeclSymGlobal (char *identifier, unsigned long libtype)
{
    symbol_t *foundsym, *clonedsym;
    symvalue_t symval;

    if ((foundsym = FindSymbol (identifier, CurrentModule()->localsymbols)) == NULL) {
        if ((foundsym = FindSymbol (identifier, globalsymbols)) == NULL) {
            symval = 0;
            foundsym = CreateSymbol (identifier, symval, SYM_NOTDEFINED | SYMXDEF | libtype, CurrentModule());
            if (foundsym != NULL) {
                if (!Insert (&globalsymbols, foundsym, (int (*)(void *,void *)) cmpidstr)) {    /* declare symbol as global */
                    ReportError (CurrentFile(), Err_Memory);
                    FreeSym(foundsym);
                }
            }
        } else {
            if (foundsym->owner != CurrentModule()) {
                /* this symbol is declared in another module */
                if (foundsym->type & SYMXREF) {
                    foundsym->owner = CurrentModule();  /* symbol now owned by this module */
                    foundsym->type &= XREF_OFF;       /* re-declare symbol as global if symbol was */
                    foundsym->type |= SYMXDEF | libtype;  /* declared extern in another module */
                } else {                          /* cannot declare two identical global's */
                    ReportError (CurrentFile(), Err_SymDeclGlobalModule);    /* Already declared global */
                }
            } else {
                ReportError (CurrentFile(), Err_SymRedeclaration);    /* re-declaration not allowed */
            }
        }
    } else {
        if (FindSymbol (identifier, globalsymbols) == NULL) {
            /* If no global symbol of identical name has been created, then re-declare local symbol as global symbol */
            foundsym->type &= SYMLOCAL_OFF;
            foundsym->type |= SYMXDEF;
            clonedsym = CreateSymbol (foundsym->symname, foundsym->symvalue, foundsym->type, CurrentModule());
            if (clonedsym != NULL) {
                if (Insert (&globalsymbols, clonedsym, (int (*)(void *,void *)) cmpidstr)) {
                    /* original local symbol cloned as global symbol, now delete old local ... */
                    DeleteNode (&(CurrentModule()->localsymbols), foundsym, (int (*)(void *,void *)) cmpidstr, (void (*)(void *)) FreeSym);
                } else {
                    ReportError (CurrentFile(), Err_Memory);
                    FreeSym(clonedsym);
                }
            }
        } else {
            ReportError (CurrentFile(), Err_SymDeclGlobal);    /* already declared global */
        }
    }
}



void DeclSymExtern (char *identifier, unsigned long libtype)
{
    symbol_t *foundsym, *extsym;
    symvalue_t symval;

    if ((foundsym = FindSymbol (identifier, CurrentModule()->localsymbols)) == NULL) {
        if ((foundsym = FindSymbol (identifier, globalsymbols)) == NULL) {
            symval = 0;
            foundsym = CreateSymbol (identifier, symval, SYM_NOTDEFINED | SYMXREF | libtype, CurrentModule());
            if (foundsym != NULL) {
                if (!Insert (&globalsymbols, foundsym, (int (*)(void *,void *)) cmpidstr)) {    /* declare symbol as extern */
                    ReportError (CurrentFile(), Err_Memory);
                    FreeSym(foundsym);
                }
            }
        } else if (foundsym->owner == CurrentModule()) {
            ReportError (CurrentFile(), Err_SymRedeclaration);    /* Re-declaration not allowed */
        }
    } else {
        if (FindSymbol (identifier, globalsymbols) == NULL) {
            /* If no external symbol of identical name has been declared, then re-declare local
               symbol as external symbol, but only if local symbol is not defined yet */
            if ((foundsym->type & SYMDEFINED) == 0) {
                symval = 0;
                foundsym->type &= SYMLOCAL_OFF;
                foundsym->type |= (SYMXREF | libtype);
                extsym = CreateSymbol (identifier, symval, foundsym->type, CurrentModule());
                if (extsym != NULL) {
                    if (Insert (&globalsymbols, extsym, (int (*)(void *,void *)) cmpidstr)) {
                        /* original local symbol cloned as external symbol, now delete old local ... */
                        DeleteNode (&(CurrentModule()->localsymbols), foundsym, (int (*)(void *,void *)) cmpidstr, (void (*)(void *)) FreeSym);
                    } else {
                        ReportError (CurrentFile(), Err_Memory);
                        FreeSym(extsym);
                    }
                }
            } else {
                ReportError (CurrentFile(), Err_SymDeclLocal);    /* already declared local */
            }
        } else {
            ReportError (CurrentFile(), Err_SymRedeclaration);    /* re-declaration not allowed */
        }
    }
}


symbol_t *DefineDefSym (char *identifier, long value, avltree_t ** root)
{
    symbol_t *staticsym;
    symvalue_t symval;

    if ( (staticsym = FindSymbol (identifier, *root)) == NULL) {
        symval = value;
        staticsym = CreateSymbol (identifier, symval, SYMDEF | SYMDEFINED, NULL);
        if (staticsym != NULL) {
            if (Insert (root, staticsym, (int (*)(void *,void *)) cmpidstr)) {
                return staticsym;
            } else {
                FreeSym(staticsym);
                return NULL;
            }
        } else {
            return NULL;
        }
    } else {
        ReportError (CurrentFile(), Err_SymDefined);  /* symbol already defined */
        return staticsym;
    }
}


char *AllocIdentifier (size_t len)
{
    return (char *) calloc (len, sizeof(char));
}


void FreeSym (symbol_t * node)
{
    if (node->symname != NULL) {
        free (node->symname);    /* release symbol identifier */
    }

    free (node);          /* then release the symbol record */
}


/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


static symbol_t *AllocSymbol (void)
{
    return (symbol_t *) malloc (sizeof (symbol_t));
}

