
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/



#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "z88card.h"

static char *formatErrorLine(char *srcfileptr);

/* global variables */
FILE *errfile;
int errornumber, totalerrors;
bool errorstatus;


/* local variables */
static char *errmsg[] = {
    "File open/read error",
    "Syntax error",
    "symbol not defined",
    "Not enough memory",
    "Integer out of range",
    "Syntax error in expression",
    "Right bracket missing",
    "Expression out of range",
    "Source filename missing",
    "Illegal option",
    "Unknown identifier",
    "Illegal identifier",
    "Max. code size of %ld bytes reached",
    "errors occurred during processing",
    "Symbol already defined",
    "Symbol already declared local",
    "Symbol already declared global",
    "Symbol already declared external",
    "No command line arguments",
    "Illegal source filename",
    "Symbol declared global in another module",
    "Re-declaration not allowed",
    "Reserved name",
    "Environment variable not defined",
    "Cannot include file recursively",
    "Expression > 255 characters",
};


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


char *GetErrorMessage(error_t errno)
{
    if ( errno < Err_totalMessages ) {
        return errmsg[errno];
    } else {
        return NULL;
    }
}



void ReportError (sourcefile_t *file, error_t errnum)
{
    char  errstr[256], errflnmstr[128], errmodstr[128], errlinestr[64];
    char  *errline = NULL;

    errornumber = errnum;      /* set the global error variable for general error trapping */
    errorstatus = true;

    errflnmstr[0] = '\0';
    errmodstr[0] = '\0';
    errlinestr[0] = '\0';
    errstr[0] = '\0';

    if (file != NULL) {
        if (file->lineptr != NULL) {
            errline = formatErrorLine((char *) file->lineptr);
        }

        sprintf (errflnmstr,"In file '%s', ", file->fname);

        if (file->lineno != 0) {
            sprintf (errlinestr, "at line %d, ", file->lineno);
        }
    }

    strcpy(errstr, errflnmstr);
    strcat(errstr, errmodstr);
    strcat(errstr, errlinestr);
    strcat(errstr, errmsg[errnum]);

    switch(errnum) {
        case Err_Status:
            fprintf (stderr, "%d %s\n", totalerrors, errmsg[errnum]);
            break;

        default:
            if (errfile != NULL) {
                fprintf (errfile, "%s\n", errstr);
                if (errline) fprintf (errfile, "Line: >>%s<<\n", errline);
            }

            /* copy the error to stderr for immediate view */
            fprintf (stderr, "%s\n", errstr);
            if (errline) {
                fprintf (stderr, "Line: >>%s<<\n", errline);
                free(errline);
            }
    }

    ++totalerrors;
}


void ReportIOError (char *filename)
{
    errornumber = 0;
    errorstatus = true;

    if (filename != NULL) {
        fprintf (stderr,"File '%s' couldn't be opened or created\n", filename);
    } else {
        fprintf (stderr,"Cannot open file - unspecified filename\n");
    }

    ++totalerrors;
}


void ReportSrcMessage  (sourcefile_t *file, int lineno, char *message)
{
    char  errstr[256], errflnmstr[128], errmodstr[128], errlinestr[64];
    char  *errline = NULL;

    errorstatus = true;         /* set the global error variable for general error trapping */

    errflnmstr[0] = '\0';
    errmodstr[0] = '\0';
    errlinestr[0] = '\0';
    errstr[0] = '\0';

    if (file != NULL) {
        if (file->lineptr != NULL) {
            errline = formatErrorLine( (char *) file->lineptr);
        }

        sprintf (errflnmstr,"In file '%s', ", file->fname);
    }

    if (lineno != 0) {
        sprintf (errlinestr, "at line %d, ", lineno);
    }

    strcpy(errstr, errflnmstr);
    strcat(errstr, errmodstr);
    strcat(errstr, errlinestr);
    strcat(errstr, message);

    if (errfile != NULL) {
        fprintf (errfile, "%s\n", errstr);
        if (errline) fprintf (errfile, "Line: >>%s<<\n", errline);
    }
    fprintf (stderr, "%s\n", errstr);
    if (errline) {
        fprintf (stderr, "Line: >>%s<<\n", errline);
        free(errline);
    }

    ++totalerrors;
}


/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


static char *formatErrorLine(char *srcfileptr)
{
    char *instr;
    char *errline = strclone(srcfileptr);

    if (errline) {
        /* truncate to only display current line (if multiple lines are available) */
        instr = strchr (errline, '\n');
        if (instr) *instr = '\0';
        trim(errline);
    }

    return errline;
}
