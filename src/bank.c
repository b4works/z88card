/******************************************************************************
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
 * (C) Copyright Garry Lancaster 2012
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "z88card.h"


/* private functionality */
static bank_t *newBank (void);
static mem_t allocBankMem (void);


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


/******************************************************************************
 * bank_t *(int cardIndex, int bankNumber)
 *
 * @brief Allocate a bank with specified number
 * @param cardIndex
 * @param bankNumber
 * @return pointer to allocated 16K Bank
 ******************************************************************************/
bank_t *Bank(int cardIndex, int bankNumber)
{
    bank_t *bnk = newBank();

    if (bnk != NULL) {
        bnk->index = cardIndex;
        bnk->bankNo = bankNumber;
    }

    return bnk;
}


/******************************************************************************
 * void FreeBank(bank_t *bnk)
 *
 * @brief release allocated Bank structure back to Heap
 * @param bnk reference to bank structure
 ******************************************************************************/
void FreeBank(bank_t *bnk)
{
    if (bnk != NULL) {
        if (bnk->bankMem != NULL) {
            free(bnk->bankMem);
        }
        if (bnk->bankFileName != NULL) {
            free(bnk->bankFileName);
        }

        free(bnk);
    }
}


/******************************************************************************
 * void SetBankNumber(bank_t *bnk, int bNo)
 *
 * @brief define number of bank
 * @param bnk reference to bank structure
 * @param bNo
 ******************************************************************************/
void SetBankNumber(bank_t *bnk, int bNo)
{
    if (bnk != NULL) {
        bnk->bankNo = bNo;
    }
}


/******************************************************************************
 * int GetBankNumber(bank_t *bnk)
 *
 * @brief get bank number
 * @param bnk reference to bank structure
 * @return -1 if bank reference is NULL
 ******************************************************************************/
int GetBankNumber(bank_t *bnk)
{
    if (bnk != NULL) {
        return bnk->bankNo;
    } else {
        return -1;
    }
}


/******************************************************************************
 * bool IsEmpty(bank_t *bnk)
 *
 * @brief return true if bank is empty (only contains FFh)
 * @param bnk reference to bank structure
 * @return false if bank reference is null or a non-FFh byte is found
 ******************************************************************************/
bool IsEmpty(bank_t *bnk)
{
    int i;

    if (bnk != NULL) {
        for (i=0; i < BANKSIZE; i++) {
            if (GetByte(bnk, i) != 0xff) {
                return false;
            }
        }

        return true;
    }

    return false;
}


/******************************************************************************
 * void setByte(bank_t *bnk, int offset, byte b)
 *
 * @brief set a byte in the bank memory
 * @param bnk reference to bank structure
 * @param offset within bank
 * @param b the byte
 ******************************************************************************/
void SetByte(bank_t *bnk, int offset, byte b)
{
    if (bnk != NULL) {
        offset &= (BANKSIZE-1); /* ensure offset stays within boundary of bank */
        bnk->bankMem[offset] = b;
    }
}


/******************************************************************************
 * getByte(bank_t *bnk, int offset)
 *
 * @brief read byte from bank at specified offset
 * @param bnk reference to bank structure
 * @param offset within bank
 * @return -1 if bank reference is NULL
 ******************************************************************************/
byte GetByte(bank_t *bnk, int offset)
{
    if (bnk != NULL) {
        offset &= (BANKSIZE-1); /* ensure offset stays within boundary of bank */
        return bnk->bankMem[offset];
    } else {
        return -1;
    }
}


/******************************************************************************
 * loadBytes(bank_t *bnk, mem_t block, int length, int offset)
 *
 * @brief loads a block of bytes into the bank memory at specified offset
 * @param bnk reference to bank structure
 * @param block pointer to block of memory
 * @param length of block
 * @param offset the offset of the bank where the block is to be loaded
 ******************************************************************************/
void LoadBytes(bank_t *bnk, const mem_t block, int length, int offset)
{
    if (bnk != NULL) {
        offset &= (BANKSIZE-1); /* ensure offset stays within boundary of bank */

        if (offset+length <= BANKSIZE) {
            memcpy(bnk->bankMem+offset, block, length);
        }
    }
}


/******************************************************************************
 * mem_t DumpBytes(bank_t *bnk, const int offset, const int length)
 *
 * @brief return a pointer to allocated area that has been extracted from bank
 * @param bnk reference to bank structure
 * @param offset the offset of the bank where the block is to be dumped from
 * @param length the lenght of the block to be dumped
 * @return NULL is bank reference is NULL, or memory exhausted
 ******************************************************************************/
mem_t DumpBytes(bank_t *bnk, int offset, const int length)
{
    mem_t  block = NULL;

    if (bnk != NULL) {
        offset &= (BANKSIZE-1); /* ensure offset stays within boundary of bank */

        if (offset+length <= BANKSIZE) {
            block = (mem_t) malloc(length);
            if (block != NULL) {
                memcpy(block, bnk->bankMem+offset, length);
            }
        }
    }

    return block;
}


/******************************************************************************
 * size_t FwriteBytes(bank_t *bnk, FILE *outfile, int offset, const int length)
 *
 * @brief write bank contents to external file (previously opened for writing)
 *
 * @param bnk reference to bank structure
 * @param outfile the file handle
 * @param offset the offset of the bank where the block is to be dumped from
 * @param length the lenght of the block to be dumped
 * @return total bytes written, or -1 if NULL bank reference or bad bank boundary
 ******************************************************************************/
size_t FwriteBytes(bank_t *bnk, FILE *outfile, int offset, const int length)
{
    size_t statusWritten = -1;

    if (bnk != NULL) {
        offset &= (BANKSIZE-1); /* ensure offset stays within boundary of bank */

        if (offset+length <= BANKSIZE) {
            statusWritten = fwrite (bnk->bankMem+offset, sizeof (char), length, outfile);
        }
    }

    return statusWritten;
}


/******************************************************************************
 * SetBankFileName(bank_t *bnk, char * fileName)
 *
 * Name the filename of the bank according to RomCombiner and Z88 Card
 * architecture rules (top bank of card is identified as 63, which is assigned
 * to filename extension).
 *
 * @brief define filename for bank according to RomCombiner convention
 * @param bnk reference to bank structure
 * @param fileName
 ******************************************************************************/
void SetBankFileName(bank_t *bnk, const char *fileName)
{
    char *tmpstr, *newFilename;
    char strconst[5];

    if (bnk != NULL && fileName != NULL) {
        if (bnk->bankFileName != NULL) {
            /* Ensure to release heap of old filename, before set new */
            free(bnk->bankFileName);
        }

        newFilename = strclone(fileName);
        tmpstr = strchr(newFilename,'.');
        if (tmpstr != NULL) {
            /* replace extension with RomCombiner bank number */
            *(tmpstr+1) = 0;
        } else {
            newFilename = strappend(newFilename, ".");
        }

        sprintf(strconst,"%d",bnk->bankNo);
        newFilename = strappend(newFilename, strconst);
        bnk->bankFileName = newFilename;
    }
}


/******************************************************************************
 * char *GetBankFileName(bank_t *bnk)
 *
 * @brief get pointer to bank filename
 * @param bnk reference to bank structure
 * @return
 ******************************************************************************/
char *GetBankFileName(bank_t *bnk)
{
    if (bnk != NULL) {
        return bnk->bankFileName;
    } else {
        return NULL;
    }
}


/******************************************************************************
 * crc32_t GetCRC32(bank_t *bnk)
 *
 * @brief calculate CRC-32 of bank contents
 * @param bnk reference to bank structure
 * @return 0xffffffff if bank reference is NULL
 ******************************************************************************/
crc32_t GetCRC32(bank_t *bnk)
{
    if (bnk != NULL) {
        return crc32(bnk->bankMem, BANKSIZE);
    } else {
        return 0xffffffff;
    }
}


/******************************************************************************
 * bool ContainsOzRomHeader(bank_t *bnk)
 *
 * @brief Validate if an 'OZ' watermark is located in specified (top) bank
 * @param bnk reference to bank structure
 * @return size of OZ ROM area in 16K banks if watermark found, else 0
 ******************************************************************************/
int ContainsOzRomHeader(bank_t *bnk)
{
    int romArea16K = 0;

    if (bnk != NULL) {
        if ( GetByte(bnk,0x3FFB) == 0x81 && GetByte(bnk,0x3FFE) == 'O' && GetByte(bnk,0x3FFF) == 'Z')
            romArea16K = GetByte(bnk,0x3FFC);
    }

    return romArea16K;
}


/******************************************************************************
 * bool ContainsAppHeader(bank_t *bnk)
 *
 * @brief Validate if this bank contains an Application Card Header
 * @param bnk reference to bank structure
 * @return size of application area in 16K banks if watermark found, else 0
 ******************************************************************************/
int ContainsAppHeader(bank_t *bnk)
{
    int applArea16K = 0;

    if (bnk != NULL) {
        if ( GetByte(bnk,0x3FFB) == 0x80 && GetByte(bnk,0x3FFE) == 'O' && GetByte(bnk,0x3FFF) == 'Z')
            applArea16K = GetByte(bnk,0x3FFC);
    }

    return applArea16K;
}


/******************************************************************************
 * bool ContainsFileHeader(bank_t *bnk)
 *
 * Check if there's a File header available at absolute bank, offset $3FC0-$3FFF.
 * (check for 'oz' file area header that is in slot 0 ROM and on external cards)
 *
 * @param bnk reference to bank structure
 * @return true, if a file header was found, otherwise false.
 */
bool ContainsFileHeader(bank_t *bnk) {
    if (bnk != NULL) {
        if ( GetByte(bnk,0x3FF7) == 0x01 && GetByte(bnk,0x3FFE) == 'o' && GetByte(bnk,0x3FFF) == 'z') {
            /* external slot 'oz' file area at top of card or below application card area */
            return true;
        } else {
            if ( GetByte(bnk,0x3FEE) == 'o' && GetByte(bnk,0x3FEF) == 'z') {
                /* File area in slot 0 ROM (top bank, part of 'OZ' ROM header) */
                return true;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}


/******************************************************************************
 * int getAppDorOffset(bank_t *bnk)
 *
 * @brief return offset to first DOR in 16K application bank
 * @param bnk reference to bank structure
 * @return -1 if no Application card header was recognized (or if bank reference is NULL)
 ******************************************************************************/
int GetAppDorOffset(bank_t *bnk)
{
    if (bnk != NULL) {
        if ( ContainsAppHeader(bnk) == 0 )
            return -1;
        else {
            // return bank offset to DOR
            return ((GetByte(bnk,0x3fc7) << 8) & 0x3f00) | (GetByte(bnk,0x3fc6) & 0xff);
        }
    } else {
        return -1;
    }
}


/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


/******************************************************************************
 * bank_t *newBank (void)
 *
 * @brief Initializes Bank structure
 * @return
 ******************************************************************************/
static bank_t *newBank (void)
{
    bank_t *bnk = (bank_t *) malloc (sizeof (bank_t));

    if (bnk != NULL) {
        bnk->bankNo = -1;
        bnk->bankMem = allocBankMem();
        bnk->bankFileName = NULL;
    }

    if (bnk->bankMem == NULL) {
        /* insufficient heap to allocate bank memory */
        free(bnk);
        bnk = NULL;
    }

    return bnk;
}


/******************************************************************************
 * mem_t *allocBankMem (void)
 *
 * @brief Allocates memory for contents of bank (16K)
 * @return pointer to allocated memory, or NULL
 ******************************************************************************/
static mem_t allocBankMem (void)
{
    mem_t mem = (mem_t) malloc (BANKSIZE);
    int i = 0;

    if (mem != NULL) {
        /* initialize memory as FF, an empty Eprom bank */
        for (; i<BANKSIZE; i++) {
            mem [i] = 0xff;
        }
    }

    return mem;
}
