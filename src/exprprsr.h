
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#ifndef EXPRPRSR_H
#define EXPRPRSR_H

#include "symbols.h"

typedef
struct pfixstack    {
    long                stackconstant;    /* stack structure used to evaluate postfix expressions */
    struct pfixstack   *prevstackitem;    /* pointer to previous element on stack */
} pfixstack_t;

typedef
struct postfixexpr  {
    struct postfixexpr *nextoperand;      /* pointer to next element in postfix expression */
    long               operandconst;
    enum symbols       operatortype;
    char               *id;               /* pointer to identifier */
    unsigned long      type;              /* type of identifier (local, global, rel. address or constant) */
} postfixexpr_t;

typedef
struct expression   {
    struct expression  *nextexpr;         /* pointer to next expression */
    postfixexpr_t      *firstnode;
    postfixexpr_t      *currentnode;
    unsigned long      rangetype;         /* range type of evaluated expression */
    char               *infixexpr;        /* pointer to a copy of compact, parsed ASCII infix expression */
    char               *infixptr;         /* pointer to current char in infix expression */
    struct sourcefile  *srcfile;          /* expr. in file 'srcfile' (also macro) */
    unsigned char      *exprlineptr;      /* pointer to expression in source file */
    int                curline;           /* expression in line of source file */
} expression_t;

typedef
struct exprlist     {
    expression_t        *firstexpr;       /* header of list of expressions in current module */
    expression_t        *currexpr;        /* point at last expression in list */
} expressions_t;

typedef
struct asmsymfunc   {
    char *drctv_mnem;                       /* inline function definition & implementation */
    symfunc drctv_func;
} exprfunc_t;

/* global functions */
expression_t *ParseNumExpr (void);
expressions_t *AllocExprHdr (void);
long EvalNumExpr (expression_t *pfixexpr);
void AddExpr2Module(expression_t *pfixexpr, unsigned long constrange);
void ReleaseExprns (expressions_t *express);
void FreeNumExpr (expression_t *pfixexpr);

/* bitmasks for expression evaluation in rangetype */
#define RANGE           0x000000FF                          /* bitmask 00000000 00000000 00000000 11111111   Range types are 0 - 255 */
#define NOTEVALUABLE    0x80000000                          /* bitmask 10000000 00000000 00000000 00000000   Expression is not evaluable */
#define EVALUATED       0x7FFFFFFF                          /* bitmask 01111111 11111111 11111111 11111111   Expression is not evaluable */
#define CLEAR_EXPRADDR  0xF7FFFFFF                          /* bitmask 11110111 11111111 11111111 11111111   Convert to constant expression */

#define RANGE_JROFFSET8  0
#define RANGE_8UNSIGN    'U'
#define RANGE_8SIGN      'S'
#define RANGE_16CONST    'C'
#define RANGE_16OFFSET   'O'
#define RANGE_32SIGN     'L'

#define MAX_EXPR_SIZE 254

#endif /* EXPRPRSR_H */
