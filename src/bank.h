
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com
  (C) Copyright Garry Lancaster 2012

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#ifndef BANK_H
#define BANK_H

#include <stdbool.h>
#include "crc32.h"

/**
 * These functions represents the Z88 16Kb Bank architecture, with the characteristics that it's
 * part of a Rom (internal chip on motherboard), an Eprom (internal chip on motherboard or as
 * part of an external Card) or a Flash Card.
 *
 * The bank implementation here focuses on providing a slot relative entity, with features
 * focused on loading / dumping / patching application binaries.
 *
 * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the 256 addressable
 * 16K banks in the Z88 4Mb memory model can be bound into the address space of the Z80
 * processor.
 *
 * Please refer to hardware section of the Developer's Notes for a more detailed description.
 * (https://cambridgez88.jira.com/wiki/display/DN/Interfacing+with+the+system)
 */

#define BANKSIZE 16384

typedef unsigned char *mem_t;
typedef unsigned char byte;


typedef struct {
    int             index;          /* index in card container, 0 = bottom of card */
    int             bankNo;
    mem_t           bankMem;
    char            *bankFileName;
} bank_t;

bank_t *Bank(int cardIndex, int bankNumber);
void FreeBank(bank_t *bnk);
void SetByte(bank_t *bnk, int addr, byte b);
void LoadBytes(bank_t *bnk, const mem_t block, int length, int offset);
void SetBankNumber(bank_t *bnk, int bNo);
void SetBankFileName(bank_t *bnk, const char * fileName);
char *GetBankFileName(bank_t *bnk);
byte GetByte(bank_t *bnk, int offset);
crc32_t GetCRC32(bank_t *bnk);
int GetBankNumber(bank_t *bnk);
int findString(bank_t *bnk, const char *str);
int GetAppDorOffset(bank_t *bnk);
int ContainsAppHeader(bank_t *bnk);
int ContainsOzRomHeader(bank_t *bnk);
bool IsEmpty(bank_t *bnk);
bool ContainsFileHeader(bank_t *bnk);
mem_t DumpBytes(bank_t *bnk, int offset, const int length);
size_t FwriteBytes(bank_t *bnk, FILE *outfile, int offset, const int length);

#endif /* BANK_H */
