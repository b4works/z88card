
/* -------------------------------------------------------------------------------------------------
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 * ------------------------------------------------------------------------------------------------*/

#ifndef FILES_H
#define FILES_H

#include <stdbool.h>

typedef
struct pathlist     {
    struct pathlist   *nextdir;           /* pointer to next directory path in list */
    char              *directory;         /* name of directory */
} pathlist_t;

struct sourcefile;

typedef
struct usedfile     {
    struct usedfile    *nextusedfile;
    struct sourcefile  *ownedsourcefile;
} usedsrcfile_t;

typedef
struct filelist     {
    struct filelist   *nextfile;          /* pointer to next file in list */
    char              *filename;          /* name of file */
} filelist_t;

typedef
struct includefile   {
    char               *fname;            /* pointer to file name of source file */
    long               filesize;          /* size of file in bytes */
    unsigned char      *filedata;         /* pointer to complete copy of file content */
} includefile_t;

typedef
struct sourcefile   {
    struct sourcefile  *prevsourcefile;   /* pointer to previously parsed source file */
    struct sourcefile  *newsourcefile;    /* pointer to new source file to be parsed */
    usedsrcfile_t      *usedsourcefile;   /* list of pointers to used files owned by this file */
    unsigned char      *lineptr;          /* pointer to beginning of current line being parsed */
    int                lineno;            /* current line number of current source file */
    char               *fname;            /* pointer to file name of current source file */
    FILE               *stream;           /* stream handle of opened file (optional) */
    long               filesize;          /* size of file in bytes */
    unsigned char      *filedata;         /* pointer to complete copy of file content */
    unsigned char      *memfileptr;       /* pointer to current character in memory file */
    bool               eol;               /* indicate if End Of Line has been reached */
    bool               eof;               /* indicate if End Of File has been reached */
    bool               includedfile;      /* if this is an INCLUDE'd file or not */
} sourcefile_t;

#if MSWIN
#define DIRSEP 0x5C         /* "\" */
#define ENVPATHSEP 0x3B     /* ";" */
#endif

#if UNIX
#define DIRSEP 0x2F         /* "/" */
#define ENVPATHSEP 0x3A     /* ":" */
#endif

#define MAX_FILENAME_SIZE 254

/* global functions */
FILE *OpenFile(char *filename, pathlist_t *pathlist, bool expandfilename);
char *AdjustPlatformFilename(char *filename);
char *AddFileExtension(const char *oldfilename, const char *extension);
char *Truncate2BaseFilename(char *filename);
unsigned char *CacheFile (sourcefile_t *srcfile, FILE *fd);
unsigned char *CacheFileData (sourcefile_t *srcfile, FILE *fd, size_t datasize);
unsigned char *MfTell(sourcefile_t *file);
unsigned char *LoadFile (char *filename);
int MfSeek(sourcefile_t *file, unsigned char *ptr);
int MfGetc(sourcefile_t *file);
int MfEof(sourcefile_t *file);
includefile_t *CacheIncludeFile (char *fname);
sourcefile_t *CurrentFile(void);
sourcefile_t *FindFile (sourcefile_t *srcfile, char *fname);
sourcefile_t *Newfile (sourcefile_t *curfile, char *fname);
sourcefile_t *Prevfile (void);
sourcefile_t *CreateFileFromString (char *name, int lineno, char *string);
void FreeCachedIncludeFile(includefile_t * inclfile);
void MfUngetc(sourcefile_t *file);
void AddPathNode (char *path, pathlist_t **plist);
void AddFileNameNode (char *filename, filelist_t **flist);
void ReleaseFile (sourcefile_t *srcfile);
void ReleaseFileData (sourcefile_t *srcfile);
void ReleaseCachedIncludeFiles(void);
size_t LengthOfFile(char *filename);

#endif // FILES_H
