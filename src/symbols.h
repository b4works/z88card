
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#ifndef SYMBOLS_H
#define SYMBOLS_H

#include "avltree.h"

/* Structured data types */
typedef long symvalue_t;                  /* symbol value is a 32bit integer */

enum symbols        { space, bin_and, dquote, squote, semicolon, comma, fullstop, strconq = fullstop, lparen, lcurly, lexpr, backslash,
                      rexpr, rcurly, rparen, plus, minus, multiply, divi, mod, bin_xor, assign, bin_or, bin_nor, colon = bin_nor,
                      bin_not, less, greater, log_not, cnstexpr, newline, power, lshift, rshift,
                      lessequal, greatequal, notequal, name, number, decmconst, hexconst, binconst, charconst, registerid,
                      negated, mod256, div256, nil, ifstatm, elsestatm, endifstatm, enddefstatm, colonlabel, asmfnname
                    };

typedef
struct node         {
    unsigned long      type;              /* type of symbol */
    char               *symname;          /* pointer to symbol identifier */
    symvalue_t         symvalue;          /* value of symbol (size dependents on type) */
    struct module      *owner;            /* pointer to module which owns symbol */
} symbol_t;

typedef symbol_t * (*symfunc) (void *);   /* ptr to symbol function returning pointer to symbol node */

/* global functions */
char       *AllocIdentifier (size_t len);
int        cmpidstr (symbol_t * kptr, symbol_t * p);
int        cmpidval (symbol_t * kptr, symbol_t * p);
symbol_t   *CreateSymNode (symbol_t *symptr);
symbol_t   *CreateSymbol (char *identifier, symvalue_t value, unsigned long symboltype, struct module *symowner);
void       DeclSymGlobal (char *identifier, unsigned long libtype);
void       DeclSymExtern (char *identifier, unsigned long libtype);
void       FreeSym (symbol_t * node);
symbol_t   *DefineDefSym (char *identifier, long value, avltree_t **root);
symbol_t   *DefineSymbol (char *identifier, symvalue_t value, unsigned long symboltype);
symbol_t   *GetSymPtr (char *identifier);
symbol_t   *FindSymbol (char *identifier, avltree_t * treeptr);

/* Bitmasks for symtype */
#define SYMDEFINED      0x01000000                          /* bitmask 00000001 00000000 00000000 00000000 */
#define SYMTOUCHED      0x02000000                          /* bitmask 00000010 00000000 00000000 00000000 */
#define SYMDEF          0x04000000                          /* bitmask 00000100 00000000 00000000 00000000 */
#define SYMADDR         0x08000000                          /* bitmask 00001000 00000000 00000000 00000000 */
#define SYMLOCAL        0x10000000                          /* bitmask 00010000 00000000 00000000 00000000 */
#define SYMXDEF         0x20000000                          /* bitmask 00100000 00000000 00000000 00000000 */
#define SYMXREF         0x40000000                          /* bitmask 01000000 00000000 00000000 00000000 */
#define SYMFUNC         0x80000000                          /* bitmask 10000000 00000000 00000000 00000000 */


/* #define SYM_BASE32      0x00000100                        symbol value is defined as 32bit native integer (long) */
/* #define SYM_BASE64      0x00000200                        symbol value is defined as 64bit integer (dlong_t) */
/* #define SYM_BASE128     0x00000400                        symbol value is defined as 128bit integer (qlong_t) */

#define SYMLOCAL_OFF    0xEFFFFFFF                          /* bitmask 11101111 11111111 11111111 11111111 */
#define XDEF_OFF        0xDFFFFFFF                          /* bitmask 11011111 11111111 11111111 11111111 */
#define XREF_OFF        0xBFFFFFFF                          /* bitmask 10111111 11111111 11111111 11111111 */
#define SYMTYPE         0x78000000                          /* bitmask 01111000 00000000 00000000 00000000 */
#define SYM_NOTDEFINED  0x00000000

#define MAX_NAME_SIZE 254

#endif /* SYMBOLS_H */
