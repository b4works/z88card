/******************************************************************************
 * This file is part of Z88Card.
 *
 * (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2016
 * (C) Copyright Garry Lancaster 2012
 *
 * Z88Card is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Z88Card;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "z88card.h"

/* external variables */
extern bool errorstatus;
extern int appCardBanks;
extern int romUpdateConfigFileType;
extern char *outputFilename, *ozUpdateConfigFilename;


/* private functionality */
static bank_t **AllocCardBanks(int totalBanks);
static card_t *AllocCard(void);
static char *GitRef(char *tmpBasefilename);
static bool LoadLargeBinary(card_t *card, mem_t codeBuffer, int codeBufferSize, int bankNo);



/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


/*****************************************************************************************
 * card_t *Card(int totalBanks, int romUpdateConfigFileType, char *outputFilename)
 *
 * @brief Create a card container, based on totalBanks and filename
 *
 * @param totalBanks
 * @param romUpdateConfigFileType
 * @param outputFilename
 *
 * @return allocated card object, or NULL if insufficient memory available
 ****************************************************************************************/
card_t *Card(int totalBanks, int romUpdateConfigFileType, char *outputFilename)
{
    int b;
    int baseSlotBankNo = 64-totalBanks;
    card_t *crd = AllocCard();
    char *gitCommitRef = NULL;
    char *tmpstrptr;

    /* make a copy of the original filename (to be modified) */
    outputFilename = strclone(outputFilename);
    tmpstrptr = strchr(outputFilename,'.');
    if (tmpstrptr != NULL) {
        /* get output filename without extension */
        *tmpstrptr = 0;
    }

    if ( (romUpdateConfigFileType == 3) || (romUpdateConfigFileType == 2) ) {
        /* OZ binaries are being generated, fetch latest Git reference, if Git and repo is available */

        gitCommitRef = GitRef(outputFilename);
        outputFilename = strappend(outputFilename, (romUpdateConfigFileType == 3) ? "s1": "s0");
        if (gitCommitRef != NULL) {
            gitCommitRef[7] = 0; /* Get shortform of Sha1 commit reference */

            outputFilename = strappend(outputFilename, "-");
            outputFilename = strappend(outputFilename, gitCommitRef);

            /* pre-define oz cfg.v3 output filename */
            ozUpdateConfigFilename = strclone(outputFilename);
            ozUpdateConfigFilename = strappend(ozUpdateConfigFilename, ".upd");
        }
    }

    if (crd != NULL) {
        crd->banks = AllocCardBanks(totalBanks);
        if (crd->banks != NULL) {
            crd->totalBanks = totalBanks;
            /* initialize banks */
            for (b=0; b < totalBanks; b++) {
                crd->banks[b] = Bank(b, baseSlotBankNo++);
                if (crd->banks[b] != NULL) {
                    SetBankFileName(crd->banks[b], outputFilename);
                }
            }

        } else {
            free(crd);
            crd = NULL;
        }
    }

    free(outputFilename);

    return crd;
}


/*****************************************************************************************
 * void FreeCard(card_t *card)
 *
 * @brief Release all allocated memory used by the card container
 * @param card
 ****************************************************************************************/
void FreeCard(card_t *card)
{
    int b;

    if (card != NULL) {
        for (b=0; b < card->totalBanks; b++) {
            if (card->banks[b] != NULL) {
                FreeBank(card->banks[b]);
                card->banks[b] = NULL;
            }
        }

        free(card->banks); /* release the array of pointers */
        free(card); /* and finally the data type */
    }
}


/*****************************************************************************************
 * @return bottom bank of card, or NULL if no banks are available
 ****************************************************************************************/
bank_t *GetBottomBank(card_t *card)
{
    if (card == NULL)
        return NULL;
    else
        return card->banks[0];
}


/*****************************************************************************************
 * @return top bank of card, or NULL if no banks are available
 ****************************************************************************************/
bank_t *GetTopBank(card_t *card)
{
    if (card == NULL)
        return NULL;
    else
        return card->banks[card->totalBanks-1];
}


/*****************************************************************************************
 * int GetNextExtAddress(int *extAddress)
 * "Internal" support method.
 *
 * Get the next adjacent extended address (24bit) pointer. The method ensures that
 * when the extended address pointer crosses a bank boundary, the absolute bank number
 * of the extended address is increased and the offset is reset to zero.
 * For example FE3FFF -> FF0000.
 *
 * This method is typically used by the File Area Management system but might be used
 * for other purposes.
 *
 * @param extAddress
 ****************************************************************************************/
void GetNextExtAddress(int *extAddress)
{
    int segmentMask = *extAddress & 0xC000;  /* preserve the segment mask, if any */
    int offset = *extAddress & 0x3FFF;       /* offset is within 16K boundary */
    int bankNo = *extAddress >> 16;          /* get absolute bank number */

    if (offset == 0x3FFF) {
        /* bank boundary will be crossed... */
        offset = 0x0000;
        bankNo++;
    } else {
        /* still within bank boundary... */
        offset++;
    }

    /* re-install the segment specifier, if any */
    offset = segmentMask | offset;

    /* finally update extended address... */
    *extAddress = (bankNo << 16) | offset;
}


byte CardGetByte(card_t *card, int extaddress)
{
    unsigned int offset = extaddress & 0x3FFF;  /* offset is within 16K boundary */
    unsigned int bankNo = extaddress >> 16;     /* get absolute bank number */

    if (card != NULL) {
        bankNo &= card->totalBanks-1;           /* code to be placed properly within the boundary of the allocated banks */
        return GetByte(card->banks[bankNo], offset);
    } else {
        return -1;
    }
}


void CardSetByte(card_t *card, int extaddress, int byte)
{
    unsigned int offset = extaddress & 0x3FFF;  /* offset is within 16K boundary */
    unsigned int bankNo = extaddress >> 16;     /* get absolute bank number */

    if (card != NULL) {
        bankNo &= card->totalBanks-1;           /* code to be placed properly within the boundary of the allocated banks */
        SetByte(card->banks[bankNo], offset, (byte & 0xff));
    }
}


/*****************************************************************************************
 * void ApplyCardId(card_t *card)
 *
 * @brief Generate cardId by calculating CRC32 of the banks in the card, using
 * bits 0..6 and 8..14 (bits 7 and 15 must be 0 in the card header).
 *
 * @param card the memory container
 ****************************************************************************************/
void ApplyCardId(card_t *card)
{
    int b;
    short cardId;
    crc32_t crc = 0xffffffff;

    if (card != NULL) {
        for (b=0; b < card->totalBanks; b++) {
            crc = GetCRC32(card->banks[b]);
        }

        cardId = (short) (crc & 0x7f7f);
        fprintf(stdout,"Generated card ID: %04X\n", cardId);

        SetByte(GetTopBank(card), 0x3ff8, (cardId & 0xff));
        SetByte(GetTopBank(card), 0x3ff9, ((cardId >> 8) & 0xff));
    }
}


/*****************************************************************************************
 * bool LoadCode(card_t **card, char *filename, mem_t codeBuffer, int codeBufferSize, unsigned int extaddress)
 *
 * Load the specified code into the final code space <b>banks</b> at bank, offset.
 * The function will check for bank boundary code loading overlap errors, and
 * report a message to stderr shell channel if an error occurs.
 *
 * @param card the memory container
 * @param codeBuffer pointer to start of binary code
 * @param codeBufferSize the size of the code to be loaded into the card container
 * @param bankNo bank of final code space to load the binary
 * @param offset offset within bank of final code space to load the binary
 * @return true, if code was succcessfully loaded into final code space.
 ****************************************************************************************/
bool LoadCode(card_t **card, char *filename, mem_t codeBuffer, int codeBufferSize, unsigned int extaddress)
{
    int b;
    unsigned int offset = extaddress & 0x3FFF;       /* offset is within 16K boundary */
    unsigned int bankNo = extaddress >> 16;          /* get absolute bank number */

    /* all directive have been specified, initialze card container, before loading any code */
    if (*card == NULL) {
        *card = Card(appCardBanks, romUpdateConfigFileType, outputFilename);
    }

    if (*card != NULL) {
        bankNo &= (*card)->totalBanks-1;  // code to be placed properly within the boundary of the allocated banks

        if (codeBufferSize > BANKSIZE) {
            return LoadLargeBinary(*card, codeBuffer, codeBufferSize, bankNo);
        } else {
            if ( (codeBufferSize + offset) > BANKSIZE ) {
                fprintf(
                            stderr,
                            "Code block in bank %02Xh, offset %04Xh, crosses 16K bank boundary by %d bytes when loading file '%s'!\n",
                            bankNo,
                            offset,
                            ((codeBufferSize + offset) - BANKSIZE),
                            filename
                        );
                errorstatus = true;
                return false;
            }

            for (b=0; b<codeBufferSize; b++) {
                if ( (GetByte((*card)->banks[bankNo], offset+b) & 0xff) != 0xff ) {
                    fprintf(stderr,"Code overlap was found at bank %02X, offset %04Xh, when loading file '%s'!\n", bankNo, offset+b, filename);
                    errorstatus = true;
                    return false;
                }
            }

            LoadBytes((*card)->banks[bankNo], codeBuffer, codeBufferSize, offset);
        }

        return true;
    } else {
        /* if for some reason there wasn't heap memory for the card container ... */
        fprintf(stderr, "Card couldn't be created !\n");
        errorstatus = true;
        return false;
    }
}


/*****************************************************************************************
 * bool Patch(card_t *card, int argc, char *argv[])
 *
 * @brief Parse the patch directive and patch bytes at specified card locations
 * patchargument[0] contains the patch address, followed by byte arguments.
 *
 * @param card the memory container
 * @param argc total amount of patch argument
 * @param argv string array of arguments
 ****************************************************************************************/
bool Patch(card_t *card, int argc, char *argv[])
{
    int patchAddr, bankNo, offset, patchByte;
    int argidx = 0;

    if (card != NULL) {
        if (argc < 2) {
            fprintf(stderr,"Insufficient patch address arguments.\n");
            return false;
        }

        if (card == NULL) {
            fprintf(stderr,"Card hasn't been created yet!\n");
            return false;
        }

        patchAddr = (int) StrToLong(argv[argidx++], 16);
        if (patchAddr == -1) {
            fprintf(stderr,"Illegal patch address!\n");
            return false;
        }

        for(; argidx < argc; argidx++) {
            bankNo = ((patchAddr & 0x3f0000) >> 16) & (card->totalBanks - 1);
            offset = patchAddr & 0x3fff;
            patchByte = (int) StrToLong(argv[argidx], 16);

            if (patchByte == -1) {
                fprintf(stderr,"Illegal patch address arguments.\n");
                return false;
            } else {
                if (card->banks[bankNo] != NULL) {
                    SetByte(card->banks[bankNo], offset, patchByte);
                } else {
                    fprintf(stderr,"Bank is not allocated!\n");
                    return false;
                }
            }

            patchAddr++;
        }

        return true;
    } else {
        return false;
    }
}


/*****************************************************************************************
 * void DumpCard(card_t *card, char *outputFilename)
 *
 * Dump the contents of the card as a complete binary file (typically an EPR file)
 *
 * @param card the memory container
 * @param outputFilename the name of the binary output file
 ****************************************************************************************/
void DumpCard(card_t *card, char *outputFilename)
{
    int b;
    FILE *cardFile;

    if (card != NULL) {
        if ((cardFile = fopen (AdjustPlatformFilename(outputFilename), "wb")) == NULL) {
            ReportIOError (outputFilename);
        } else {
            for (b=0; b < card->totalBanks; b++) {
                if (card->banks[b] != NULL) {
                    FwriteBytes(card->banks[b], cardFile, 0, BANKSIZE);
                }
            }
            fclose (cardFile);
        }
    }
}


/*****************************************************************************************
 * void DumpCardAsBanks(card_t *card)
 *
 * Dump the non-empty contents of the card as a individual bank files (RomCombiner notation)
 * to current path of where z88card tool is running.
 *
 * @param card the memory container
 ****************************************************************************************/
void DumpCardAsBanks(card_t *card)
{
    int b;
    FILE *bankFile;

    if (card != NULL) {
        for (b=0; b < card->totalBanks; b++) {
            if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) ) {
                if ((bankFile = fopen (AdjustPlatformFilename(GetBankFileName(card->banks[b])), "wb")) == NULL) {
                    ReportIOError (GetBankFileName(card->banks[b]));
                    return;
                } else {
                    FwriteBytes(card->banks[b], bankFile, 0, BANKSIZE);
                    fclose (bankFile);
                }
            }
        }
    }
}


/*****************************************************************************************
 * bool CreateRomUpdCfgFile_AppCard(card_t *card, char *romUpdateConfigFilename)
 *
 * @brief Create "romupdate.cfg" file for Application card.
 *
 * @return true if file was successfully created, otherwise false
 ****************************************************************************************/
bool CreateRomUpdCfgFile_AppCard(card_t *card, char *romUpdateConfigFilename)
{
    FILE *cfgFile;
    bank_t *bottomBank = GetBottomBank(card);

    if (card != NULL) {
        if (card->totalBanks > 1) {
            fprintf(stderr,"RomUpdate currently only supports single 16K application cards. 'romupdate.cfg' not created.\n");
            return false;
        }

        if (bottomBank == NULL) {
            fprintf(stderr,"Card contains no banks yet!\n");
            return false;
        }

        if (ContainsAppHeader(bottomBank) == 0) {
            fprintf(stderr,"Application card not recognized. 'romupdate.cfg' not created.\n");
            return false;
        } else {
            if ((cfgFile = fopen (AdjustPlatformFilename(romUpdateConfigFilename), "w")) == NULL) {
                ReportIOError (romUpdateConfigFilename);
                return false;
            } else {
                fprintf(cfgFile,"CFG.V1\n");
                fprintf(cfgFile,"; filename of a single 16K bank image, CRC (32bit), pointer to application DOR in 16K file image.\n");
                fprintf(cfgFile,"\"%s\",", GetBankFileName(bottomBank));
                fprintf(cfgFile,"$%08x,", (unsigned int) GetCRC32(bottomBank));
                fprintf(cfgFile,"$%04x\n", (unsigned int) GetAppDorOffset(bottomBank));

                fclose(cfgFile);
                return true;
            }
        }
    }

    return false;
}


/*****************************************************************************************
 * bool CreateRomUpdCfgFile_OzSlot(card_t *card, int slotNo, char *romUpdateConfigFilename)
 *
 * @brief Create "romupdate.cfg" file for OZ ROM in slot 0 or 1.
 *
 * @return true if file was successfully created, otherwise false
 ****************************************************************************************/
bool CreateRomUpdCfgFile_OzSlot(card_t *card, int slotNo, char *romUpdateConfigFilename)
{
    FILE *cfgFile;
    int b;
    int totalNonEmptyBanks = 0;
    int base_slot_bank = 0;

    if (card != NULL) {
        if (slotNo == 1)
            base_slot_bank = 64 - card->totalBanks;

        for (b=0; b < card->totalBanks; b++) {
            if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) )
                totalNonEmptyBanks++; /* count total number of banks to be blown to slot x */
        }

        if ((cfgFile = fopen (AdjustPlatformFilename(romUpdateConfigFilename), "w")) == NULL) {
            ReportIOError (romUpdateConfigFilename);
            return false;
        } else {
            fprintf(cfgFile,"CFG.V4\n");
            fprintf(cfgFile,"; OZ ROM, and total amount of banks to update.\n");
            fprintf(cfgFile,"CD,%d,\"OZ for slot %d\"\n", totalNonEmptyBanks, slotNo);
            fprintf(cfgFile,"; Bank file, CRC, destination bank in slot %d.\n", slotNo);

            for (b=0; b < card->totalBanks; b++) {
                if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) ) {
                    fprintf(cfgFile,"\"%s\",", GetBankFileName(card->banks[b]));
                    fprintf(cfgFile,"$%08x,", (unsigned int) GetCRC32(card->banks[b]));
                    fprintf(cfgFile,"$%02x\n", base_slot_bank + b);
                }
            }

            fclose(cfgFile);
            return true;
        }
    }

    return false;
}


/*****************************************************************************************
 * bool CreateOzUpdCfgFile_OzSlot(card_t *card, int slotNo, char *ozUpdateConfigFilename)
 *
 * @brief Create "ozupdate.cfg" file for OZ ROM in slot 0 or 1, used by Index in OZ v4.7+.
 *
 * @return true if file was successfully created, otherwise false
 ****************************************************************************************/
bool CreateOzUpdCfgFile_OzSlot(card_t *card, int slotNo, char *ozUpdateConfigFilename)
{
    FILE *cfgFile;
    int b;
    int totalNonEmptyBanks = 0;
    int base_slot_bank = 0;

    if (card != NULL) {
        if (slotNo == 1)
            base_slot_bank = 64 - card->totalBanks;

        for (b=0; b < card->totalBanks; b++) {
            if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) )
                totalNonEmptyBanks++; /* count total number of banks to be blown to slot x */
        }

        ozUpdateConfigFilename = AdjustPlatformFilename(ozUpdateConfigFilename);
        if ( ozUpdateConfigFilename == NULL) {
            ReportIOError (ozUpdateConfigFilename);
            return false;
        }
        if ((cfgFile = fopen (AdjustPlatformFilename(ozUpdateConfigFilename), "w")) == NULL) {
            ReportIOError (ozUpdateConfigFilename);
            return false;
        } else {
            fprintf(cfgFile,"CFG.V3\n");
            fprintf(cfgFile,"; OZ ROM for slot %d and total of %d banks to update.\n", slotNo, totalNonEmptyBanks);
            fprintf(cfgFile,"OZ.%d,%d\n", slotNo, totalNonEmptyBanks);
            fprintf(cfgFile,"; Bank file, CRC, destination bank in slot %d.\n", slotNo);

            for (b=0; b < card->totalBanks; b++) {
                if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) ) {
                    fprintf(cfgFile,"\"%s\",", GetBankFileName(card->banks[b]));
                    fprintf(cfgFile,"$%08x,", (unsigned int) GetCRC32(card->banks[b]));
                    fprintf(cfgFile,"$%02x\n", base_slot_bank + b);
                }
            }

            fclose(cfgFile);
            return true;
        }
    }

    return false;
}


/*****************************************************************************************
 * bool CreateRomUpdCfgFile_Card(card_t *card, char *romUpdateConfigFilename)
 *
 * @brief Create "romupdate.cfg" file for generic card
 *
 * @return true if file was successfully created, otherwise false
 ****************************************************************************************/
bool CreateRomUpdCfgFile_Card(card_t *card, char *romUpdateConfigFilename)
{
    FILE *cfgFile;
    int b;
    int totalNonEmptyBanks = 0;
    int base_slot_bank = 64 - card->totalBanks;

    if (card != NULL) {
        for (b=0; b < card->totalBanks; b++) {
            if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) )
                totalNonEmptyBanks++; /* count total number of banks to be blown to slot x */
        }

        if ((cfgFile = fopen (AdjustPlatformFilename(romUpdateConfigFilename), "w")) == NULL) {
            ReportIOError (romUpdateConfigFilename);
            return false;
        } else {
            fprintf(cfgFile,"CFG.V4\n");
            fprintf(cfgFile,"; Card and total amount of banks to update.\n");
            fprintf(cfgFile,"CD,%d,\"Card\"\n", totalNonEmptyBanks);
            fprintf(cfgFile,"; Bank file, CRC, destination bank in slot.\n");

            for (b=0; b < card->totalBanks; b++) {
                if ( (card->banks[b] != NULL) && (IsEmpty(card->banks[b]) == false) ) {
                    fprintf(cfgFile,"\"%s\",", GetBankFileName(card->banks[b]));
                    fprintf(cfgFile,"$%08x,", (unsigned int) GetCRC32(card->banks[b]));
                    fprintf(cfgFile,"$%02x\n", base_slot_bank + b);
                }
            }

            fclose(cfgFile);
            return true;
        }
    }

    return false;
}


/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


/*****************************************************************************************
 * bool LoadLargeBinary(card_t *card, mem_t codeBuffer, int codeBufferSize, int bankNo)
 *
 * Load file (binary) image into card container. The image will be loaded from the
 * specified bank and upwards in the card container.
 * The remaining banks of the card will be left untouched
 * (initialized as being empty). If the container has the same size as the file image, the
 * complete container is automatically filled in natural order.
 *
 * @param card the memory container
 * @param codeBuffer the binary file
 * @param codeBufferSize the size of the code to be loaded into the card container
 * @param bankNo start to load image from specified bank and upwards
 * @return true if binary code has been loaded into card container, otherwise false
 ****************************************************************************************/
static bool LoadLargeBinary(card_t *card, mem_t codeBuffer, int codeBufferSize, int bankNo)
{
    int copyOffset = 0;

    if (card != NULL) {
        if (codeBufferSize > (card->totalBanks * BANKSIZE)) {
            fprintf(stderr,"Binary image larger than specified loadmap buffer!\n");
            errorstatus = true;
            return false;
        }
        if (codeBufferSize % BANKSIZE > 0) {
            fprintf(stderr,"Binary image must be in 16K sizes!\n");
            errorstatus = true;
            return false;
        }
        if ((codeBufferSize % BANKSIZE) > card->totalBanks) {
            fprintf(stderr,"Binary image larger than specified loadmap buffer!\n");
            errorstatus = true;
            return false;
        }

        while (copyOffset < codeBufferSize) {
            LoadBytes(card->banks[bankNo++], codeBuffer+copyOffset, 0, BANKSIZE);
            copyOffset += BANKSIZE;
        }

        return true;
    } else {
        return false;
    }
}


/*****************************************************************************************
 * char *GitRef(char *tmpBasefilename)
 *
 * @brief Fetch latest HEAD commit reference of Git repository where z88card is running
 *
 * @return pointer to allocated string that contains Git commit reference, or NULL
 ****************************************************************************************/
static char *GitRef(char *tmpBasefilename)
{
#ifdef Q_OS_WIN32
    static const char syscmd[] = "cmd /c git log --pretty=format:%h -n 1";
#else
    #ifdef Q_OS_WIN64
        static const char syscmd[] = "cmd /c git log --pretty=format:%h -n 1";
    #else
        static const char syscmd[] = "git log --pretty=format:%h -n 1";
    #endif
#endif
    char *tmpFilename = AddFileExtension(tmpBasefilename, ".gref");
    char *gitcmd;
    char *gitRefStr = NULL; /* if git nor repository was available, return NULL */
    int gitStatus;

    if (tmpFilename == NULL) {
        return NULL;
    }

    gitcmd = (char *) malloc( strlen(syscmd) + 4 + strlen(tmpFilename) );
    if (gitcmd != NULL) {
        strcpy(gitcmd, syscmd);
        strcat(gitcmd, " > ");
        strcat(gitcmd, tmpFilename);
    } else {
        free(tmpFilename);
        return NULL;
    }


    gitStatus = system(gitcmd);                 /* try to call Git to get HEAD ref */
    switch(gitStatus) {
        case 0:
            gitRefStr = (char *) LoadFile(tmpFilename); /* fetch output from Git command */
            remove(tmpFilename);                        /* delete temporary file */
            break;
        default:
            fprintf(stderr,"Git tool failed or was not available!\n");
            errorstatus = true;
    }

    free(tmpFilename);                          /* free temporary strings */
    free(gitcmd);

    return gitRefStr;
}


static bank_t **AllocCardBanks(int totalBanks)
{
    return (bank_t **) malloc (totalBanks * sizeof(bank_t));
}


static card_t *AllocCard(void)
{
    return (card_t *) malloc (sizeof(card_t));
}

