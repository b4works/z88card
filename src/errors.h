
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/
#ifndef ERRORS_H
#define ERRORS_H

typedef enum {
    Err_FileIO,                     /* 0,  "File open/read error" */
    Err_Syntax,                     /* 1,  "Syntax error" */
    Err_SymNotDefined,              /* 2,  "symbol not defined" */
    Err_Memory,                     /* 3,  "Not enough memory" */
    Err_IntegerRange,               /* 4,  "Integer out of range" */
    Err_ExprSyntax,                 /* 5,  "Syntax error in expression" */
    Err_ExprBracket,                /* 6,  "Right bracket missing" */
    Err_ExprOutOfRange,             /* 7,  "Out of range" */
    Err_SrcfileMissing,             /* 8,  "Source filename missing" */
    Err_IllegalOption,              /* 9,  "Illegal option" */
    Err_UnknownIdent,               /* 10, "Unknown identifier" */
    Err_IllegalIdent,               /* 11, "Illegal label/identifier" */
    Err_MaxCodeSize,                /* 12, "Max. code size of %ld bytes reached" */
    Err_Status,                     /* 13, "errors occurred during processing" */
    Err_SymDefined,                 /* 14, "symbol already defined" */
    Err_SymDeclLocal,               /* 18, "symbol already declared local" */
    Err_SymDeclGlobal,              /* 19, "symbol already declared global" */
    Err_SymDeclExtern,              /* 20, "symbol already declared external" */
    Err_NoArguments,                /* 21, "No command line arguments" */
    Err_IllegalSrcfile,             /* 22, "Illegal source filename" */
    Err_SymDeclGlobalModule,        /* 23, "symbol declared global in another module" */
    Err_SymRedeclaration,           /* 24, "Re-declaration not allowed" */
    Err_SymResvName,                /* 28, "Reserved name" */
    Err_EnvVariable,                /* 31, "Environment variable not defined" */
    Err_IncludeFile,                /* 32, "Cannot include file recursively" */
    Err_ExprTooBig,                 /* 34, "Expression > 255 characters" */
    Err_totalMessages
} error_t;

void ReportError (sourcefile_t *file, error_t errnum);
void ReportIOError (char *filename);
char *GetErrorMessage(error_t errno);
void ReportSrcMessage  (sourcefile_t *file, int lineno, char *message);

#endif // ERRORS_H
