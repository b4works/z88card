#-----------------------------------------------------------------------------------------
#
# This file is part of Z88Card.
#
# (C) Copyright Gunther Strube (gstrube@gmail.com), 2005-2015
# (C) Copyright Garry Lancaster 2012
#
# Z88Card is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Z88Card;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
#
#-----------------------------------------------------------------------------------------

TEMPLATE = app

CONFIG += console
CONFIG -= qt

macx {
        # Don't create a Mac App bundle...
        CONFIG -= app_bundle
}

TARGET = z88card

DESTDIR = ../build
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

win32 {
        DEFINES += MSWIN
}

!win32 {
        DEFINES += UNIX
}

SOURCES += z88card.c main.c avltree.c files.c directives.c prsline.c exprprsr.c symbols.c bank.c card.c crc32.c errors.c \
    filecard.c
HEADERS += z88card.h avltree.h files.h directives.h prsline.h exprprsr.h symbols.h bank.h card.h crc32.h errors.h \
    filecard.h
