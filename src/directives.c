
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include "z88card.h"

/* externally defined variables */
extern bool errorstatus;
extern char *ident, *line, *outputFilename;
extern enum symbols sym;
extern modules_t *modulehdr;
extern pathlist_t *gIncludePath;
extern avltree_t *globalsymbols;
extern card_t *card;
extern filearea_t *filearea;

extern int appCardBanks, appCardSize;
extern int romUpdateConfigFileType;
extern bool splitBanks,generateCardId;


/* local functions */
static int identcmp (const char *idptr, const identfunc_t *symptr);
static int drctvfunccmp (const char *idptr, const exprfunc_t *symptr);
static long Evallogexpr (void);
static long Parsedefvarsize (long offset);
static long Parsevarsize (void);
static void CardSize (void);
static void Save16KOption(void);
static void GenerateCardIdKOption(void);
static void OutputfileDirective(void);
static void RomUpdateDirective(void);
static void OzUpdateDirective(void);
static void PatchDirective(void);
static void ListingOn (void);
static void ListingOff (void);
static void FileAreaMgr(void);
static int DefSp (void);
static symbol_t *ExprSymDay (void);
static symbol_t *ExprSymHour (void);
static symbol_t *ExprSymMinute (void);
static symbol_t *ExprSymMonth (void);
static symbol_t *DrctvSymZ88CardVersion (void);
static symbol_t *ExprSymSecond (void);
static symbol_t *ExprSymYear (void);

/* local variables */
static char stringconst[MAX_NAME_SIZE];

/* Pre-defined expression functions, defined here for quick validation */
static exprfunc_t exprfunctionlist[] = {
    {"$DAY",  (symbol_t *(*)(void *)) ExprSymDay},
    {"$EVAL",  (symbol_t *(*)(void *)) DirectiveEvalExpr},
    {"$HOUR", (symbol_t *(*)(void *)) ExprSymHour},
    {"$MINUTE", (symbol_t *(*)(void *)) ExprSymMinute},
    {"$MONTH", (symbol_t *(*)(void *)) ExprSymMonth},
    {"$VERSION", (symbol_t *(*)(void *)) DrctvSymZ88CardVersion},
    {"$SECOND", (symbol_t *(*)(void *)) ExprSymSecond},
    {"$YEAR", (symbol_t *(*)(void *)) ExprSymYear}
};

/* ------------------------------------------------------------------------------
   Pre-sorted array of directive mnemonics.
   ------------------------------------------------------------------------------ */
static identfunc_t directives[] = {
    {"DC", DEFC},
    {"DEFC", DEFC},
    {"DEFGROUP", DEFGROUP},
    {"DEFINE", DefSym},
    {"DEFSYM", DefSym},
    {"DEFVARS", DefVars},
    {"DV", DefVars},
    {"ELSE", ELSEstat},
    {"ENDDEF", ENDDEFstat},
    {"ENDIF", ENDIFstat},
    {"ENUM", DEFGROUP},
    {"ERROR", ERROR},
    {"EXTERN", DeclExternIdent},
    {"FILEAREA", FileAreaMgr},
    {"GENERATECARDID", GenerateCardIdKOption},
    {"GLOBAL", DeclGlobalIdent},
    {"IF", IFstat},
    {"INCLUDE", IncludeFile},
    {"LSTOFF", ListingOff},
    {"LSTON", ListingOn},
    {"OUTPUTFILE", OutputfileDirective},
    {"OZUPDATE", OzUpdateDirective},
    {"PATCH",PatchDirective},
    {"ROMUPDATE", RomUpdateDirective},
    {"SAVE16K", Save16KOption},
    {"SIZE", CardSize},
    {"UNDEFINE", UnDefineSym},
    {"VARAREA", DefVars},
    {"XDEF", DeclGlobalIdent},
    {"XREF", DeclExternIdent}
};


ptrfunc
LookupIdentifier (const char *identifier, identfunc_t idfuncs[], size_t totalid)
{
    identfunc_t *foundsym;

    if (sym == name) {
        foundsym = (identfunc_t *) bsearch (identifier, idfuncs, totalid, sizeof (identfunc_t), (fptr) identcmp);
        if (foundsym == NULL) {
            return NULL;
        } else {
            return foundsym->drctv_func;
        }
    } else {
        /* all directives are names, therefore nothing would be found anyway... */
        return NULL;
    }
}


symfunc
LookupExprFunction (const char *fnname)
{
    exprfunc_t *foundsym;

    foundsym = (exprfunc_t *) bsearch (fnname, exprfunctionlist, sizeof(exprfunctionlist)/sizeof(exprfunc_t), sizeof (exprfunc_t), (fptr) drctvfunccmp);
    if (foundsym == NULL) {
        return NULL;
    } else {
        return foundsym->drctv_func;
    }
}


ptrfunc
LookupDirective(char *id)
{
    return LookupIdentifier (id, directives, sizeof(directives)/sizeof(identfunc_t));
}



/* dummy function - not used but needed for C compiler & program logic... */
void
IFstat (void)
{
}


void
ELSEstat (void)
{
    sym = elsestatm;
}


void
ENDIFstat (void)
{
    sym = endifstatm;
}

void
ENDDEFstat (void)
{
    sym = enddefstatm;            /* and ENDDEF statement was reached */
}


/* multilevel conditional assembly logic */
void
Ifstatement (bool interpret)
{
    if (interpret == true) {
        /* evaluate IF expression */
        if (Evallogexpr () != 0) {
            do {
                /* expression is TRUE, interpret lines until ELSE or ENDIF */
                if (!MfEof (CurrentFile())) {
                    ParseLine (true);
                } else {
                    return;    /* end of file - exit from this IF level */
                }
            } while ((sym != elsestatm) && (sym != endifstatm));

            if (sym == elsestatm) {
                do {
                    /* then ignore lines until ENDIF ... */
                    if (!MfEof (CurrentFile())) {
                        ParseLine (false);
                    } else {
                        return;
                    }
                } while (sym != endifstatm);
            }
        } else {
            do {
                /* expression is FALSE, ignore until ELSE or ENDIF */
                if (!MfEof (CurrentFile())) {
                    ParseLine (false);
                } else {
                    return;
                }
            } while ((sym != elsestatm) && (sym != endifstatm));

            if (sym == elsestatm) {
                do {
                    if (!MfEof (CurrentFile())) {
                        ParseLine (true);
                    } else {
                        return;
                    }
                } while (sym != endifstatm);
            }
        }
    } else {
        do {
            /* don't evaluate IF expression and ignore all lines until ENDIF */
            if (!MfEof (CurrentFile())) {
                ParseLine (false);
            } else {
                return;    /* end of file - exit from this IF level */
            }
        } while (sym != endifstatm);
    }

    sym = nil;
}


void
DeclGlobalIdent (void)
{
    do {
        if (GetSym () == name) {
            DeclSymGlobal (ident, 0);
        } else {
            ReportError (CurrentFile(), Err_Syntax);
            return;
        }
    } while (GetSym () == comma);

    if (sym != newline) {
        ReportError (CurrentFile(), Err_Syntax);
    }
}



void
DeclGlobalLibIdent (void)
{
    if (GetSym () == name) {
        DeclSymGlobal (ident, SYMDEF);
    } else {
        ReportError (CurrentFile(), Err_Syntax);
        return;
    }
}


void
DeclExternIdent (void)
{
    do {
        if (GetSym () == name) {
            DeclSymExtern (ident, 0);    /* Define symbol as extern */
        } else {
            ReportError (CurrentFile(), Err_Syntax);
            return;
        }
    } while (GetSym () == comma);

    if (sym != newline) {
        ReportError (CurrentFile(), Err_Syntax);
    }
}


void
DefVars (void)
{
    expression_t *postfixexpr;
    long offset;

    GetSym ();

    if ((postfixexpr = ParseNumExpr ()) != NULL) {
        /* expr. must not be stored in relocatable file */
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (CurrentFile(), Err_SymNotDefined);
            FreeNumExpr (postfixexpr);
            return;
        } else {
            offset = EvalNumExpr (postfixexpr);  /* offset expression must not contain undefined symbols */
            FreeNumExpr (postfixexpr);
        }
    } else {
        return;    /* syntax error - get next line from file... */
    }

    GetSym (); /* read first symbol of new line */

    /* skip anything until we meet a name - but also allow for an empty DEFVARS */
    while (!MfEof (CurrentFile()) && sym != name && (sym != rcurly && LookupDirective(ident) != ENDDEFstat)) {
        if (CurrentFile()->eol == true) {
            ++CurrentFile()->lineno;
            CurrentFile()->eol = false;
        }

        GetSym ();
    }

    /* found a name definition - parse variable area definition until } or ENDDEF */
    while (!MfEof (CurrentFile()) && (sym != rcurly && LookupDirective(ident) != ENDDEFstat) ) {
        if (CurrentFile()->eol == true) {
            ++CurrentFile()->lineno;
            CurrentFile()->eol = false;
        } else {
            offset += Parsedefvarsize (offset);
        }

        GetSym();
    }
}


/* ---------------------------------------------------------------
   Size specifiers
     DS.B = 8 bit
     DS.W = 16 bit ('Word')
     DS.P = 24 bit ('Pointer')
     DS.L = 32 bit ('Long')
   --------------------------------------------------------------- */
static int
DefSp (void)
{
    if (GetSym () == fullstop)
        if (GetSym () == name)
            switch (ident[0]) {
            case 'B':
                return 1;

            case 'W':
                return 2;

            case 'P':
                return 3;

            case 'L':
                return 4;

            default:
                return -1;
            }
        else {
            ReportError (CurrentFile(), Err_Syntax);
            return -1;
        }
    else {
        ReportError (CurrentFile(), Err_Syntax);
        return -1;
    }
}


static long
Parsedefvarsize (long offset)
{
    long varoffset = 0;

    switch (sym) {
    case name:
        if (strcmp (ident, "DS") != 0) {
            DefineSymbol (ident, (symvalue_t) offset, 0);
            GetSym();
        }
        if (sym == name) {
            varoffset = Parsevarsize ();
        }
        break;

    default:
        ReportError (CurrentFile(), Err_Syntax);
    }

    return varoffset;
}


static long
Parsevarsize (void)
{
    expression_t *postfixexpr;
    long offset = 0, varsize, size_multiplier;

    if (strcmp (ident, "DS") != 0) {
        ReportError (CurrentFile(), Err_IllegalIdent);
    } else {
        if ((varsize = DefSp ()) == -1) {
            ReportError (CurrentFile(), Err_UnknownIdent);
        } else {
            GetSym ();

            if ((postfixexpr = ParseNumExpr ()) != NULL) {
                if (postfixexpr->rangetype & NOTEVALUABLE) {
                    ReportError (CurrentFile(), Err_SymNotDefined);
                } else {
                    size_multiplier = EvalNumExpr (postfixexpr);
                    offset = varsize * size_multiplier;
                }

                FreeNumExpr (postfixexpr);
            }
        }
    }

    return offset;
}


void
DEFGROUP (void)
{
    expression_t *postfixexpr;
    long enumconst = 0;

    /* skip anything until we meet a name */
    while (!MfEof (CurrentFile()) && GetSym () != name ) {
        SkipLine ();

        ++CurrentFile()->lineno;
        CurrentFile()->eol = false;
    }

    while (!MfEof (CurrentFile()) && (sym != rcurly && LookupDirective(ident) != ENDDEFstat) ) {
        if (CurrentFile()->eol == true) {
            ++CurrentFile()->lineno;
            CurrentFile()->eol = false;
        } else {
            do {
                if (sym == comma) {
                    GetSym ();    /* prepare for next identifier */
                }

                switch (sym) {
                case rcurly:
                case semicolon:
                case newline:
                    break;

                case name:
                    strcpy (stringconst, ident);      /* remember name */

                    if (GetSym () == assign) {
                        GetSym ();

                        if ((postfixexpr = ParseNumExpr ()) != NULL) {
                            if (postfixexpr->rangetype & NOTEVALUABLE) {
                                ReportError (CurrentFile(), Err_SymNotDefined);
                            } else {
                                enumconst = EvalNumExpr (postfixexpr);
                                DefineSymbol (stringconst, enumconst++, 0);
                            }
                            FreeNumExpr (postfixexpr);
                        }
                        GetSym ();    /* prepare for next identifier */
                    } else {
                        DefineSymbol (stringconst, enumconst++, 0);
                    }

                    break;

                default:
                    ReportError (CurrentFile(), Err_Syntax);
                    break;
                }
            } while (sym == comma);   /* get enum definitions separated by comma in current line */

            SkipLine ();    /* ignore rest of line */
        }
        GetSym ();
    }
}


void
DefSym (void)
{
    do {
        if (GetSym () == name) {
            DefineDefSym (ident, 1, &CurrentModule()->localsymbols);
        } else {
            ReportError (CurrentFile(), Err_Syntax);
            break;
        }
    } while (GetSym () == comma);
}


void
UnDefineSym(void)
{
    symbol_t *foundsym;

    do {
        if (GetSym () == name) {
            foundsym = FindSymbol(ident,CurrentModule()->localsymbols);
            if ( foundsym != NULL ) {
                DeleteNode (&CurrentModule()->localsymbols, foundsym, (int (*)(void *,void *)) cmpidstr, (void (*)(void *)) FreeSym);
            }
        } else {
            ReportError (CurrentFile(), Err_Syntax);
            break;
        }
    } while (GetSym () == comma);
}


/* ------------------------------------------------------------------------------
    DEFC <name> = <expression>
   ------------------------------------------------------------------------------ */
void
DEFC (void)
{
    expression_t *postfixexpr;
    long constant;

    do {
        if (GetSym () == name) {
            strcpy (stringconst, ident);  /* remember name */

            if (GetSym () == assign) {
                GetSym ();        /* get numerical expression */
                if ((postfixexpr = ParseNumExpr ()) != NULL) {
                    /* expression must not be stored in relocatable file */
                    if (postfixexpr->rangetype & NOTEVALUABLE) {
                        ReportError (CurrentFile(), Err_SymNotDefined);
                        break;
                    } else {
                        constant = EvalNumExpr (postfixexpr);    /* DEFC expression must not contain undefined symbols */
                        DefineSymbol (stringconst, constant, 0);
                    }
                    FreeNumExpr (postfixexpr);
                } else {
                    break;    /* syntax error - get next line from file... */
                }
            } else {
                ReportError (CurrentFile(), Err_Syntax);
                break;
            }
        } else {
            ReportError (CurrentFile(), Err_Syntax);
            break;
        }
    } while (sym == comma);       /* get all DEFC definition separated by comma */
}



void
IncludeFile (void)
{
    includefile_t *includefile;
    sourcefile_t *newfile;
    char includefilename[MAX_FILENAME_SIZE+1];

    if (GetSym () == dquote) {
        /* fetch filename of include file in current (cached) source file */
        MfGetfilename (includefilename);

        if (GetChar () != '\n') {
            SkipLine ();    /* skip rest of line and get ready for first char of next line */
        }

        if ((includefile = CacheIncludeFile(includefilename)) == NULL) {
            /* file wasn't available on disc, file I/O problems or no memory... */
            return;
        } else {
            /* INCLUDE file loaded (from disc or fetched from Include File Cache) */
            newfile = Newfile (CurrentFile(), includefilename);   /* Allocate new file into file information list (if memory available) */
            if (newfile != NULL) {

                /* link to cached include file */
                newfile->filedata = includefile->filedata;
                newfile->memfileptr = includefile->filedata;
                newfile->filesize = includefile->filesize;
                newfile->includedfile = true;

                /* focus to new file as the current one to be parsed... */
                CurrentModule()->cfile = newfile;

                ParseLoadMapFile ();                        /* parse include file */
                CurrentModule()->cfile = Prevfile ();       /* then get back to previous file... */
            }

            if (errorstatus)
                return;
        }
    } else {
        ReportError (CurrentFile(), Err_Syntax);
    }

    sym = newline;
}


void
ERROR (void)
{
    char errmsg[256];
    long constant, bytepos = 0;

    if (GetSym () == dquote) {
        while (!MfEof (CurrentFile()) && bytepos < 255) {
            constant = GetChar ();
            if (constant == EOF || constant == '\"') {
                sym = newline;
                CurrentFile()->eol = true;
                break;
            } else {
                errmsg[bytepos++] = (unsigned char) constant;
            }
        }

        errmsg[bytepos] = 0;
        ReportSrcMessage (CurrentFile(), CurrentFile()->lineno, errmsg);
    } else {
        ReportError (CurrentFile(), Err_Syntax);
    }
}


/* Dummy directive to successfully parse MPM definition source code */
static void
ListingOn (void)
{}


/* Dummy directive to successfully parse MPM definition source code */
static void
ListingOff (void)
{}


/* ----------------------------------------------------------------
    void MfGetfilename (char *filename)

    Get a filename from current memory file at current read position, onwards.
    The filename is stored / null-terminated in the buffer <filename>,
    truncated at #define MAX_FILENAME_SIZE chars.

    Terminating character of the filename is a space, EOL or a double quote.

    on return, the file pointer is updated to point at the beginning
    of the next line (or EOF if last line was read).
   ---------------------------------------------------------------- */
void MfGetfilename (char *filename)
{
    int c = 0,l;

    for (l = 0; l<=MAX_FILENAME_SIZE; ) {
        if (!MfEof (CurrentFile())) {
            c = GetChar ();
            if ((c == '\n') || (c == EOF)) {
                break;
            }

            if (!isspace(c) && c != ':' && c != '"' && c != ';') {
                /* read filename until a space, a comment or a double quote */
                /* swallow '#' (old syntax no longer used), but use all other chars in filename */
                if (c != '#') {
                    filename[l++] = (char) c;
                }
            } else {
                break;
            }
        } else {
            break;                /* fatal - end of file reached! */
        }
    }
    filename[l] = '\0';           /* null-terminate file name string */
    AdjustPlatformFilename(filename);

    MfUngetc(CurrentFile());      /* evaluate last char by caller... */
}



static int
identcmp (const char *idptr, const identfunc_t *symptr)
{
    return strcmp (idptr, symptr->drctv_mnem);
}

static int
drctvfunccmp (const char *idptr, const exprfunc_t *symptr)
{
    return strcmp (idptr, symptr->drctv_mnem);
}


static long
Evallogexpr (void)
{
    expression_t *postfixexpr;
    long constant = 0;

    GetSym ();                    /* get logical expression */
    if ((postfixexpr = ParseNumExpr ()) != NULL) {
        constant = EvalNumExpr (postfixexpr);
        FreeNumExpr (postfixexpr);     /* remove linked list, expression evaluated */
    }
    return constant;
}



/* --------------------------------------------------------------------------
    symbol_t *DirectiveEvalExpr (void *expr)

    Evaluate expression in specified <expr> string and update result in
    global $EVAL (directive function) symbol.

    returns NULL, if expression evaluation failed.
   -------------------------------------------------------------------------- */
symbol_t *
DirectiveEvalExpr (void *expr)
{
    symbol_t *evalFnPtr = FindSymbol ("$EVAL", globalsymbols);
    sourcefile_t *stringFile, *origFile;
    expression_t *postfixexpr;
    int exprevaluated = 1;
    long constant = 0;

    if (expr == NULL) {
        /* no expression specified */
        return NULL;
    }

    if ( (stringFile = CreateFileFromString(CurrentFile()->fname, CurrentFile()->lineno, expr)) == NULL) {
        /* couldn't create temp. memory file... */
        return NULL;
    } else {
        origFile = CurrentFile();
        CurrentModule()->cfile = stringFile;

        GetSym ();                    /* parse expression in string */
        if ((postfixexpr = ParseNumExpr ()) != NULL) {
            if (postfixexpr->rangetype & NOTEVALUABLE) {
                exprevaluated = 0;
            } else {
                constant = EvalNumExpr (postfixexpr);
            }

            FreeNumExpr (postfixexpr);     /* remove linked list, expression evaluated */
        }

        CurrentModule()->cfile = origFile;
    }

    if (exprevaluated) {
        evalFnPtr->symvalue = constant;
        return evalFnPtr;
    } else {
        return NULL;
    }
}


static symbol_t *
ExprSymDay (void)
{
    time_t asmtime;
    struct tm *localtm;
    symbol_t *symptr;

    time(&asmtime);
    localtm = localtime(&asmtime);
    symptr = FindSymbol ("$DAY", globalsymbols);
    symptr->symvalue = localtm->tm_mday;

    return symptr;
}


static symbol_t *
ExprSymHour (void)
{
    time_t asmtime;
    struct tm *localtm;
    symbol_t *symptr;

    time(&asmtime);
    localtm = localtime(&asmtime);
    symptr = FindSymbol ("$HOUR", globalsymbols);
    symptr->symvalue = localtm->tm_hour;

    return symptr;
}


static symbol_t *
ExprSymMinute (void)
{
    time_t asmtime;
    struct tm *localtm;
    symbol_t *symptr;

    time(&asmtime);
    localtm = localtime(&asmtime);
    symptr = FindSymbol ("$MINUTE", globalsymbols);
    symptr->symvalue = localtm->tm_min;

    return symptr;
}


static symbol_t *
ExprSymSecond (void)
{
    time_t asmtime;
    struct tm *localtm;
    symbol_t *symptr;

    time(&asmtime);
    localtm = localtime(&asmtime);
    symptr = FindSymbol ("$SECOND", globalsymbols);
    symptr->symvalue = localtm->tm_sec;

    return symptr;
}


static symbol_t *
ExprSymMonth (void)
{
    time_t asmtime;
    struct tm *localtm;
    symbol_t *symptr;

    time(&asmtime);
    localtm = localtime(&asmtime);
    symptr = FindSymbol ("$MONTH", globalsymbols);
    symptr->symvalue = localtm->tm_mon+1;

    return symptr;
}


static symbol_t *
DrctvSymZ88CardVersion (void)
{
    symbol_t *symptr;

    symptr = FindSymbol ("$VERSION", globalsymbols);
    symptr->symvalue = VERSION_NUMBER;

    return symptr;
}


static symbol_t *
ExprSymYear (void)
{
    time_t asmtime;
    struct tm *localtm;
    symbol_t *symptr;

    time(&asmtime);
    localtm = localtime(&asmtime);
    symptr = FindSymbol ("$YEAR", globalsymbols);
    symptr->symvalue = localtm->tm_year+1900;

    return symptr;
}


/* Loadmap file 'size' directive */
static void CardSize (void)
{
    expression_t *postfixexpr;

    GetSym ();                    /* get logical expression */
    if ((postfixexpr = ParseNumExpr ()) != NULL) {
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (CurrentFile(), Err_SymNotDefined);
        } else {
            appCardSize = (int) EvalNumExpr (postfixexpr);

            if ( (appCardSize != 16) && (appCardSize != 32) && (appCardSize != 64) &&
                (appCardSize != 128) && (appCardSize != 256) && (appCardSize != 512) && (appCardSize != 1024)) {
                appCardSize = -1;
            }

            if (appCardSize != -1) {
                appCardBanks = appCardSize / 16;
            } else {
                fprintf(stderr,"Illegal card size. Use only: 16K, 32K, 64K, 128K, 256K, 512K or 1024K.\n");
                errorstatus = true;
            }
        }

        FreeNumExpr (postfixexpr);     /* remove linked list, expression evaluated */
    }
}


/* Loadmap file 'save16k' directive */
static void Save16KOption(void)
{
    splitBanks = true;
}


/* Loadmap file 'generateCardId' directive */
static void GenerateCardIdKOption(void)
{
    generateCardId = true;
}


/* Loadmap file 'outputfile <filename>' directive */
static void OutputfileDirective(void)
{
    unsigned char *lptr = MfTell (CurrentFile());
    char filename[MAX_FILENAME_SIZE+1];

    filename[0] = 0; /* null-terminate filename (in case its not specified in loadmap file) */

    if (GetSym () != dquote) {
        MfSeek(CurrentFile(), lptr); /* filename wasn't optionally defined with double quotes */
    }

    while(isspace(GetChar())); /* Skip whitespace until first char of filename */
    MfUngetc(CurrentFile());

    /* fetch filename for card output file */
    MfGetfilename (filename);

    if (strlen(filename) == 0) {
        ReportError (CurrentFile(), Err_Syntax);
    } else {
        outputFilename = strclone(filename); /* assign pointer to global output filename */
    }
}


/* Loadmap file 'romupdate <cfgtype>' directive */
static void RomUpdateDirective(void)
{
    /* always output 16K bank files when instructed to create a romupdate config file */
    splitBanks = true;

    if (GetSym() == name) {
        /* get config type argument */
        if ( strcmp(ident,"APP") == 0) {
            romUpdateConfigFileType = 1;
            return;
        } else if ( strcmp(ident,"CARD") == 0) {
            romUpdateConfigFileType = 4;
            return;
        } else if ( strcmp(ident,"OZ") == 0) {
            GetSym(); /* . */
            GetSym(); /* '0' or '1' */
            if ( strcmp(ident,"0") == 0) {
                romUpdateConfigFileType = 2;
                return;
            } else if ( strcmp(ident,"1") == 0) {
                romUpdateConfigFileType = 3;
                return;
            }
        }
    }

    /* unknown config type */
    ReportError (CurrentFile(), Err_Syntax);
}


/* Loadmap file 'ozupdate <oz.0|1>' directive, generating both
 * CFG.V3 "ozupdate.cfg" file and
 * generic CFG.V4 "romupdate.cfg" files
*/
static void OzUpdateDirective(void)
{
    /* always output 16K bank files when instructed to create a romupdate config file */
    splitBanks = true;

    if (GetSym() == name) {
        if ( strcmp(ident,"OZ") == 0) {
            GetSym(); /* . */
            GetSym(); /* '0' or '1' */
        } else if ( strcmp(ident,"OZ") == 0) {
            GetSym(); /* . */
            GetSym(); /* '0' or '1' */
            if ( strcmp(ident,"0") == 0) {
                romUpdateConfigFileType = 2; /* generate "ozupdate.cfg" + "romupdate.cfg" file for slot 0 */
                return;
            } else if ( strcmp(ident,"1") == 0) {
                romUpdateConfigFileType = 3; /* generate "ozupdate.cfg" + "romupdate.cfg" file for slot 1 */
                return;
            }
        }
    }

    /* unknown config type */
    ReportError (CurrentFile(), Err_Syntax);
}


/* Loadmap file 'patch <address>' directive */
static void PatchDirective(void)
{
    char *patchArgs[16];
    char stringconst[8];
    char patchaddrhexformat[] = "%06x";
    char patchbytehexformat[] = "%02x";
    int  i,patchArgc = 0;
    int  patcharg;
    expression_t *postfixexpr;

    GetSym ();
    do {
        if ((postfixexpr = ParseNumExpr ()) != NULL) {
            if (postfixexpr->rangetype & NOTEVALUABLE) {
                ReportError (CurrentFile(), Err_SymNotDefined);
            } else {
                patcharg = (unsigned int) EvalNumExpr (postfixexpr);
                if (patchArgc == 0)
                    sprintf(stringconst,patchaddrhexformat,patcharg);
                else
                    sprintf(stringconst,patchbytehexformat,patcharg);

                patchArgs[patchArgc++] = strclone(stringconst);
            }

            FreeNumExpr (postfixexpr);     /* expression evaluated */
        }

        if (sym == comma)
            GetSym ();
    } while ((sym != newline) && (patchArgc<16) && (errorstatus == false));

    if (errorstatus == false) {
        Patch(card, patchArgc, (char **) patchArgs);

        /* releases strings */
        for (i=0; i<patchArgc; i++) {
            free(patchArgs[i]);
        }
    }
}


static void FileAreaMgr (void)
{
    char hostfileName[MAX_FILENAME_SIZE+1];
    char fileEntryName[MAX_FILENAME_SIZE+1];
    int fileLength;
    mem_t fileImage;

    /* guarantee that that the card structure is established, before working on file area directives */
    if (card == NULL) {
        card = Card(appCardBanks, romUpdateConfigFileType, outputFilename);
    }

    if (GetSym () == newline) {
        /* no filename arguments simply indicates to create a file area header on card */
        if (filearea == NULL) {
            filearea = InitFileArea(card);
        }
        return;
    }

    if (sym == dquote) {
        /* fetch filename of host file to be loaded into file area */
        MfGetfilename (hostfileName);
        GetSym();
    } else {
        ReportError (CurrentFile(), Err_Syntax);
        return;
    }

    if (GetSym () == dquote) {
        /* fetch file entry name, always beginning with "/" */
        MfGetfilename (fileEntryName);
        AdjustEntryFilename(fileEntryName); /* ensure that path separators are '/' in file entry names */
        GetSym();
    } else {
        ReportError (CurrentFile(), Err_Syntax);
        return;
    }

    /* load host file and store it into the file area */
    if (filearea == NULL) {
        filearea = InitFileArea(card); /* initialize file area before loading files... */
    }

    if (filearea != NULL) {
        fileLength = LengthOfFile(hostfileName);
        if (fileLength > 0) {
            fileImage = LoadFile(hostfileName);
            if (fileImage != NULL) {
                StoreFileEntry(filearea, fileEntryName, fileImage, fileLength);
                free(fileImage);
            }
        }
    }
}


void LoadBinaryDirective (void)
{
    char binaryFilename[MAX_FILENAME_SIZE+1];
    expression_t *postfixexpr;
    unsigned int address;
    int LengthOfCode;
    mem_t codeBuffer;

    MfGetfilename (binaryFilename);

    GetSym ();                    /* get address expression */
    if ((postfixexpr = ParseNumExpr ()) != NULL) {
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (CurrentFile(), Err_SymNotDefined);
        } else {
            address = (unsigned int) EvalNumExpr (postfixexpr);

            LengthOfCode = LengthOfFile(binaryFilename);
            if (LengthOfCode > 0) {
                codeBuffer = LoadFile(binaryFilename);
                if (codeBuffer != NULL) {
                    LoadCode(&card, binaryFilename, codeBuffer, LengthOfCode, address);
                    free(codeBuffer);
                }
            }
        }

        FreeNumExpr (postfixexpr);     /* remove linked list, expression evaluated */
    }
}
