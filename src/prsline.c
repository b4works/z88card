/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "z88card.h"


/* globally defined variables */
enum symbols sym, ssym[] = {
    space, bin_and, dquote, squote, semicolon, comma, fullstop,
    lparen, lcurly, lexpr, backslash, rexpr, rcurly, rparen, plus, minus, multiply, divi, mod, bin_xor,
    assign, bin_or, bin_nor, bin_not,less, greater, log_not, cnstexpr
};

const char separators[] = " &\"\';,.({[\\]})+-*/%^=|:~<>!#";

/* Global text buffers, allocated by AllocateTextBuffers() during startup */
char *ident = NULL;
char *line = NULL;

/* local defined variables */
static bool cstyle_comment = false;

/* local functions */
static int CheckBaseType(int chcount);
static void CharToIdent(const char c, const int index);
static int __gcLf (void);
static char *stringrev(char *str);


/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


void ParseLine (bool interpret)
{
    line[0] = '\0';             /* preset line buffer to being empty */

        CurrentFile()->lineptr = MfTell (CurrentFile()); /* preserve the beginning of the current line, for reference */
        ++CurrentFile()->lineno;

        CurrentFile()->eol = false; /* reset END OF LINE flag */
        GetSym ();                  /* and fetch first symbol on line */

        switch (sym) {
            case name:
                /* Parse source code line based on current <ident> name, which can be either a directive or a filename */
                if (LookupDirective(ident) == NULL) {
                    if (interpret == true) {
                        /* everything not a directive is a filename for the loadmap, back to beginning of line.. */
                        MfSeek(CurrentFile(), CurrentFile()->lineptr);

                        /* read filename and address, then load it into the card container */
                        LoadBinaryDirective ();
                    } else {
                        SkipLine ();
                    }
                } else {
                    ParseDirective (interpret);
                }
                break;

            case newline:
                break;        /* empty line, get next... */

            default:
                if ( interpret == true ) {
                    /* everything not a directive is a filename for the loadmap, back to beginning of line.. */
                    MfSeek(CurrentFile(), CurrentFile()->lineptr);

                    /* read filename and address, then load it into the card container */
                    LoadBinaryDirective ();
                }
        }
}



/* ---------------------------------------------------------------------------
    void ParseDirective (enum flag interpret)

    Parse source code line based on current collected <ident> name
    (of sym type = name), which can be either a directive or a filename
   --------------------------------------------------------------------------- */
void ParseDirective (const bool interpret)
{
    ptrfunc function;

    /* fprintf(stderr, "ParseDirective(%s): '%s'\n", CurrentFile()->fname, ident); */

    if ((function = LookupDirective(ident)) == NULL) {
        if (interpret == true) {
            /* it is not a directive */
            ReportError (CurrentFile(), Err_UnknownIdent);
        }
        SkipLine ();
    } else {
        /* current identifier contains a directive */
        if (function == IFstat) {
            if (interpret == false) {
                SkipLine ();
            }
            Ifstatement (interpret);
        } else if ((function == ELSEstat) || (function == ENDIFstat)) {
            (function)();
            SkipLine ();
        } else {
            if (interpret == true) {
                (function)();
            }
            SkipLine ();
        }
    }
}


void ParseCmdLineDefSym(char *symbol)
{
    int i;

    strncpy (ident, symbol, MAX_NAME_SIZE-1);    /* Copy argument string */
    if (!isalpha(ident[0]) && ident[0] != '_') {
        ReportError (NULL, Err_IllegalIdent);    /* symbol must begin with alpha */
        return;
    }

    i = 0;
    while (ident[i] != '\0') {
        if (strchr (separators, ident[i]) == NULL) {
            if (!isalnum (ident[i]) && ident[i] != '_') {
                ReportError (NULL, Err_IllegalIdent);        /* illegal char in identifier */
                return;
            } else {
                ident[i] = toupper (ident[i]);
            }
        } else {
            ReportError (NULL, Err_IllegalIdent);        /* illegal char in identifier */
            return;
        }
        ++i;
    }

    DefineDefSym (ident, 1, &CurrentModule()->localsymbols);
}


/* Copyright (c) 2012, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
* * Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* * Redistributions in binary form must reproduce the above
* copyright notice, this list of conditions and the following
* disclaimer in the documentation and/or other materials provided
* with the distribution.
* * Neither the name of The Linux Foundation nor the names of its
* contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Edited for long integer, by Gunther Strube, 2014.
*/
int ltoasc(long num, char *str, int len, int base)
{
    int i = 0;
    int digit;

    if (len == 0)
        return -1;

    do {
        digit = num % base;
        if (digit < 0xA)
            str[i++] = '0' + digit;
        else
            str[i++] = 'A' + digit - 0xA;
        num /= base;
    } while (num && (i < (len - 1)));

    if (i == (len - 1) && num)
        return -1;

    str[i] = '\0';
    stringrev(str);

    return 0;
}


/* ---------------------------------------------------------------------------
    char *trim(char *str)

    Trim leading and trailing white spaces of string and retain original
    pointer to beginning of string.

    Implemented by http://stackoverflow.com/users/19719/indiv
   --------------------------------------------------------------------------- */
char *trim(char *str)
{
    size_t len = 0;
    char *frontp = str - 1;
    char *endp = NULL;

    if ( str == NULL )
        return NULL;

    if ( str[0] == '\0' )
        return str;

    len = strlen(str);
    endp = str + len;

    /* Move the front and back pointers to address
     * the first non-whitespace characters from
     * each end.
     */
    while( isspace(*(++frontp)) );
    while( isspace(*(--endp)) && endp != frontp );

    if( str + len - 1 != endp )
        *(endp + 1) = '\0';
    else if( frontp != str &&  endp == frontp )
        *str = '\0';

    /* Shift the string so that it starts at str so
     * that if it's dynamically allocated, we can
     * still free it on the returned pointer.  Note
     * the reuse of endp to mean the front of the
     * string buffer now.
     */
    endp = str;
    if( frontp != str )
    {
        while( *frontp ) *endp++ = *frontp++;
        *endp = '\0';
    }

    return str;
}


/*!
 * \brief Duplicate S, returning an identical malloc'd string
 * \param s
 * \return
 */
char *
strclone (const char *s)
{
    size_t len = strlen (s) + 1;
    void *new = malloc (len);

    if (new == NULL)
        return NULL;

    return (char *) memcpy (new, s, len);
}


/* ---------------------------------------------------------------------------
    int strnicmp (const char *s1, const char *s2, size_t n)

    Compare no more than N characters of S1 and S2,
    returning less than, equal to or greater than zero
    if S1 is lexicographically less than, equal to or
    greater than S2.

    Original algorithm, Copyright GNU LIBC, adapted with toupper()
   --------------------------------------------------------------------------- */
int strnicmp (const char *s1, const char *s2, size_t n)
{
  unsigned char c1 = '\0';
  unsigned char c2 = '\0';

  if (n >= 4)
    {
      size_t n4 = n >> 2;
      do
      {
        c1 = toupper((unsigned char) *s1++);
        c2 = toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
        c1 = toupper((unsigned char) *s1++);
        c2 = toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
        c1 = toupper((unsigned char) *s1++);
        c2 = toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
        c1 = toupper((unsigned char) *s1++);
        c2 = toupper((unsigned char) *s2++);
        if (c1 == '\0' || c1 != c2)
          return c1 - c2;
      } while (--n4 > 0);
      n &= 3;
    }

  while (n > 0)
    {
      c1 = toupper((unsigned char) *s1++);
      c2 = toupper((unsigned char) *s2++);
      if (c1 == '\0' || c1 != c2)
        return c1 - c2;
      n--;
    }

  return c1 - c2;
}


/* ---------------------------------------------------------------------------
    char *substr(char *s, char *find)

    Locate <find> string in s, case independent.

    Original strstr() algorithm, Copyright GNU LIBC, adapted with toupper()
   --------------------------------------------------------------------------- */
char *substr(char *s, char *find)
{
    char c, sc;
    size_t len;

    if ((c = toupper(*find++)) != 0) {
        len = strlen(find);
        do {
            do {
                if ((sc = toupper(*s++)) == 0)
                    return NULL;
            } while (sc != c);
        } while (strnicmp(s, find, len) != 0);
        s--;
    }
    return s;
}


/* ---------------------------------------------------------------------------
    char *append(const char *curstr, const char *newstr)

    Re-allocate string with newstr appended to curstr.

    Return pointer (= curstr) to re-allocated memory, or NULL
   --------------------------------------------------------------------------- */
char *strappend( char *curstr, const char *newstr)
{
    char *tmpstr = NULL;

    if ( (curstr != NULL) && (newstr != NULL) ) {
        tmpstr = (char *) realloc( curstr, strlen(curstr) + strlen(newstr) + 1);
        if (tmpstr != NULL) {
            strcat(tmpstr, newstr);
        }
    }

    return tmpstr;
}


/* ----------------------------------------------------------------
    unsigned char *GetLine (unsigned char *startlineptr)

    From specified position in current memory file, read a source
    code line into <line> buffer (truncated if line is bigger than
    line buffer).

    Current file position is maintained.

    '\n' byte is applied to buffer if a CR/LF/CRLF variation line
    feed is found.

    return pointer to start of line buffer or NULL (if NULL were specified)
   ---------------------------------------------------------------- */
unsigned char *GetLine (unsigned char *startlineptr)
{
    unsigned char *origlineptr;
    int l,c;

    if (startlineptr != NULL) {
        /* remember current file position */
        origlineptr = MfTell (CurrentFile());

        /* position to specified file pointer in current file */
        MfSeek(CurrentFile(), startlineptr);

        c = '\0';
        for (l=0; (l<MAX_LINE_BUFFER_SIZE) && (c!='\n'); l++) {
            c = GetChar();
            if (c != EOF) {
                line[l] = c;    /* line feed inclusive */
            } else {
                break;
            }
        }
        line[l] = '\0';

        /* resume file position */
        MfSeek(CurrentFile(), origlineptr);

        return (unsigned char *) line;
    } else {
        return NULL;
    }
}


int GetChar (void)
{
    int c = __gcLf();

    /* continuous line or escape sequence? */
    if ( c == '\\' ) {
            c = __gcLf();

            /* Also handle escape sequences, http://en.wikipedia.org/wiki/Escape_sequences_in_C  */
            switch(c) {
                case '\n':
                    /* There was an EOL just after the \, return a space and update line counter */
                    c = 0x20;
                    CurrentFile()->eol = false;
                    break;
                case '\\':
                    c = '\\'; /* interpret \\ as \ */
                    break;
                case 'n':
                    c = 0x0a; /* interpret as Ascii Line feed */
                    break;
                case 'r':
                    c = 0x0d; /* interpret as Ascii Carriage return */
                    break;
                case 't':
                    c = 0x09; /* interpret as Ascii horisontal tab */
                    break;
                case 'a':
                    c = 0x07; /* interpret as Ascii Alarm */
                    break;
                case 'b':
                    c = 0x08; /* interpret as Ascii Backspace */
                    break;
                case 'f':
                    c = 0x0c; /* interpret as Ascii Formfeed */
                    break;
                case '\'':
                    c = '\''; /* ' */
                    break;
                case '\"':
                    c = '\"'; /* " */
                    break;
                default:
                    /* continuous line marker, skip until EOL */
                    MfUngetc(CurrentFile());
                    SkipLine();
                    if (CurrentFile()->eol == true) {
                        CurrentFile()->eol = false;
                    }
            }
    }

    return c; /* return all other characters */
}


/* ----------------------------------------------------------------
    int fGetChar (FILE *fptr)

    Return a character from opened file with CR/LF/CRLF parsing capability.
    '\n' byte is return if a CR/LF/CRLF variation line feed is found.
   ---------------------------------------------------------------- */
int fGetChar (FILE *fptr)
{
    int c;

    c = fgetc (fptr);
    if (c == 13) {
        /* Mac line feed found, poll for MSDOS line feed */
        c = fgetc (fptr);
        if (c != 10) {
            ungetc (c, fptr);    /* push non-line-feed character back into file */
        }

        c = '\n'; /* always return the symbolic '\n' for line feed */
    } else if (c == 10) {
        c = '\n';    /* UNIX line feed */
    }

    return c; /* return all other characters */
}


enum symbols GetSym (void)
{
    char *instr;
    int c, chcount = 0, endbracket = 0;
    unsigned char *ptr;

    ident[0] = '\0';

    if (CurrentFile()->eol == true) {
        sym = newline;
        return sym;
    }

    for (;;) {
        /* Ignore leading white spaces, if any... */
        if (MfEof (CurrentFile())) {
            sym = newline;
            CurrentFile()->eol = true;
            return newline;
        } else {
            c = GetChar ();
            if ((c == '\n') || (c == EOF) || (c == '\x1A')) {
                sym = newline;
                CurrentFile()->eol = true;
                return newline;
            } else if (!isspace (c)) {
                break;
            }
        }
    }

    instr = strchr (separators, c);
    if (instr != NULL) {
        sym = ssym[instr - separators]; /* index of found char in separators[] */
        if (sym == semicolon) {
            SkipLine ();        /* ';' or '#', ignore comment line, prepare for next line */
            sym = newline;
        }

        switch (sym) {
        case multiply:
            c = GetChar ();
            if (c == '*') {
                sym = power;    /* '**' */
            } else if (c == '/') {
                /* c-style end-comment, continue parsing after this marker */
                cstyle_comment = false;
                GetSym();
            } else {
                /* push this character back for next read */
                MfUngetc(CurrentFile());
            }
            break;

        case divi:         /* c-style comment begin */
            c = GetChar ();
            if (c == '*') {
                cstyle_comment = true;
                SkipLine ();    /* ignore comment block */
                GetSym();
            } else {
                /* push this character back for next read */
                MfUngetc(CurrentFile());
            }
            break;

        case less:         /* '<' */
            c = GetChar ();
            switch (c) {
            case '<':
                sym = lshift;       /* '<<' */
                break;

            case '>':
                sym = notequal;    /* '<>' */
                break;

            case '=':
                sym = lessequal;       /* '<=' */
                break;

            default:
                /* '<' was found, push this character back for next read */
                MfUngetc(CurrentFile());
                break;
            }
            break;

        case greater:          /* '>' */
            c = GetChar ();
            switch (c) {
            case '>':
                sym = rshift;       /* '>>' */
                break;

            case '=':
                sym = greatequal;      /* '>=' */
                break;

            default:
                /* '>' was found, push this character back for next read */
                MfUngetc(CurrentFile());
                break;
            }
            break;

        default:
            break;
        }

        if (cstyle_comment == true) {
            SkipLine ();
            return GetSym();
        } else {
            return sym;
        }
    }

    /* before going deeper into symbol parsing, check if we're in a c-style comment block... */
    if (cstyle_comment == true) {
        SkipLine ();
        return GetSym();
    }

    CharToIdent((char) toupper (c), chcount++);
    switch (c) {
    case '$':
        sym = hexconst;
        break;

    case '@':
        sym = binconst;
        break;

    case '_':                   /* leading '_' allowed for name definitions */
        sym = name;
        break;

    case '#':
        sym = name;
        break;

    default:
        if (isdigit (c)) {
            sym = decmconst;  /* a decimal number found */
        } else {
            if (isalpha (c)) {
                sym = name;   /* an identifier found */
            } else {
                sym = nil;    /* rubbish ... */
            }
        }
        break;
    }

    /* Read identifier until space or legal separator is found */
    if (sym == name) {
        for (;;) {
            if (MfEof (CurrentFile())) {
                break;
            } else {
                c = GetChar ();
                if ((c != EOF) && (!iscntrl (c)) && (strchr (separators, c) == NULL)) {
                    if (!isalnum (c)) {
                        if (c != '_') {
                            sym = nil;
                            break;
                        } else {
                            /* underscore in identifier */
                            CharToIdent('_', chcount++);
                        }
                    } else {
                        CharToIdent((char) toupper (c), chcount++);
                    }
                } else {
                    if ( c != ':' ) {
                        MfUngetc(CurrentFile());   /* puch character back for next read */
                    } else {
                        sym = colonlabel;
                    }
                    break;
                }
            }
        }
    } else {
        for (;;) {
            if (MfEof (CurrentFile())) {
                break;
            } else {
                c = GetChar ();
                if ((c != EOF) && !iscntrl (c) && (strchr (separators, c) == NULL)) {
                    CharToIdent((char) toupper (c), chcount++);
                } else {
                    MfUngetc(CurrentFile());   /* puch character back for next read */

                    CharToIdent(0, chcount);
                    /* validate if ident might be a number constant as with a trailing h, d or b */
                    chcount = CheckBaseType(chcount);

                    if (sym == asmfnname) {
                        if (LookupExprFunction (ident) != NULL) {
                            ptr = MfTell(CurrentFile());
                            /* an expression function name was recognized */
                            /* include function body as part of name, if it is specified */

                            /* skip white space until first real character after function name.. */
                            /* check if a [ ] is placed after the function name, and read it as part of the name.. */
                            while ( ((c = GetChar ()) != EOF) && isspace(c) ) {
                                ;
                            }

                            switch(c) {
                                case '(':
                                    endbracket = ')';
                                    break;
                                case '[':
                                    endbracket = ']';
                                    break;
                            }

                            if (c == '(' || c == '[') {
                                CharToIdent('[', chcount++);

                                /* read function body, collect all valid chars and skip all white spaces until ')' or ']' */
                                while ( ((c = GetChar ()) != EOF) && (c != endbracket) ) {
                                    if (!iscntrl(c) && !isspace(c)) {
                                        CharToIdent((char) toupper (c), chcount++);
                                    }
                                }
                                CharToIdent(']', chcount++);
                            } else {
                                /* optional function body was not specified */
                                /* resume read position, just at byte after function name */
                                MfSeek(CurrentFile(), ptr);
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    ident[chcount] = '\0';
    return sym;
}



void SkipLine (void)
{
    int c;

    if (CurrentFile()->eol == false) {
        while (!MfEof (CurrentFile())) {

            c = MfGetc (CurrentFile());
            if (c == 13) {
                /* Mac line feed found, poll for MSDOS line feed */
                c = MfGetc (CurrentFile());
                if (c != 10) {
                    MfUngetc(CurrentFile());  /* push non-line-feed character back into file */
                }

                c = '\n'; /* always return the symbolic '\n' for line feed */
            } else if (c == 10) {
                c = '\n';    /* UNIX line feed */
            }

            if ((c == '\n') || (c == EOF)) {
                break;    /* get to beginning of next line... */
            }
            if ( c == '*' ) {
                c = MfGetc (CurrentFile());
                if ( c == '/' ) {
                    if (cstyle_comment == true) {
                        cstyle_comment = false;
                        return;
                    }
                } else {
                    MfUngetc (CurrentFile());    /* puch character back for next read */
                }
            }
        }

        CurrentFile()->eol = true;
    }
}


void fSkipLine (FILE *fptr)
{
    int c;

    while (!feof (fptr)) {
        c = fGetChar (fptr);
        if ((c == '\n') || (c == EOF)) {
            break;    /* get to beginning of next line... */
        }
        if ( c == '*' ) {
            c = fGetChar (fptr);
            if ( c == '/' ) {
                if (cstyle_comment == true) {
                    cstyle_comment = false;
                    return;
                }
            } else {
                ungetc (c, fptr);    /* puch character back for next read */
            }
        }
    }
}


/** ------------------------------------------------------------------------------------------
 * int StrToLong(char *str)
 *
 * @brief
 * Parse integer value of radix from string and return long integer
 *
 * @param str
 * @return -1 if the value was illegal or badly formed
 */
long StrToLong(char *str, int radix)
{
    char *temp = NULL;
    long lv;

    lv = (long) strtoll(str, &temp, radix);
    if (*temp != '\0' || errno == ERANGE) {
        return -1;
    } else {
        return lv;
    }
}


/* ---------------------------------------------------------------------------
   Evaluate the current [ident] buffer for integer constant. The following
   type specifiers are recognized:

        0xhhh , $hhhh   hex constant
        @bbbb           binary constant

        constant is evaluated by default as decimal, if no type specifier is used.

   The evaluated constant is returned as a long integer.

   *evalerr byref argument is set to 0 when constant was successfully evaluated,
   otherwise 1.
   --------------------------------------------------------------------------- */
long GetConstant (char *evalerr)
{
    short size;
    char *temp = NULL;
    long lv;

    errno = 0;            /* reset global error number */
    lv = 0;
    *evalerr = 0;         /* preset evaluation return code to no errors */
    size = strlen (ident);

    if ((sym != hexconst) && (sym != binconst) && (sym != decmconst)) {
        *evalerr = 1;
        return lv;       /* syntax error - illegal constant definition */
    }

    if ( ident[0] == '0' && toupper(ident[1]) == 'X') {
        /* fetch hex constant specified as 0x... */
        lv = (long) strtoll((ident + 2), &temp, 16);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }

    if ( ident[0] == '0' && toupper(ident[1]) == 'B' && toupper(ident[size-1] == 'H')) {
        /* fetch hex constant specified as 0b..H (truncate 'H' specifier) */
        ident[size-1] = '\0';
        lv = (long) strtoll(ident, &temp, 16);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }

    if ( ident[0] == '0' && toupper(ident[1]) == 'B' && toupper(ident[size-1] != 'H')) {
        /* fetch binary constant specified as 0b... */
        lv = (long) strtoll((ident + 2), &temp, 2);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }

    if (sym != decmconst) {
        if ((--size) == 0) { /* adjust size of non decimal constants without leading type specifier */
            *evalerr = 1;
            return lv;     /* syntax error - no constant specified */
        }
    }

    switch (ident[0]) {
    case '@':
        /* Binary integer are identified with leading @ */
        lv = (long) strtoll((ident + 1), &temp, 2);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */

    case '$':
        /* Hexadecimal integers may be specified with leading $ */
        lv = (long) strtoll((ident + 1), &temp, 16);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */

        /* Parse default decimal integers */
    default:
        lv = (long) strtoll(ident, &temp, 10);
        if (*temp != '\0' || errno == ERANGE) {
            *evalerr = 1;
        }

        return lv; /* returns 0 on error */
    }
}


/* ---------------------------------------------------------------------------
   int AllocateTextBuffers()

   Allocate dynamic memory for line and identifier buffers.

   Return 1 if allocated, or if no room in system
   --------------------------------------------------------------------------- */
int AllocateTextBuffers()
{
    if ( (ident = AllocIdentifier(MAX_NAME_SIZE+1)) == NULL ) {
        ReportError (NULL, Err_Memory);
        return 0;
    }

    if ( (line = AllocIdentifier(MAX_LINE_BUFFER_SIZE+1)) == NULL ) {
        ReportError (NULL, Err_Memory);
        FreeTextBuffers();
        return 0;
    }

    return 1;
}


/* ---------------------------------------------------------------------------
   void FreeTextBuffers()

   Release previously allocated dynamic memory for line and identifier buffers.
   --------------------------------------------------------------------------- */
void FreeTextBuffers()
{
    if ( ident != NULL ) {
        free(ident);
        ident = NULL;
    }

    if ( line != NULL ) {
        free(line);
        line = NULL;
    }
}



/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */



/* ----------------------------------------------------------------
    static int __gcLf (void) {

    Return a character from current (cached) memory file with
    CR/LF/CRLF parsing capability.

    Handles continuous line '\' marker (skip physical EOL)
    and returns value of escape sequences (\n, \\, \r, \t, \a, \b, \f, \', \")

    '\n' byte is returned if a CR/LF/CRLF variation line feed is found.
   ---------------------------------------------------------------- */
static int __gcLf (void) {
    int c = MfGetc (CurrentFile());
    if (c == 13) {
        /* Mac line feed found, poll for MSDOS line feed */
        if ( MfGetc (CurrentFile()) != 10) {
            MfUngetc(CurrentFile());  /* push non-line-feed character back into file */
        }
        c = '\n';    /* always return UNIX line feed for CR or CRLF */
    }

    return c;
}


static void CharToIdent(const char c, const int index)
{
    if (index <= MAX_NAME_SIZE) {
        ident[index] = c;
    }
}



/* ----------------------------------------------------------------
  Add a trailing $ to a sequence of hex digits in [ident]
  of chcount characters

  returns chcount+1 (the ident has increased with one character)
   ---------------------------------------------------------------- */
static int Conv2hextype(int chcount)
{
    int i;

    for ( i = chcount; i >= 0 ; i-- ) {
        ident[i+1] = ident[i];
    }

    ident[0] = '$';
    sym = hexconst;

    return chcount+1;
}


/* return position of non-hex digit, or full size of ident */
int checkHexNumber(int chcount)
{
    int i;

    /* Check for this to be a hex constant here */
    for ( i=0; i < chcount; i++ ) {
        if ( !isxdigit(ident[i])  ) {
            return i;
        }
    }

    return i;
}


/* ----------------------------------------------------------------
   Identify Hex-, binary and decimal constants in [ident] of
        $xxxx or 0x or xxxxH (hex format)
        0Bxxxxx or xxxxB     (binary format)
        xxxxD                (decimal format)

        and

   Identify assembler functions as $ (converted to $PC) or $name
   (which is not a legal hex constant)
   ---------------------------------------------------------------- */
static int CheckBaseType(int chcount)
{
    int  i;
    bool binaryDigits = true;

    if (ident[0] == '$') {
        if (strlen(ident) > 1) {
            for (i = 1; i < chcount; i++) {
                if (isxdigit (ident[i]) == 0) {
                    sym = asmfnname;
                    return chcount;
                }
            }

            sym = hexconst;
            return chcount;
        }
    }

    /* If it's not a hex digit straight off then reject it */
    if ( !isxdigit(ident[0]) || chcount < 2 ) {
        return chcount;
    }

    /* C style hex number */
    if ( chcount > 2 && strnicmp(ident,"0x",2) == 0 ) {
        /* 0x hex constants are evaluated by GetConstant() */
        sym = hexconst;
        return chcount;
    }

    /* z88card uses default hex constants in loadmap files as 24bit hex constants, bboooo, without type identifier */
    /* bb = bank number, oooo = bank offset */
    if ( chcount == 6 && checkHexNumber(chcount) == 6) {
        /* a 6 character constant has been successfully parsed above as containing hex digits */
        return Conv2hextype(chcount);
    }

    /* C style hex number, ambiguous constant 0bxxxH! */
    if ( chcount > 2 && strnicmp(ident,"0b",2) == 0 && ident[chcount-1] == 'H') {
        /* hex constants are evaluated by GetConstant() */
        sym = hexconst;
        return chcount;
    }

    /* C style binary number */
    if ( chcount > 2 && strnicmp(ident,"0b",2) == 0 ) {
        /* Check for binary constant (only 0 or 1) */
        for ( i = 2; i <  chcount ; i++ ) {
            if ( ident[i] != '0' && ident[i] != '1' ) {
                binaryDigits = false;
                break;
            }
        }

        /* 0b binary constants are evaluated by GetConstant() */
        if (binaryDigits == true) {
            sym = binconst;
            return chcount;
        }
    }

    /* Check for this to be a hex constant here */
    i = checkHexNumber(chcount);
    if ( i == (chcount-1) ) {
        /* Convert xxxxH hex constants to $xxxxx */
        if ( toupper(ident[i]) == 'H' ) {
            return Conv2hextype(chcount-1);
        } else {
            /* If we reached end of hex digits and the last one wasn't a 'h', then something is wrong */
            return chcount;
        }
    }

    /* Check for binary constant (only 0 or 1) */
    for ( i = 0; i <  chcount ; i++ ) {
        if ( ident[i] != '0' && ident[i] != '1' ) {
            binaryDigits = false;
            break;
        }
    }

    if ( (binaryDigits == false) && (i == (chcount-1)) && (toupper(ident[i]) == 'B') ) {
        /* Convert xxxxB binary constants to @xxxx constants */
        for ( i = (chcount-1); i >= 0 ; i-- ) {
            ident[i+1] = ident[i];
        }
        ident[0] = '@';
        sym = binconst;
        return chcount;
    }

    /* Check for decimal (we default to it in anycase.. but */
    for ( i = 0; i <  chcount ; i++ ) {
        if ( !isdigit(ident[i]) ) {
            break;
        }
    }
    if ( i == (chcount-1) && toupper(ident[i]) == 'D' ) {
        sym = decmconst;
        return chcount-1; /* chop off the 'D' trailing specifier for decimals */
    }

    /* No hex, binary or decimal base types were recognized, return without change */
    return chcount;
}


static char *stringrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}
