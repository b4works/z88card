
/* -------------------------------------------------------------------------------------------------

  This file is part of Z88Card.

  Copyright (C) 1991-2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include "symbols.h"

typedef void (*ptrfunc) (void);           /* ptr to function returning void */
typedef int (*fptr) (const void *, const void *);
typedef ptrfunc (*mnemfunc) (const char *);

typedef
struct identfunc       {
    char *drctv_mnem;                       /* identifier definition & function implementation */
    ptrfunc drctv_func;
} identfunc_t;

/* global functions */
symfunc LookupExprFunction (const char *ident);
ptrfunc LookupIdentifier (const char *identifier, identfunc_t identfuncs[], size_t totalid);
ptrfunc LookupDirective(char *id);

symbol_t *DirectiveEvalExpr (void *);

void MfGetfilename (char *filename);
void LoadBinaryDirective (void);
void ALIGN(void);
void BINARY (void);
void CPU(void);
void DEFB (void), DEFC (void), DEFM (void), DEFMZ (void), DEFW (void), DEFP (void), DEFL (void);
void DEFGROUP (void), DefVars (void), DEFS (void);
void ERROR(void), EQU(void), LINE(void);
void DeclExternIdent (void), DeclGlobalIdent (void), DeclLibIdent (void), DeclGlobalLibIdent (void);
void DeclModule (void);
void DefSym (void), UnDefineSym(void);
void IFstat (void), ELSEstat (void), ENDIFstat (void);
void ENDDEFstat (void);
void IncludeFile (void);
void Ifstatement (bool interpret);
