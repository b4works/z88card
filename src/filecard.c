
/* -------------------------------------------------------------------------------------------------

  File Area functionality to load files into card.
  This file is part of Z88Card.

  Copyright (C) 2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "z88card.h"

/* external variables */
extern bool errorstatus;

/* local variables and functions */
static flacardhdr_t *ReadFileAreaHeader(bank_t *bnk);
static filearea_t *CreateFileArea(card_t *card);
static filearea_t *LoadFileArea(card_t *card, bank_t *fhdrBnk);
static filearea_t *InitFileAreaStructure(void);
static filearea_t *AllocFileAreaStructure(void);
static flacardhdr_t *AllocFileAreaHeaderStructure(void);
static fileentry_t *LoadFileEntry(filearea_t *fa, int fileEntryPtr);
static fileentry_t *AllocFileEntryStructure(void);
static fileentry_t **AllocFileEntries(fileentry_t **entries, const int currentEntries, const int totalEntries);
static fileentry_t *GetFileEntryByName(filearea_t *fa, char *filename);
static void FreeFileEntryStructure(fileentry_t *fe);
static void FreeFileEntries(fileentry_t **entries, int totalEntries);
static void FreeFileAreaHeaderStructure(flacardhdr_t *flchdr);
static void WriteFileAreaHeader(filearea_t *fa, bank_t *bnk);
static void WriteFileEntryHeader(filearea_t *fa, int fileEntryPtr, char *fileEntryName, int fileImageSize);
static void ScanFileEntries(filearea_t *fa);
static void MarkFileEntryAsDeleted(filearea_t *fa, fileentry_t *fe);
static int GetFreeSpacePtr(filearea_t *fa);
static int ptrToInt(int extAddress);
static int intToPtr(int i);
static bool FormatFileArea(filearea_t *fa, bank_t *bnk);
static bool RecordFileEntry(filearea_t *fa, fileentry_t *fe);



/* ------------------------------------------------------------------------------------------
    Public functions
   ------------------------------------------------------------------------------------------ */


/********************************************************************************
 * filearea_t *CreateFileArea(card_t *card)
 *
 * @brief Initialize a file area in allocated card (create on empty card, or read file area info)
 * @param card
 * @return pointer to allocated file area meta data, or NULL (format failed, or no memory)
 ********************************************************************************/
filearea_t *InitFileArea(card_t *card)
{
    filearea_t *fa = NULL;
    bank_t *fhdrBank = NULL;

    if (card != NULL) {
        fhdrBank = GetFileHeaderBank(card);
        if (fhdrBank == NULL) {
            /* file area not found on card, create it */
            fa = CreateFileArea(card);
        } else {
            /* File area header has been identified on card, parse file entries, if any */
            fa = LoadFileArea(card, fhdrBank);
        }
    }
    
    return fa;
}


/********************************************************************************
 * bool StoreFileEntry(filearea_t *fa, char *fileEntryName, mem_t fileImage, int fileImageSize)
 *
 * @brief Store a hosted file
 * @param card
 * @return pointer to allocated file area meta data, or NULL (format failed, or no memory)
 ********************************************************************************/
bool StoreFileEntry(filearea_t *fa, char *fileEntryName, mem_t fileImage, int fileImageSize)
{
    bool storedStatus = false;
    int b, fileEntryPtr, fileImagePtr;
    fileentry_t *fe, *oldfe;

    if (fa != NULL) {
        if ((1 + (int) strlen(fileEntryName) + 4 + fileImageSize) <= GetFileAreaFreeSpace(fa)) {
            /* first find a match of an existing entry - later to be marked as deleted */
            oldfe = GetFileEntryByName(fa, fileEntryName);

            fileEntryPtr = GetFreeSpacePtr(fa);
            WriteFileEntryHeader(fa, fileEntryPtr, fileEntryName, fileImageSize);
            fe = LoadFileEntry(fa, fileEntryPtr);
            if (fe != NULL) {
                if (RecordFileEntry(fa, fe) == true) {
                    /* finally, write the file image to the file area */
                    fileImagePtr = fe->fileImagePtr;
                    for (b = 0; b < fileImageSize; b++) {
                        CardSetByte(fa->card, fileImagePtr, fileImage[b]);
                        GetNextExtAddress(&fileImagePtr);
                    }

                    storedStatus = true;
                } else {
                    /* problems in recording the parsed file entry in FAT, abort */
                    FreeFileEntryStructure(fe);
                }
            } else {
                fprintf(stderr,"No Room. File entry could not be recorded.\n");
                errorstatus = true;
            }
        } else {
            /* not enough free space for file entry header and file image */
            fprintf(stderr,"File Area has not sufficient free space to store '%s' file.\n", fileEntryName);
            errorstatus = true;
        }
    }

    if (storedStatus == true) {
        if (oldfe != NULL) {
            /* a new version of file entry was stored in file area, mark old as deleted */
            MarkFileEntryAsDeleted(fa, oldfe);
        }
    }

    return storedStatus;
}


/*****************************************************************************************
 * bool isFileCard(card_t *card)
 *
 * @brief Validates if the card is used only as file area ('oz' watermark in top bank)
 * @param card
 ****************************************************************************************/
bool isFileCard(card_t *card)
{
    if (card != NULL) {
        return ContainsFileHeader(GetTopBank(card));
    } else {
        return false;
    }
}


/*****************************************************************************************
 * bank_t *getFileHeaderBank(card_t *card)
 *
 * @brief return bank in card that contains the 'oz' file header, or NULL if none found
 * @param card
 ****************************************************************************************/
bank_t *GetFileHeaderBank(card_t *card)
{
    int b;

    if (card != NULL) {
        for (b=0; b < card->totalBanks; b++) {
            if (card->banks[b] != NULL) {
                if (ContainsFileHeader(card->banks[b]) == true) {
                    return card->banks[b];
                }
            }
        }
    }

    return NULL;
}


/*****************************************************************************************
 * int GetFileAreaSize(filearea_t *fa)
 *
 * @brief Return the total size of the file area in bytes, regardless of content
 * @param fa
 * @return 0 if file area is not defined, otherwise file area capacity in bytes
 ****************************************************************************************/
int GetFileAreaSize(filearea_t *fa)
{
    int fileAreaSize = 0;
    
    if (fa != NULL) {
        fileAreaSize = fa->fileAreaHdr->size * BANKSIZE - 64; /* correct size without file header... */
    }
    
    return fileAreaSize;
}


/*****************************************************************************************
 * int GetFileAreaFreeSpace(filearea_t *fa)
 *
 * @brief Return the size of free space in the file area in bytes
 * @param fa
 * @return 0 if file area is not defined
 ****************************************************************************************/
int GetFileAreaFreeSpace(filearea_t *fa)
{
    int fileAreaFreeSpace = 0;

    if (fa != NULL) {
        fileAreaFreeSpace = GetFileAreaSize(fa) - 
                            (ptrToInt(GetFreeSpacePtr(fa)) - ptrToInt(fa->fileAreaBottomBank->index << 16));
    }

    return fileAreaFreeSpace;
}


/*****************************************************************************************
 * void FreeFileArea(filearea_t *fa)
 *
 * @brief Release all allocated memory of meta data structures that defined the file area
 * @param fa pointer to file area structure
 ****************************************************************************************/
void FreeFileArea(filearea_t *fa)
{
    if (fa != NULL) {
        if (fa->fileAreaHdr != NULL) {
            FreeFileAreaHeaderStructure(fa->fileAreaHdr);
            fa->fileAreaHdr = NULL;
        }

        if (fa->entries != NULL) {
            FreeFileEntries(fa->entries, fa->totalFileEntries);
            fa->entries = NULL;
        }

        free(fa);
    }
}


/* ------------------------------------------------------------------------------------------
   char *AdjustEntryFilename(char *filename)

   Adjust entry filename to use "/" as path separators

   Returns:
   same pointer as argument (beginning of filename)
   ------------------------------------------------------------------------------------------ */
char *AdjustEntryFilename(char *filename)
{
    char *flnmptr = filename;

    if (filename == NULL) {
        return NULL;
    }

    while(*flnmptr != '\0') {
        if (*flnmptr == '\\') {
            *flnmptr = '/';
        }

        flnmptr++;
    }

    return filename;
}

/* ------------------------------------------------------------------------------------------
    Private functions
   ------------------------------------------------------------------------------------------ */


/********************************************************************************
 * filearea_t *CreateFileArea(card_t *card)
 *
 * @brief Create & "format" a file area in allocated card, verified to be empty for adding files
 * @param card
 * @return pointer to allocated file area meta data, or NULL (non-empty, or no memory)
 ********************************************************************************/
static filearea_t *CreateFileArea(card_t *card)
{
    filearea_t *fa = NULL;
    int appArea16K, topBankNo, fileAreaSize;

    if (card != NULL) {
        /* create file area on card */
        fa = InitFileAreaStructure();
        if (fa != NULL) {
            fa->card = card;

            /* create file header on card and validate file area to be empty */
            if ( (appArea16K = ContainsOzRomHeader(GetTopBank(card))) > 0 ||
                 (appArea16K = ContainsAppHeader(GetTopBank(card))) > 0) {
                /* create a file area below OZ ROM or application area */
                if (card->totalBanks == appArea16K) {
                    fprintf(stderr,"No room for file area, reserved for applications.\n");
                    errorstatus = true;
                    FreeFileArea(fa);
                    fa = NULL;
                } else {
                    /* create file area below app area, depending on size of card */
                    switch(card->totalBanks) {
                        case 2:   /* 32K */
                        case 8:   /* 128K */
                        case 256: /* 256K */
                            /* UV EPROM sizes, 16K organisation, file area just one bank below app area */
                            topBankNo = card->totalBanks - appArea16K - 1;
                            if (topBankNo < 0) {
                                /* miminim 16K bank size is not available... */
                                fprintf(stderr,"No room for file area in UV Eprom.\n");
                                errorstatus = true;
                                FreeFileArea(fa);
                                fa = NULL;
                            } else {
                                if ( FormatFileArea(fa, card->banks[topBankNo]) == false ) {
                                    FreeFileArea(fa);
                                    fa = NULL;
                                }
                            }                            
                            break;

                        case 32: /* 512K */ 
                        case 64: /* 1024K */
                            /* Flash card (Amd/Intel), 512K or 1Mb size, 64 sector organisation */
                            /* align file area modulus 64K below app area */
                            fileAreaSize = card->totalBanks - appArea16K;
                            fileAreaSize -= (fileAreaSize % 4);
                            if (fileAreaSize < 4) {
                                /* miminim 64K sector size is not available... */
                                fprintf(stderr,"No room for file area, less than 64K.\n");
                                errorstatus = true;
                                FreeFileArea(fa);
                                fa = NULL;
                            } else {
                                if ( FormatFileArea(fa, card->banks[fileAreaSize-1]) == false ) {
                                    FreeFileArea(fa);
                                    fa = NULL;
                                }
                            }
                    }
                }
            } else {
                /* the entire card will be identified for file area... */
                if ( FormatFileArea(fa, GetTopBank(card)) == false ) {
                    FreeFileArea(fa);
                    fa = NULL;
                }
            }
        }
    }

    return fa;
}


/********************************************************************************
 * filearea_t *LoadFileArea(card_t *card, bank_t *bnk)
 *
 * @brief Read existing file area in card container and construct meta data-structures
 * @param card
 * @param bnk
 * @return pointer to allocated file area meta data, or NULL (no card container, no memory)
 ********************************************************************************/
static filearea_t *LoadFileArea(card_t *card, bank_t *fhdrBnk)
{
    filearea_t *fa = NULL;

    if (card != NULL) {
        fa = InitFileAreaStructure();
        if (fa != NULL) {
            fa->card = card;
            fa->fileAreaHdr = ReadFileAreaHeader(fhdrBnk);
            if (fa->fileAreaHdr != NULL) {
                fa->fileAreaTopBank = fhdrBnk;
                fa->fileAreaBottomBank = card->banks[ fhdrBnk->index - fa->fileAreaHdr->size + 1];                

                /* Parse file area for file entries, and load into data-structure */
                ScanFileEntries(fa);
            } else {
                /* Ups, no heap memory! */
                FreeFileArea(fa);
                fa = NULL;
            }
        }
    }

    return fa;
}


/********************************************************************************
 * void ScanFileEntries(filearea_t *fa)
 *
 * @brief Scan an existing file area for entries to build the internal FAT
 * @param fa the current file area
 ********************************************************************************/
static void ScanFileEntries(filearea_t *fa)
{
    int fileEntryPtr;
    fileentry_t *fe;

    if (fa != NULL) {
        /* the bank array index serves as relative card bank number 00 - 3F for 1Mb, for example */
        /* first file entry is at offset 0 of bottom bank of file area */
        fileEntryPtr = fa->fileAreaBottomBank->index << 16;
        while (CardGetByte(fa->card, fileEntryPtr) != 0xff && CardGetByte(fa->card, fileEntryPtr) != 0x00) {
            fe = LoadFileEntry(fa, fileEntryPtr);
            if (fe != NULL) {
                if (RecordFileEntry(fa, fe) == true) {
                    /* point at next File Entry (or empty space)... */
                    fileEntryPtr = intToPtr(ptrToInt(fileEntryPtr) + fe->hdrLength + fe->fileLength);
                } else {
                    /* problems in recording the parsed file entry, abort scanning */
                    FreeFileEntryStructure(fe);
                    return;
                }
            } else {
                fprintf(stderr,"No Room. Identified File entry could not be recorded.\n");
                errorstatus = true;
                return;
            }
        }
    }
}


/********************************************************************************
 * fileentry_t *LoadFileEntry(filearea_t *fa, int fileEntryPtr)
 *
 * @brief Load existing file entry into meta data structure
 * @param fa the current file area
 * @param fileEntryPtr extended 24bit pointer to beginning of file entry
 * @return pointer to allocated file entry meta data, or NULL (no memory)
 ********************************************************************************/
static fileentry_t *LoadFileEntry(filearea_t *fa, int fileEntryPtr)
{
    fileentry_t *fe = AllocFileEntryStructure();
    int c;
    int filenameLength;

    if (fe != NULL) {
        fe->hdrLength = 0;
        fe->fileEntryPtr = fileEntryPtr;
        filenameLength = CardGetByte(fa->card, fileEntryPtr);

        GetNextExtAddress(&fileEntryPtr);
        fe->hdrLength++;

        if (CardGetByte(fa->card, fileEntryPtr) == 0) {
            /* first char of filename is 0, which identifies a file entry marked as deleted */
            filenameLength--; /* filename is one char less...*/
            GetNextExtAddress(&fileEntryPtr);
            fe->hdrLength++;
            fe->deleted = true;
        } else {
            fe->deleted = false;
        }

        fe->fileName = AllocIdentifier(filenameLength+1);
        if (fe->fileName != NULL) {
            /* allocate space for filename + null-terminator, copy into meta structure */
            for (c=0; c<filenameLength; c++) {
                fe->fileName[c] = CardGetByte(fa->card, fileEntryPtr);
                GetNextExtAddress(&fileEntryPtr);
                fe->hdrLength++;
            }
            fe->fileName[c] = '\0'; /* null-terminate filename string */

            /* retrieve the length of the file image (4 bytes) */
            fe->fileLength = CardGetByte(fa->card, fileEntryPtr);
            GetNextExtAddress(&fileEntryPtr);
            fe->fileLength |= CardGetByte(fa->card, fileEntryPtr) << 8;
            GetNextExtAddress(&fileEntryPtr);
            fe->fileLength |= CardGetByte(fa->card, fileEntryPtr) << 16;
            GetNextExtAddress(&fileEntryPtr);
            fe->fileLength |= CardGetByte(fa->card, fileEntryPtr) << 24;
            GetNextExtAddress(&fileEntryPtr);
            fe->hdrLength += 4;

            /* finally, record to extended pointer to the actual file image on the card */
            fe->fileImagePtr = fileEntryPtr;
        } else {
            /* problems with allocating filename, abort mission */
            FreeFileEntryStructure(fe);
            fe = NULL;
        }
    }

    return fe;
}


/********************************************************************************
 * void WriteFileEntryHeader(filearea_t *fa, int fileEntryPtr, char *fileEntryName, int fileImageSize)
 *
 * @brief Internal helper method to "blow" the file entry header
 *
 * @param fa, the file area
 * @param fileEntryPtr, the extended pointer to free space in file area
 * @param fileEntryName, pointer to string of file entry name
 * @param fileImageSize, size of file image in bytes
 ********************************************************************************/
static void WriteFileEntryHeader(filearea_t *fa, int fileEntryPtr, char *fileEntryName, int fileImageSize)
{
    int i;

    if (fa != NULL) {
        /* first store byte of new file entry (length of filename)... */
        CardSetByte(fa->card, fileEntryPtr, strlen(fileEntryName));
        GetNextExtAddress(&fileEntryPtr);

        /* followed by the filename... */
        for (i=0; i< (int) strlen(fileEntryName); i++) {
            CardSetByte(fa->card, fileEntryPtr, fileEntryName[i]);
            GetNextExtAddress(&fileEntryPtr);
        }

        /* followed by the file length, 4 bytes LSB order... */
        for (i = 0; i < 4; i++) {
            CardSetByte(fa->card, fileEntryPtr, fileImageSize & 0xFF);
            GetNextExtAddress(&fileEntryPtr);
            fileImageSize >>= 8;
        }
    }
}


/********************************************************************************
 * bool RecordFileEntry(filearea_t *fa, fileentry_t *fe)
 *
 * @brief Record parsed File Entry into File Area FAT
 *
 * @param fa, the file area
 * @param fe, the file entry
 * @return true, if file entry has been successfully recorded in FAT
 ********************************************************************************/
static bool RecordFileEntry(filearea_t *fa, fileentry_t *fe)
{
    fileentry_t **fefat;

    if (fa != NULL) {
        if (fe != NULL) {
            if (fa->totalFileEntries == fa->entryCapacity) {
                /* file entry FAT is full, increase capacity.. */
                fefat = AllocFileEntries(fa->entries, fa->totalFileEntries, fa->entryCapacity+256);
                if (fefat != NULL) {
                    fa->entries = fefat;
                    fa->entryCapacity += 256;
                } else {
                    /* report memory error */
                    fprintf(stderr,"No Room. File entry '%s' could not be recorded.\n", fe->fileName);
                    errorstatus = true;
                    return false;
                }
            }

            fa->entries[fa->totalFileEntries++] = fe;
        }
    }

    return true;
}


/********************************************************************************
 * fileentry_t *GetFileEntryByName(filearea_t *fa, char *filename)
 *
 * @brief Find active (non-deleted) file entry by filename
 *
 * @param fa, the file area
 * @param filename, pointer to search string
 * @return pointer to found file entry, or NULL if not found
 ********************************************************************************/
static fileentry_t *GetFileEntryByName(filearea_t *fa, char *filename)
{
    fileentry_t *fe = NULL;
    int n = (int) strlen(filename);
    int i;

    if (fa != NULL) {
        for (i=0; i < fa->totalFileEntries; i++) {
            if ( strnicmp( filename, fa->entries[i]->fileName, n ) == 0) {
                fe = fa->entries[i];
                break;
            }
        }
    }

    return fe;
}


/********************************************************************************
 * void MarkFileEntryAsDeleted(filearea_t *fa, fileentry_t *fe)
 *
 * @brief Mark file entry as deleted in file area
 *
 * @param fa, the file area
 * @param fe, pointer to file entry meta data
 ********************************************************************************/
static void MarkFileEntryAsDeleted(filearea_t *fa, fileentry_t *fe)
{
    int fileEntryPtr;

    if (fa != NULL && fe != NULL) {
        fileEntryPtr = fe->fileEntryPtr;        /* points at length of filename */
        GetNextExtAddress(&fileEntryPtr);       /* point at '/' of filename */
        CardSetByte(fa->card, fileEntryPtr, 0); /* mark entry as deleted in card memory */
        fe->deleted = true;                     /* also identify the deleted status in the meta data */
    }
}


/********************************************************************************
 * int ptrToInt(int extAddress)
 *
 * @brief Convert the extended address to a calculable integer.
 *
 * @param extAddress the extended address
 * @return integer
 ********************************************************************************/
static int ptrToInt(int extAddress)
{
    int bank = (extAddress >> 16) & 0x3F; // no slot info...
    int offset = extAddress & 0x3FFF;

    return bank * BANKSIZE + offset;
}


/********************************************************************************
 * int intToPtr(int i)
 *
 * @brief Convert the calculable integer to an extended address.
 * @param i
 * @return extended address
 ********************************************************************************/
static int intToPtr(int i)
{
    int bank = i / BANKSIZE;
    int offset = i % BANKSIZE;

    return (bank << 16) | offset;
}


/********************************************************************************
 * int GetFreeSpacePtr(filearea_t *fa)
 *
 * @brief Get pointer to first free space in File Area (where to store a new file).
 * @return extended address of free space,
 ********************************************************************************/
static int GetFreeSpacePtr(filearea_t *fa)
{
    int freeSpacePtr = 0; /* no file area */
    fileentry_t *fe = NULL;

    if (fa != NULL) {
        if (fa->totalFileEntries > 0) {
            /* there's file entries available, get last entry from list */
            fe = fa->entries[fa->totalFileEntries-1];
            freeSpacePtr = intToPtr( ptrToInt(fe->fileEntryPtr) + fe->hdrLength + fe->fileLength );
        } else {
            freeSpacePtr = fa->fileAreaBottomBank->index << 16;
        }
    }

    return freeSpacePtr;
}


/********************************************************************************
 * bool FormatFileArea(filearea_t *fa, bank_t *bnk)
 *
 * @brief Create file header in specified bank and validate that all banks of file are is empty (FFh)
 *
 * @param fa the file area structure
 * @param bnk pointer to bank instance
 * @return true if file are was "formatted", or false if it was not empty (conflict of data loaded)
 ********************************************************************************/
static bool FormatFileArea(filearea_t *fa, bank_t *bnk)
{
    int bankIndex = bnk->index;
    
    if (fa != NULL) {
        /* first check that all banks of file area are empty */
        while (bankIndex >=0) {
            if (IsEmpty(fa->card->banks[bankIndex]) == false) {
                fprintf(stderr,"For file area, card is not empty in bank %02X\n", fa->card->banks[bankIndex]->bankNo);
                errorstatus = true;
                return false;
            }

            bankIndex--;
        }

        /* file area is empty, create file header in top of file area */
        fa->fileAreaTopBank = bnk;
        WriteFileAreaHeader(fa, bnk);

        /* file area formatted, read file header information */
        fa->fileAreaHdr = ReadFileAreaHeader(bnk);
        if (fa->fileAreaHdr != NULL) {
            fa->fileAreaTopBank = bnk;
            fa->fileAreaBottomBank = fa->card->banks[ fa->fileAreaTopBank->index - fa->fileAreaHdr->size + 1];                
            
            return true;
        } else
            return false;
    }

    return false;
}


/********************************************************************************
 * void WriteFileAreaHeader(filearea_t *fa, bank_t *bnk)
 *
 * @brief Write file header on an external card, to specified bank, offset 3FC0h
 * @param fa
 * @param bnk
 ********************************************************************************/
static void WriteFileAreaHeader(filearea_t *fa, bank_t *bnk)
{
    int offset;
    
    srand(time(NULL));
    
    for (offset = 0x3FC0; offset < 0x3FF7; offset++) {
        SetByte(bnk, offset, 0);
    }
  
    SetByte(bnk, 0x3FF7, 0x01);
    SetByte(bnk, 0x3FF8, rand() % 256);
    SetByte(bnk, 0x3FF9, rand() % 256);
    SetByte(bnk, 0x3FFA, rand() % 256);
    SetByte(bnk, 0x3FFB, rand() % 256);

    SetByte(bnk, 0x3FFC, bnk->index+1); /* size of file area in 16K banks */

    /* define the sub-type, by identifying the card size */
    switch(fa->card->totalBanks) {
        case 2:
            SetByte(bnk, 0x3FFD, 0x7E); /* 32K UV Eprom */
            break;
            
        case 8:
        case 16:
            SetByte(bnk, 0x3FFD, 0x7C); /* 128K / 256K UV Eprom */
            break;
            
        default:
            SetByte(bnk, 0x3FFD, 0x6F);   /* 512K / 1024K, Define as AMD or STM sub type */
    }
    
    SetByte(bnk, 0x3FFE, 'o');
    SetByte(bnk, 0x3FFF, 'z');
}


/********************************************************************************
 * flacardhdr_t *ReadFileAreaHeader(bank_t *bnk)
 *
 * @brief Read File Header memory content of specified bank, offset $3FC8 - $3FFD.
 * @param bnk
 * @return pointer to allocated file header meta data, or NULL if no file header
 ********************************************************************************/
static flacardhdr_t *ReadFileAreaHeader(bank_t *bnk)
{
    flacardhdr_t *flachdr = NULL;

    if (bnk != NULL) {
        if (ContainsFileHeader(bnk) == true) {
            flachdr = AllocFileAreaHeaderStructure();
            if (flachdr != NULL) {
                flachdr->randomId =
                        GetByte(bnk,0x3FF8) << 24 |
                        GetByte(bnk,0x3FF9) << 16 |
                        GetByte(bnk,0x3FFA) << 8 |
                        GetByte(bnk,0x3FFB);

                if ( GetByte(bnk,0x3FEE) == 'o' && GetByte(bnk,0x3FEF) == 'z') {
                    /* Size of file area in 16K banks, slot 0 ROM */
                    flachdr->size = GetByte(bnk,0x3FEC);
                } else {
                    /* Size of file area in 16K banks, external cards */
                    flachdr->size = GetByte(bnk,0x3FFC);
                }

                flachdr->subtype = GetByte(bnk,0x3FFD);
                flachdr->bankHdr = bnk->bankNo;
            }
        }
    }

    return flachdr;
}


/********************************************************************************
 * filearea_t *InitFileAreaStructure(void)
 *
 * @brief Allocate & initialize filearea_t structure
 * @return pointer to initialized file area meta data, or NULL if no memory
 ********************************************************************************/
static filearea_t *InitFileAreaStructure(void)
{
    filearea_t *fa = AllocFileAreaStructure();
    
    if (fa != NULL) {
        fa->card = NULL;
        fa->fileAreaBottomBank = NULL; /* fetched via card container */
        fa->fileAreaTopBank = NULL;   /* will be resolved when header is created/loaded */
        fa->fileAreaHdr = NULL;       /* will be resolved when header is created/loaded */
        fa->totalFileEntries = 0;
        
        /* initialize empty array of file entry pointers */
        fa->entryCapacity = 256;
        fa->entries = AllocFileEntries(NULL, 0, fa->entryCapacity);
        if (fa->entries == NULL) {
            free(fa);
            fa = NULL;
        }
    }
    
    return fa;
}


static void FreeFileEntryStructure(fileentry_t *fe)
{
    if (fe != NULL) {
        if (fe->fileName != NULL)
            free(fe->fileName);
        free(fe);
    }
}

  
static void FreeFileEntries(fileentry_t **entries, int totalEntries)
{
    int b;

    if (entries != NULL) {
        for (b=0; b < totalEntries; b++) {
            if (entries[b] != NULL) {
                FreeFileEntryStructure(entries[b]);
                entries[b] = NULL;
            }
        }
    }

    free(entries); /* release the array of pointers */
}


static void FreeFileAreaHeaderStructure(flacardhdr_t *flchdr)
{
    if (flchdr != NULL) {
        free(flchdr);
    }
}


static fileentry_t *AllocFileEntryStructure(void)
{
    return (fileentry_t *) malloc (sizeof(fileentry_t));
}


static filearea_t *AllocFileAreaStructure(void)
{
    return (filearea_t *) malloc (sizeof(filearea_t));
}


static flacardhdr_t *AllocFileAreaHeaderStructure(void)
{
    return (flacardhdr_t *) malloc (sizeof(flacardhdr_t));
}


static fileentry_t **AllocFileEntries(fileentry_t **entries, const int currentEntries, const int totalEntries)
{
    int i = currentEntries;
    fileentry_t **fearray = (fileentry_t **) realloc (entries, totalEntries * sizeof(fileentry_t));
    
    if (fearray != NULL) {
        for (; i<totalEntries; i++) {
            fearray[i] = NULL;
        }
    }
    
    return fearray;
}
