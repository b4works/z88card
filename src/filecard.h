
/* -------------------------------------------------------------------------------------------------

  File Area functionality to load files into card.
  This file is part of Z88Card.

  Copyright (C) 2016, Gunther Strube, gstrube@gmail.com

  Z88Card is free software; you can redistribute it and/or modify it under the terms of the
  GNU General Public License as published by the Free Software Foundation;
  either version 2, or (at your option) any later version.
  Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along with Z88Card;
  see the file COPYING. If not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/

#ifndef FILECARD_H
#define FILECARD_H

#include "bank.h"

typedef struct {
    int         randomId;                   /* 4-byte random number */
    int         size;                       /* size of file area in 16K banks */
    int         subtype;
    int         bankHdr;                    /* bank number of 'oz' file area header */
} flacardhdr_t;

typedef struct {
    char        *fileName;                  /* Filename of entry. If the entry is active, the filename begins with a '/' */
    int         hdrLength;                  /* Length of File Entry Header */
    int         fileLength;                 /* Length of file image (excluding the file entry data) */
    bool        deleted;                    /* Indicates whether the file entry is marked as deleted or not */
    int         fileEntryPtr;               /* The File Entry pointer */
    int         fileImagePtr;               /* Pointer to beginning of file image */
} fileentry_t;


typedef struct {
    card_t       *card;                     /* copy of the reference to the allocated card that holds the file area */
    bank_t       *fileAreaTopBank;          /* The top bank number of the File Area, NULL if no file area */
    bank_t       *fileAreaBottomBank;       /* The bottom bank number of the File Area, NULL if no file area */
    int           totalFileEntries;         /* no. of entries loaded in file area */
    fileentry_t **entries;                  /* array of (pointers to) file entries in file area */
    int           entryCapacity;            /* current pre-allocated space of entry pointers in blocks of 256 pointers */
    flacardhdr_t *fileAreaHdr;              /* The File Header of this File Area */
} filearea_t;

filearea_t *InitFileArea(card_t *card);
void FreeFileArea(filearea_t *fa);
bool StoreFileEntry(filearea_t *fa, char *fileEntryName, mem_t fileImage, int fileImageSize);
bool isFileCard(card_t *card);
bank_t *GetFileHeaderBank(card_t *card);
int GetFileAreaSize(filearea_t *fa);
int GetFileAreaFreeSpace(filearea_t *fa);
char *AdjustEntryFilename(char *filename);

#endif // FILECARD_H
