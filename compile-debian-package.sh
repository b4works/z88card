#!/bin/bash

# *************************************************************************************
#
# Build script to generate Ubuntu/Debian binary package for Z88Card utility.
#
# Requirements:
# a) apt-get install build-essential dpkg-dev devscripts dh-make qt4-qmake
# b) import GPG keys (optional):
#	gpg --import mygpgkey_pub.asc
#	gpg --allow-secret-key-import --import mygpgkey-secret.asc
#
# 1) Debian / Ubuntu package development tools
# 2) qmake (>= 4.6.0), make / gcc compiler tools
#
# Copyright (C) 2012-2016, Gunther Strube, gstrube@gmail.com
#
# Z88Card is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Z88Card;
# see the file COPYING.  If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# *************************************************************************************

# also remember to change version in debian/changelog
Z88CARD_PACKAGEVERSION=2.0.0~ppa1

# get the relative path to this script
Z88CARD_PATH=$PWD
Z88CARD_SRC=$Z88CARD_PATH/src

command -v debuild >/dev/null 2>&1 || { echo "debuild utility is not available on system!" >&2; exit 1; }
command -v dpkg-buildpackage >/dev/null 2>&1 || { echo "dpkg-buildpackage utility is not available on system!" >&2; exit 1; }
command -v tar >/dev/null 2>&1 || { echo "tar utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v gcc >/dev/null 2>&1 || { echo "GNU C compiler is not available on system!" >&2; exit 1; }

QMAKE_FOUND=1
QMAKE4_FOUND=1
QMAKE5_FOUND=1
command -v qmake >/dev/null 2>&1 || { QMAKE_FOUND=0; }
command -v qmake-qt4 >/dev/null 2>&1 || { QMAKE4_FOUND=0; }
command -v qmake-qt5 >/dev/null 2>&1 || { QMAKE5_FOUND=0; }

if [ $QMAKE_FOUND -eq 0 ] && [ $QMAKE4_FOUND -eq 0 ] && [ $QMAKE5_FOUND -eq 0 ]; then
      echo "qmake utility is not available on system!" >&2
      exit 1
else
    if [ $QMAKE_FOUND -eq 1 ]; then
        QMAKE_TOOL=qmake
    fi
    if [ $QMAKE4_FOUND -eq 1 ]; then
        QMAKE_TOOL=qmake-qt4
    fi
    if [ $QMAKE5_FOUND -eq 1 ]; then
        QMAKE_TOOL=qmake-qt5
    fi
fi

# delete any previous build output
rm -fR z88card-$Z88CARD_PACKAGEVERSION
rm -f z88card_$Z88CARD_PACKAGEVERSION*

# copy current Z88Card src folder as "z88card-<version>" folder
mkdir -p z88card-$Z88CARD_PACKAGEVERSION/src
if test $? -gt 0; then
	echo "Unable to create z88card-$Z88CARD_PACKAGEVERSION/src folder. build-script aborted."
	exit 1
fi

mkdir -p z88card-$Z88CARD_PACKAGEVERSION/docs
if test $? -gt 0; then
	echo "Unable to create z88card-$Z88CARD_PACKAGEVERSION/docs folder. build-script aborted."
	exit 1
fi

cp -fR $Z88CARD_SRC z88card-$Z88CARD_PACKAGEVERSION
if test $? -gt 0; then
	rm -fR z88card-$Z88CARD_PACKAGEVERSION
	echo "copying current source tree failed. build-script aborted."
	exit 1
fi

# copy debian folder into as "z88card_<version>" folder
cp -fR $Z88CARD_PATH/debian z88card-$Z88CARD_PACKAGEVERSION/debian
if test $? -gt 0; then
	rm -fR z88card-$Z88CARD_PACKAGEVERSION
	echo "copying debian/ tree failed. build-script aborted."
	exit 1
fi

# copy docs folder into "z88card_<version>" folder
cp -fR $Z88CARD_PATH/docs z88card-$Z88CARD_PACKAGEVERSION
if test $? -gt 0; then
	rm -fR z88card-$Z88CARD_PACKAGEVERSION
	echo "copying docs/ folder failed. build-script aborted."
	exit 1
fi

cp z88card.pro z88card-$Z88CARD_PACKAGEVERSION

cd "z88card-$Z88CARD_PACKAGEVERSION"

# remove Git repository, executable scripts & compile output in copied folder..
rm -fR .git
rm -f *.o
rm -fR ../bin
rm -fR ../build
rm -f Makefile
rm -f z88card.pro.user
rm -f *~

# generate original package, which is used to build package
tar czf ../z88card_$Z88CARD_PACKAGEVERSION.orig.tar.gz *

# build the source debian package...
debuild -i -I -S -sa

if test $? -eq 0; then
	echo "DEB source package built successfully."
fi

# build the debian architecture-specific installer package
dpkg-buildpackage -rfakeroot -us -uc -b

if test $? -eq 0; then
	echo "DEB installer package built successfully."
else
	exit 1
fi

cd $Z88CARD_PATH
