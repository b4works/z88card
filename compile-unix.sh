#!/bin/bash

# *************************************************************************************
# Generic Unix compile script for Z88Card - the Z88 Application Card Generator.
# (C) Gunther Strube (gstrube@gmail.com) 2016
#
# Requirements:
#   make and gcc (llvm)
#   and available in PATH and LD_LIBRARY_PATH using distribution package system (DEB, RPM)
#
# Z88Card is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Z88Card;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# *************************************************************************************


# get the relative path to this script
Z88CARD_PATH=$PWD
Z88CARD_SRC=$Z88CARD_PATH/src

USE_STATIC_MAKEFILE=1
QMAKE_FOUND=1
QMAKE4_FOUND=1
QMAKE5_FOUND=1
command -v qmake >/dev/null 2>&1 || { QMAKE_FOUND=0; }
command -v qmake-qt4 >/dev/null 2>&1 || { QMAKE4_FOUND=0; }
command -v qmake-qt5 >/dev/null 2>&1 || { QMAKE5_FOUND=0; }

if [ $QMAKE_FOUND -eq 1 ]; then
    QMAKE_TOOL=qmake
fi
if [ $QMAKE4_FOUND -eq 1 ]; then
    QMAKE_TOOL=qmake-qt4
fi
if [ $QMAKE5_FOUND -eq 1 ]; then
    QMAKE_TOOL=qmake-qt5
fi

CC_FOUND=1
GCC_FOUND=1

command -v make >/dev/null 2>&1 || { echo "make utility was not found on system!" >&2; exit 1; }
command -v cc >/dev/null 2>&1 || { CC_FOUND=0; }
command -v gcc >/dev/null 2>&1 || { GCC_FOUND=0; }

if [ $CC_FOUND -eq 0 ] && [ $GCC_FOUND -eq 0 ]; then
    echo "C compiler was not found on system!" >&2;
    exit 1
fi

# delete all auto-generated make files, if generated
rm -f Makefile src/Makefile

# delete qmake-related auto-generated files
rm -f z88card.pro.user* .qmake.stash

# delete the build/ and generated folders that are created via Qmake's Makefile
rm -fR build

# if Qmake was found, let it generate a Makefile
if [ $QMAKE_FOUND -eq 1 ] || [ $QMAKE4_FOUND -eq 1 ] || [ $QMAKE5_FOUND -eq 1 ]; then
    USE_STATIC_MAKEFILE=0
    $QMAKE_TOOL -config release z88card.pro
    if test $? -gt 0; then
        echo "qmake failed generating makefile - fallback to generic Unix Makefile.."
        USE_STATIC_MAKEFILE=1
    else
        if [ ! -f $Z88CARD_PATH/Makefile ]; then
            echo "$Z88CARD_PATH/Makefile was not created by qmake - fallback to generic Unix Makefile.."
            USE_STATIC_MAKEFILE=1
        fi
    fi
fi

if [ $USE_STATIC_MAKEFILE -eq 1 ]; then

    # compile Z88Card sources "manually"...
    cd "$Z88CARD_SRC"

    make -f Makefile.all platform=UNIX
    if test $? -gt 0; then
        echo "make failed compiling Z88Card sources!"
        cd ..
    else
        cd ..
        echo "Z88Card compiled successfully on your system."
        ls -lp src/z88card
    fi
else
    # compile using auto-generated Makefile from Qmake
    make
    ls -lh build/z88card
fi
