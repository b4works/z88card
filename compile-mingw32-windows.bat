:: *************************************************************************************
:: Windows compile script for Z88Card - the Z88 Application Card Generator.
:: Copyright (C) 2016-2022, Gunther Strube, hello@bits4fun.net
::
:: Optional:
::   Build using Qmake if found on system (priority #1)
::
:: Requirements:
::   mingw32-make and gcc-compatible compiler available in PATH
::
:: Z88Card is free software; you can redistribute it and/or modify it under the terms of the
:: GNU General Public License as published by the Free Software Foundation;
:: either version 2, or (at your option) any later version.
:: Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
:: without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:: See the GNU General Public License for more details.
:: You should have received a copy of the GNU General Public License along with Z88Card;
:: see the file COPYING. If not, write to the
:: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
::
:: *************************************************************************************

@echo off

:: --------------------------------------------------------------------------
:: delete any previous compile output
rmdir /S /Q "%Z88CARD_PATH%\debug" 2>nul >nul
rmdir /S /Q "%Z88CARD_PATH%\release" 2>nul >nul
del /S /Q "%Z88CARD_PATH%\z88card.pro.user" 2>nul >nul
del /S /Q "%Z88CARD_PATH%\.qmake.stash" 2>nul >nul
del /S /Q "%Z88CARD_PATH%\Makefile" 2>nul >nul

:: --------------------------------------------------------------------------
:: remember the current path in from where this script is called
set Z88CARD_PATH=%cd%
set Z88CARD_SRC=%Z88CARD_PATH%\src

:: try to locate mingw32-make.exe...
set mingwmake_available=0
for %%x in (mingw32-make.exe) do if not [%%~$PATH:x]==[] set mingwmake_available=1
if "%mingwmake_available%"=="0" goto MINGW32MAKE_NOT_AVAILABLE

:: try to locate qmake.exe...
set qmake_available=0
for %%x in (qmake.exe) do if not [%%~$PATH:x]==[] set qmake_available=1
if "%qmake_available%"=="0" goto QMAKE_NOT_AVAILABLE

qmake -config release z88card.pro
if ERRORLEVEL 0 goto CHECK_MAKEFILE
echo qmake failed generating makefiles!
goto END

:CHECK_MAKEFILE
if exist "%Z88CARD_PATH%\Makefile" goto COMPILE_MAKEAPP
echo "%Z88CARD_PATH%\Makefile was not created by qmake!"
goto END

:COMPILE_MAKEAPP
mingw32-make
if ERRORLEVEL 0 goto MAKEAPP_COMPILED
echo make failed compiling Z88Card sources!
goto END

:MAKEAPP_COMPILED
echo Z88Card compiled successfully on your system.
goto END

:QMAKE_NOT_AVAILABLE
:: QMAKE not available, try to use use generic MingW compiler on Windows with static makefile
set gcc_available=0
for %%x in (gcc.exe) do if not [%%~$PATH:x]==[] set gcc_available=1
if "%gcc_available%"=="0" goto MINGW32_COMPILER_NOT_AVAILABLE
cd "%Z88CARD_SRC%"
mingw32-make -f Makefile.all platform=MSWIN
if ERRORLEVEL 0 goto MAKEAPP_COMPILED
goto END

:MINGW32MAKE_NOT_AVAILABLE
echo mingw32-make.exe utility could not be located!
echo (PATH env. variable doesn't point to Mingw32 compiler tools and libs)
goto END

:MINGW32_COMPILER_NOT_AVAILABLE
echo MingW C/C++ compilter could not be located!
echo (PATH env. variable doesn't point to Mingw32 compiler tools and libs)
goto END

:END
cd %Z88CARD_PATH%
@echo on
