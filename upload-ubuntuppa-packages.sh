#!/bin/bash

# *************************************************************************************
#
# Build script to generate Debian source packages of Z88Card, uploaded to ppa:cambridgez88/z88card
#
# Requirements:
# 1) Debian / Ubuntu package development tools
#    build-essential fakeroot dpkg-dev devscripts dh-make
# 2) libqt4-dev (>= 4.8.0), qt4-qmake (>= 4.8.0), make / gcc compiler tools
#
# Copyright (C) 2012, Gunther Strube, gstrube@gmail.com
#
# Z88Card is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Z88Card;
# see the file COPYING.  If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# *************************************************************************************

Z88CARD_PACKAGEVERSION=1.0.5~ppa2

# get the relative path to this script
Z88CARD_PATH=$PWD
Z88CARD_SRC=$Z88CARD_PATH/src

command -v debuild >/dev/null 2>&1 || { echo "debuild utility is not available on system!" >&2; exit 1; }
command -v dpkg-buildpackage >/dev/null 2>&1 || { echo "dpkg-buildpackage utility is not available on system!" >&2; exit 1; }
command -v tar >/dev/null 2>&1 || { echo "tar utility is not available on system!" >&2; exit 1; }
command -v qmake-qt4 >/dev/null 2>&1 || { echo "qmake utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v g++ >/dev/null 2>&1 || { echo "GNU C++ compiler is not available on system!" >&2; exit 1; }

# delete any previous build output
rm -fR z88card-*
rm -f z88card_*
rm -f *.o
rm -fR bin
rm -fR build
rm -fR Makefile
rm -f z88card.pro.user
rm -fR *~

# generate original package, which is used to build package
tar --exclude ".*" -czf z88card_$Z88CARD_PACKAGEVERSION.orig.tar.gz *
if test $? -gt 0; then
	echo "Unable to create z88card_$Z88CARD_PACKAGEVERSION.orig.tar.gz archive. build-script aborted."
	exit 1
fi

mkdir z88card-$Z88CARD_PACKAGEVERSION
if test $? -gt 0; then
	echo "Unable to create z88card-$Z88CARD_PACKAGEVERSION folder. build-script aborted."
	exit 1
fi

# extract archive into build folder
tar xzf z88card_$Z88CARD_PACKAGEVERSION.orig.tar.gz -C z88card-$Z88CARD_PACKAGEVERSION
# the ubuntu source package builder will create ubuntu-release specific orig.tar.gz packages
rm -f z88card_$Z88CARD_PACKAGEVERSION.orig.tar.gz

# build the DEB source package...
cd z88card-$Z88CARD_PACKAGEVERSION
$Z88CARD_PATH/ppa_publish ppa:cambridgez88/z88card precise trusty vivid wily

if test $? -eq 0; then
	echo "SOURCE DEB package built successfully."
fi
cd $Z88CARD_PATH
