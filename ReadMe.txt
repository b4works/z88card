Git repository for Z88Card - Cambridge Z88 Application Card Manager
GPL V2 software


------------------------------------------------------------------------------------
Introduction
------------------------------------------------------------------------------------

Z88Card is the core tool used to build application card binaries for the
Cambridge Z88, combining different Zilog Z80 code and data into a specially designed
data structure which the Cambridge Z88 portable computer understand.

Once the card binaries are blown on a Cambridge Z88 Memory Card, it can be inserted
into the computer, so applications will be installed into the operating system.


Read more about the Z88Card tool here here:
https://cambridgez88.jira.com/browse/MKAP

Read more about the Cambridge Z88 project here:
https://cambridgez88.jira.com/wiki



------------------------------------------------------------------------------------
Downloading pre-made executable for your desktop operating system
------------------------------------------------------------------------------------

For convenience, ready-made Z88Card executable is available to be downloaded &
installation for Mac OS X, Linux or Windows here:

http://sourceforge.net/projects/z88/files/Z88%20Assembler%20Workbench%20Tools/

Follow the ReadMe description in the Zip archive on how to install it on
your system. Basically, ensure that the tool is available on the operating
system PATH, so it can be accessible everywhere on your command shell.




------------------------------------------------------------------------------------
Compiling Z88Card
------------------------------------------------------------------------------------

Z88Card is developed in standard Ansi-C, easily compilable on any desktop with a C
compiler. The source code project uses Qt's QMake to produce standard Makefile output
for the installed compiler system on your desktop.

Z88Card is found together with Mpm in most Z88 application compile scripts.
The resulting binary is a z88card executable file, just as Mpm.

Start a command shell and change directory to your check-out location. More
information is described in the comments in each platform compile script:

    compile-unix.sh              (qmake, make & gcc are installed your
                                  linux using native package manager)

    compile-qtsdk-linux.sh       (qmake and tools,are installed from
                                  QtSDK for Linux - make & gcc are on linux)

    compile-mingw32-windows.bat  (qmake, mingw make & gcc are installed
                                  through the QtSDK for Windows)

    compile-qtsdk-macosx.sh      (qmake and tools are installed from QtSDK
                                  for Mac OSX. Requires Xcode to be installed)

Provide a PATH setup for the compiled binary, so the Cambridge Z88 source code
project scripts can produce binaries.
