#!/bin/bash

# *************************************************************************************
# Linux compile script using QtSDK for Z88Card - the Z88 Application Card Generator.
# (C) Gunther Strube (gstrube@gmail.com) 2012
#
# Requirements:
#   Qt 4.8.x+ libraries are minimum dependency.
#
#   1)   make and g++ compiler tools are available in PATH and LD_LIBRARY_PATH using
#        distribution package system (DEB, RPM)
#
#   2)   Qt tools and libraries are installed using QtSDK Installer which are available
#        through QTDIR and PATH environment variables, which must be defined in
#        .bash_profile (system script executed during account login)
#
#   2.1) QTDIR points to <QtSDK-Install-Dir>/Desktop/Qt/<version>/gcc
#        export QTDIR=<QtSDK-Install-Dir>/Desktop/Qt/<version>/gcc
#
#   2.2) add to PATH:  export PATH=$QTDIR/bin:$PATH
#
# Z88Card is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# Z88Card is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Z88Card;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# *************************************************************************************


# get the relative path to this script
Z88CARD_PATH=$PWD
Z88CARD_SRC=$Z88CARD_PATH/src

if [ -z "$QTDIR" ]; then
	echo "QTDIR environment variable empty or not defined."
	echo "(QTDIR must point to <QtSDK-Install-Dir>/Desktop/Qt/<version>/gcc)"
	exit 1
fi

command -v qmake >/dev/null 2>&1 || { echo "qmake utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v g++ >/dev/null 2>&1 || { echo "GNU C++ compiler is not available on system!" >&2; exit 1; }

# delete any previous compile output
rm -f $Z88CARD_SRC/*.o
rm -f $Z88CARD_SRC/z88card.pro.user
rm -f $Z88CARD_PATH/Makefile*

qmake -config release z88card.pro
if test $? -gt 0; then
	echo "qmake failed generating makefile!"
	cd $Z88CARD_PATH
	exit 1
fi

if [ ! -f $Z88CARD_PATH/Makefile ]; then
	echo "$Z88CARD_PATH/Makefile was not created by qmake!"
	cd $Z88CARD_PATH
	exit 1
fi

# compile Z88Card sources...
make -f Makefile
if test $? -gt 0; then
	echo "make failed compiling Z88Card sources!"
	cd $Z88CARD_PATH
	exit 1
fi

cd $Z88CARD_PATH

echo "Z88Card compiled successfully on your system."
